#!/bin/bash
#Uso:
# bash script_to_plot_at_each_N_frames-dr_bin16_via_gnu_script.sh 1 199 --> ( $1 = first frame ) and ( $2 = last frame )
#
index=0
for i in `ls *drbin16.vaf`
do
    files_array+=($i)
    ((index++))
done
#echo ${files_array[198]}
#echo ${files_array[@]}

as='"'
pv=';'
hst='#'
dol='$'

echo
echo -e " #From ${as}${0}${as} :"

echo
mkdir -p N_by_N_plots-dr_bin16 ; echo "        N_by_N_plots-dr_bin16 folder created (via 'mkdir -p')"

for N in 2 3 4 5 6 7 8
do
    echo set macros > tmp_file
    echo -e "XY = ${as}u 1:2:(${dol}1-${dol}3):(${dol}1+${dol}3):(${dol}2-${dol}4):(${dol}2+${dol}4):5 w xyerrorbars${as}" >> tmp_file 
    #echo set term png >> tmp_file
    echo set term pngcairo font ${as}Arial, 14${as} >> tmp_file
    echo set yrange[-1.5:1.0] >> tmp_file
    echo set grid >> tmp_file
    echo set title ${as} dr Bin 16 {/Symbol m}m ${as} >> tmp_file
    echo -e "set ylabel ${as}mean Cvv(dr)${as}" >> tmp_file
    echo -e "set xlabel ${as}mean dr [{/Symbol m}m]${as}" >> tmp_file
   
    START=$(($1-1)) ; END=$(($2-1)) ;JUMP=$N
    #for i in $(eval echo "{$START..$END..$JUMP}"); do echo "$i"; done
    #exit $?
    for (( i=START; i<=END; i+=JUMP ))
    do
	a=$((i)); b=$((i+1)); c=$((i+2)); d=$((i+3)); e=$((i+4)); f=$((i+5)); g=$((i+6)); h=$((i+7));
	echo -e "A = ${as} t 'frame ${b}'${as} ${pv} B = ${as} t 'frame ${c}'${as} ${pv} C = ${as} t 'frame ${d}'${as} ${pv} D = ${as} t 'frame ${e}'${as} ${pv} E = ${as} t 'frame ${f}'${as} ${pv} F = ${as} t 'frame ${g}'${as} ${pv} G = ${as} t 'frame ${h}'${as} ${pv} H = ${as} t 'frame $((${h}+1))'${as}" >> tmp_file #; exit $?
	if [ $b -lt 10 ] ; then file="N${N}_000${b}.png" ; fi
	if [ $b -ge 10 ] && [ $b -lt 100 ] ; then file="N${N}_00${b}.png" ; fi
	if [ $b -ge 100 ] && [ $b -lt 1000 ] ; then file="N${N}_0${b}.png" ; fi
	
	if [ $N -eq 2 ] ; then echo set output $as$file$as $pv plot $as${files_array[$a]}$as @XY@A, $as${files_array[$b]}$as @XY@B $pv replot >> tmp_file ; fi
	
	if [ $N -eq 3 ] ; then echo set output $as$file$as $pv plot $as${files_array[$a]}$as @XY@A, $as${files_array[$b]}$as @XY@B, $as${files_array[$c]}$as @XY@C $pv replot >> tmp_file ; fi
	
	if [ $N -eq 4 ] ; then echo set output $as$file$as $pv plot $as${files_array[$a]}$as @XY@A, $as${files_array[$b]}$as @XY@B, $as${files_array[$c]}$as @XY@C, $as${files_array[$d]}$as @XY@D pt 1 $pv replot >> tmp_file ; fi
	
	if [ $N -eq 5 ] ; then echo set output $as$file$as $pv plot $as${files_array[$a]}$as @XY@A, $as${files_array[$b]}$as @XY@B, $as${files_array[$c]}$as @XY@C, $as${files_array[$d]}$as @XY@D pt 1, $as${files_array[$e]}$as @XY@E pt 2 $pv replot >> tmp_file ; fi
	
	if [ $N -eq 6 ] ; then echo set output $as$file$as $pv plot $as${files_array[$a]}$as @XY@A, $as${files_array[$b]}$as @XY@B, $as${files_array[$c]}$as @XY@C, $as${files_array[$d]}$as @XY@D pt 1, $as${files_array[$e]}$as @XY@E pt 2, $as${files_array[$f]}$as @XY@F pt 3 $pv replot >> tmp_file ; fi
	
	if [ $N -eq 7 ] ; then echo set output $as$file$as $pv plot $as${files_array[$a]}$as @XY@A, $as${files_array[$b]}$as @XY@B, $as${files_array[$c]}$as @XY@C, $as${files_array[$d]}$as @XY@D pt 1, $as${files_array[$e]}$as @XY@E pt 2, $as${files_array[$f]}$as @XY@F pt 3, $as${files_array[$g]}$as @XY@G pt 1 $pv replot >> tmp_file ; fi
	
	if [ $N -eq 8 ] ; then echo set output $as$file$as $pv plot $as${files_array[$a]}$as @XY@A, $as${files_array[$b]}$as @XY@B, $as${files_array[$c]}$as @XY@C, $as${files_array[$d]}$as @XY@D pt 1, $as${files_array[$e]}$as @XY@E pt 2, $as${files_array[$f]}$as @XY@F pt 3, $as${files_array[$g]}$as @XY@G pt 1, $as${files_array[$h]}$as @XY@H pt 2 $pv replot >> tmp_file ; fi
    done

    echo
    mv tmp_file script_to_plot_Cvv_at_each_N_frames-dr_bin16.gnu ;
    gnuplot script_to_plot_Cvv_at_each_N_frames-dr_bin16.gnu ; echo -e "        ${N}_by_${N}_plots-dr_bin16 done"
    
    mv N${N}_* N_by_N_plots-dr_bin16/. ; echo -e "        ${N}_by_${N}_plots-dr_bin16 moved to folder"
done

echo
echo " #By by"
exit $?
