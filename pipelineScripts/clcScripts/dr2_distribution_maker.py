import numpy as np

#**** initilizing variables ****#
#N_vectors = 5440  #number of vectors (or linear size of the latice)
#N_X = float(85) ; N_Y = float(64) ; dl = 16  #X and Y litice numebr of point and laticce minimun lentgh

#N_vectors = 4860  #number of vectors (or linear size of the latice)
#N_X = float(81) ; N_Y = float(60) ; dl = 16  #X and Y litice numebr of point and laticce minimun lentgh

N_vectors = 4312  #number of vectors (or linear size of the latice)
N_X = float(77) ; N_Y = float(56) ; dl = 16  #X and Y litice numebr of point and laticce minimun lentgh

N = int(N_X*N_Y)  #points in the lattice
if N != N_vectors:     #security routine
    print "ERROR:   wrong total number of drs pairs" 
    print "code stoped!"
    exit()
N_total_of_drs = (N*(N-1))/2
#**** ****#
#exit()

#Criando a base da lista que vai ter todos os "drs" diferentes 
lista_dr2=[]
counter = 0
for i in range(N):
    for j in xrange(i+1,N):
        Dx = dl*(i-int(N_X)*int(i/N_X)) - dl*(j-int(N_X)*int(j/N_X));
        Dy = -dl*int(i/N_X) + dl*int(j/N_X)       
        dr2 = Dx*Dx + Dy*Dy
        dr = np.sqrt(dr2)
        #print type(dr2)    # debug line
        lista_dr2.append(dr2)
        counter += 1
        
if counter != N_total_of_drs:     #security routine
    print "ERROR:   wrong total number of drs pairs" 
    print "code stoped!"
    exit()

#print lista_dr2  # debug line
listaSET_dr2 = list(set(lista_dr2))
listaSET_dr2.sort()
#print listaSET_dr2  # debug line
#Fim da volta 2 ~1 min (pcpaulo)

# fazendo os "Vdot's" e jah alocando-os na lista correspodnente
list_dr2_different_and_frequency = [[ele] for ele in (listaSET_dr2)]
for i in range(N):
    for j in xrange(i+1,N):
        Dx = dl*(i-int(N_X)*int(i/N_X)) - dl*(j-int(N_X)*int(j/N_X));
        Dy = -dl*int(i/N_X) + dl*int(j/N_X)
        dr2 = Dx*Dx + Dy*Dy
        #print list_dr2_different_and_frequency[listaSET_dr2.index(dr2)]    # debug line
        list_dr2_different_and_frequency[listaSET_dr2.index(dr2)].append(0)
#Fim da volta 3 ~ 3 min 30 s (pcpaulo)


#**** writing out file ****#
file_out_name = "dr2_distribution-%ix%i.ddm" % (N_X,N_Y)
file_out = open(file_out_name,"w")

stp = "# %ix%i = %i vectors (or latice linear size)\n" % (N_X,N_Y,N_vectors)
stp += "# dl = %i is the lattice characteristic length\n" % (dl)
stp += "# Number_of_different_drs = %i\n" % (len(list_dr2_different_and_frequency))
stp += "# Number_total_of_drs = %i\n" % (N_total_of_drs)
stp += "# dr2[px2] Frequency[#] dr[px] (dr_{i}-dr_{i-1})[px] | where \"i\" is the line number: \"dr=%i\" is the \"dr_{i=1}\"\n"%(list_dr2_different_and_frequency[0][0])

ele = list_dr2_different_and_frequency[0]
stp += "%i %i %f NaN\n" % (ele[0],(len(ele)-1),np.sqrt(ele[0]))
for i in xrange(1,len(list_dr2_different_and_frequency),1):
    ele = list_dr2_different_and_frequency[i]
    ele2 = list_dr2_different_and_frequency[i-1]
    stp += "%i %i %f %f\n" % (ele[0],(len(ele)-1),np.sqrt(ele[0]),(np.sqrt(ele[0])-np.sqrt(ele2[0])))
stp += "#END"

file_out.write(stp)
file_out.close()
#**** ****#
#exit()
