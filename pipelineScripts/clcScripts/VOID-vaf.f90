program vaf
  implicit none
  integer :: N_dr_sizes, N_Vdots, i,j,k,l,index
  character :: trash,q

  real, allocatable :: VectorField(:,:)
  
  integer, allocatable :: dr2_sizes(:), dr_freq(:)
  real, allocatable :: sum_Vdots_for_dr2_sizes(:), Cvv(:)
  
  real, allocatable :: Vdots(:)
  integer, allocatable :: Vdots_mask(:)

  integer :: index_dr_sizes !is the bin (i.e. the "dr2_size") that a "Vdot" belong --> those are (will be) the "Vdots_mask" values 
  integer :: dr2, dx, dy

  integer :: index_Vdots, dr_bin_of_Vdot, N_Vdots_for_dr_size 
  real :: Vdot, sum_V2, mean_V2, mean_Vdot_for_dr_size
  
  integer, parameter :: N_frames = <NUMBER_OF_FRAMES>  
  integer, parameter :: N_vectors = <NUMBER_OF_VECTORS>
  integer, parameter :: N_X = <LATTICE_X_LENGTH>
  integer, parameter :: N_Y = <LATTICE_Y_LENGTH>
  integer, parameter :: dl = <LATTICE_CHARACTERISTIC_LENGTH>
  
  integer, parameter :: len_name_fluctuation_files = <LEN_NAME_FLUCTUATION_FILES> 
  !integer, parameter :: len_name_fluctuation_files = 90   ! debug line
  character(len=len_name_fluctuation_files) :: name_fluctuation_file
  character(len=len_name_fluctuation_files), dimension(N_frames) :: list_of_fluctuation_files_names
  !integer, parameter :: len_name_Cvv_tmp_files = <LEN_NAME_CVV_F90_TMP_FILES>
  integer, parameter :: len_name_Cvv_tmp_files = 16
  character(len=len_name_Cvv_tmp_files) :: name_Cvv_tmp_file
  character(len=len_name_Cvv_tmp_files), dimension(N_frames) :: list_of_Cvv_tmp_files_names


  !**** loading (from ".tmp" file) the fluctuation files names that will be used to calc Cvv and the out "Cvv_tmp" names ****!
  open(10,file='f90_in_files_names.tmp')
  do j=1,N_frames
     !print*, "gello"   !debug line
     read(10,"(A,X,A)") list_of_fluctuation_files_names(j), list_of_Cvv_tmp_files_names(j)  ! name of the file that have the fluctu field data to be used to Cvv calculations
     !print*, list_of_fluctuation_files_names(j), list_of_Cvv_tmp_files_names(j)    !debug line
  enddo
  close(10)
  !**** ****!
  !stop

  !**** loading dr2_distribution header ****!
  !open(20,file="dr2_distribution-85x64.ddm")
  !open(20,file="dr2_distribution-81x60.ddm")
  open(20,file="dr2_distribution-77x56.ddm")
  read(20,*) ; read(20,*) 
  read(20,*) trash, trash, trash, N_dr_sizes
  read(20,*) trash, trash, trash, N_Vdots
  read(20,*)
  !print*, N_dr_sizes, N_Vdots   ! debug line
  close(20)
  !**** ****!
  !stop

  !**** allocating real and integer arrays ****!
  allocate(dr2_sizes(N_dr_sizes),dr_freq(N_dr_sizes),sum_Vdots_for_dr2_sizes(N_dr_sizes),Cvv(N_dr_sizes))
  allocate(VectorField(N_vectors,4))
  allocate(Vdots(N_Vdots),Vdots_mask(N_Vdots))
  !**** ****!
  !stop

  !**** loading dr2_distribution data ****!
  !open(20,file="dr2_distribution-85x64.ddm")
  !open(20,file="dr2_distribution-81x60.ddm")
  open(20,file="dr2_distribution-77x56.ddm")
  read(20,*) ; read(20,*) ; read(20,*) ; read(20,*) ; read(20,*)
  do i=1,N_dr_sizes
     read(20,*) dr2_sizes(i), dr_freq(i)
     !print*, dr2_sizes(i), dr_freq(i), i    ! debug line
  enddo
  !**** ****!
  !stop
  
  !**** reading the "Vdots_mask" values from file ****! -----> ~6 seg to read file (in pcpaulo)
  !print*, " >>> from 'vaf_f90.ex' : loading Vdots mask values"    ! progress check statment
  !open(30,file="Vdots_mask_value_85x64.vaff90")
  !open(30,file="Vdots_mask_value_81x60.vaff90")
  open(30,file="Vdots_mask_value_77x56.vaff90")
  do i=1,N_Vdots
     read(30,*) Vdots_mask(i)
  enddo
  close(30)
  !**** ****!
  !stop

  !**** Computing the Vdots_mask index ****! ---->  ~42 seg to calculate  (in pcpaulo)
  !index_Vdots = 0
  !do i=0,N_vectors-1
  !   do j=i+1,N_vectors-1
  !      dx = dl*(i-int(N_x)*int(i/N_x)) - dl*(j-int(N_x)*int(j/N_x));
  !      dy = -dl*int(i/N_x) + dl*int(j/N_x)
  !      dr2 = dx*dx + dy*dy
  !      do l = 1,N_dr_sizes
  !         index_dr_sizes = l
  !         if (dr2.eq.dr2_sizes(index_dr_sizes)) then
  !            index_Vdots = index_Vdots + 1
  !            Vdots_mask(index_Vdots) = index_dr_sizes
  !            !print*, dr2, index_Vdots, index_dr_sizes    ! debug line
  !         endif
  !      enddo
  !   enddo
  !enddo
  !**** ****!
  !stop

  !**** making "Vdots_mask" file ****! -----> ~4 seg to write file (in pcpaulo)
  !open(30,file="Vdots_mask_value_85x64.vaff90")
  !open(30,file="Vdots_mask_value_81x60.vaff90")
  !open(30,file="Vdots_mask_value_77x56.vaff90")
  !do i=1,N_Vdots
  !   write(30,*) Vdots_mask(i)
  !enddo
  !close(30)
  !**** ****!
  !stop

  !**************************************************!
  !***** (A) looping over in (fluctu vff) files *****!
  !**************************************************!
  do k=1,N_frames

     !**** defining in and out files names ****!
     name_fluctuation_file = list_of_fluctuation_files_names(k)
     name_Cvv_tmp_file = list_of_Cvv_tmp_files_names(k)
     !**** ****!
     !stop

     !**** progress check statment ****!
     print*, " >>> from 'vaf_f90.ex' : calculating frame ", name_fluctuation_file
     !**** ****!
     !stop
     
     !**** reading fluctuation field from file ****!
     open(40,file=name_fluctuation_file) 
     read(40,*) ; read(40,*) 
     do i=1,N_vectors
        read(40,*) VectorField(i,1), VectorField(i,2), VectorField(i,3), VectorField(i,4)
        !print*, i, VectorField(i,1), VectorField(i,2), VectorField(i,3), VectorField(i,4)    ! debug line
     enddo
     close(40)
     !**** ****!
     !stop

     !**** calculatig the Vdot's****!
     sum_V2=0.0 ; mean_V2 = 0.0
     index_Vdots = 0
     dr_bin_of_Vdot = 0
     sum_Vdots_for_dr2_sizes = 0.
     do i=1,N_vectors
        sum_V2 = sum_V2 + VectorField(i,3)*VectorField(i,3) + VectorField(i,4)*VectorField(i,4)
        do j=i+1,N_vectors
           Vdot = VectorField(i,3)*VectorField(j,3) + VectorField(i,4)*VectorField(j,4)

           dx = int(VectorField(i,1)-VectorField(j,1));
           dy = int(VectorField(i,2)-VectorField(j,2));
           dr2 = dx*dx + dy*dy
           
           index_Vdots = index_Vdots + 1
           
           dr_bin_of_Vdot = Vdots_mask(index_Vdots)
           sum_Vdots_for_dr2_sizes(dr_bin_of_Vdot) = sum_Vdots_for_dr2_sizes(dr_bin_of_Vdot) + Vdot
        enddo
     enddo
     !print*, 1, Vdots(1); print*, index_Vdots, Vdots(index_Vdots) ; print*, size(Vdots)    ! debug line
     mean_V2 = sum_V2/N_vectors
     !print*, sum_V2, N_vectors, mean_V2, N_dr_sizes    ! debug line
     !**** ****!
     !stop

     if (index_Vdots.ne.N_Vdots) then !security routine
        print*, "ERROR:   total number of 'index_Vdots' is diferent from expected number of 'N_Vdots': something is wrong"
        print*, "         Expected 'N_Vdots' number = ", N_Vdots
        print*, "         total number of 'index_Vdots' = ", index_Vdots
        print*, "code stoped!"
        stop
     endif

     !**** Computing the Cvv ****!
     Cvv(:) = 0.
     N_Vdots_for_dr_size = 0
     mean_Vdot_for_dr_size = 0.
     do index = 1,N_dr_sizes
        N_Vdots_for_dr_size = dr_freq(index)
        mean_Vdot_for_dr_size = sum_Vdots_for_dr2_sizes(index)/N_Vdots_for_dr_size
        Cvv(index) = mean_Vdot_for_dr_size/mean_V2
     enddo
     !stop
     
     !**** writing Cvv data in file.tmp out ****!
     open(50,file=name_Cvv_tmp_file)
     write(50,*) "# dr[um] Cvv(dr)[ad.] N_pair(dr)[#]"
     do i=1,N_dr_sizes
        write(50,*) 0.645*sqrt(real(dr2_sizes(i))), Cvv(i), dr_freq(i)
     enddo
     write(50,*) "#END"
     close(50)
     !**** ****!
     !stop
     
  enddo
  !**************************************************!
  !***** END (A) ************************************!
  !**************************************************!
  !stop
  
end program vaf
