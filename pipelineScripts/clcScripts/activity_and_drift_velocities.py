# the version 1.0 of this code is equal to "activity-and-drift-velocity.py" from "../resultados_parcias.../"
import os
import sys
import numpy as np
import subprocess as subp
import matplotlib.pyplot as plt
import scipy.optimize #.curve_fit

print "\n >>> from adv.py:  "

#******************************************************************#
#**** Section 0: defining in variables and defining input data ****#
#******************************************************************#

#**** in variables ****#
d0 = sys.argv[1] ; # d0 = <d0> = linage-movie directory
local_path_to_field = sys.argv[2]    # data files directory path
local_path_to_fluctu = sys.argv[3]   # data files directory path
hyst_or_norm = sys.argv[4]   # "hyst" or "norm" images
extension = sys.argv[5]   # tif, bmp, png ...
number_of_frames = int(sys.argv[6])
number_of_vectors = int(sys.argv[7])    # after vff
local_path_to_adv = sys.argv[8]
aux_adv_script = sys.argv[9]
path_to_pipeline = sys.argv[10]
absolut_time_correction_factor_in_hours = float(sys.argv[11]) ; tcf = absolut_time_correction_factor_in_hours

label = (d0+"-"+hyst_or_norm).replace("_aux","") # "hacat-m01-hyst"
label_out_files = label+"-piv"+extension+"-vff"  # "hacat-m01-hyst-pivtif-vff" prefixo(radical) nome(label) dos arquivos
input_data_directory = local_path_to_field
#**** ****#
#exit()

#**** Geting list of files ("field_frames.vff") to be used ****#
string_to_list_field_frames = "ls %s*.vff" % (input_data_directory)
#os.system(string_to_list_field_frames) #debug line
f_string = subp.check_output(string_to_list_field_frames,shell=True) # string of all ".vff (fields)" files 
#print f_string    # debug line
field_files_list = f_string.split("\n")  ;  field_files_list.remove("")
#print field_files_list    # debug line
#** security routine:
if len(field_files_list) != number_of_frames:     #security routine
    print "ERROR:   number of in \".vff\" files is diferent from expected number of frames"
    print "         Expected number of frames = ", number_of_frames
    print "         number of in files =", len(field_files_list)
    print "code stoped!"
    exit()
#**** ****#
#exit()

#**********************************************************************#
#**** END : Section 0 *************************************************#
#**********************************************************************#
#exit()
print

#****************************************************#
#**** Section 1 : pre loop necessary definitions ****#
#****************************************************#

#**** defining the lits that will cointain the activity and fluctuation in activity ****#
activity_list = []
fluctuation_in_activity_list = []
#**** ****#
#exit()

#**** defining the list that will cointain the drift speed ****#
drift_speed_list = []
#**** ****#
#exit()

#**** defining the list that will cointaing the root mean squared speed ****#
vrms_list = []
#**** ****#
#exit()

#**** defining frames time list ****#
time_frames_list = []
#**** ****#
#exit()

#****************************************************#
#**** END : Section 1 *******************************#
#****************************************************#
#exit()
#print

#*********************************************************************#
#**** Section 3 : LOOP over input data files and adv calculations ****#
#*********************************************************************#
for in_file_name in field_files_list:

    #**** loding input data ****#
    in_file = open(in_file_name,'r')
    data = in_file.read() ;
    #print data    # debug line
    data=data.split('\n') ; data.remove(data[0]) ; data.remove("#END")
    #print data  # debug line
    if len(data) != number_of_vectors:                  # security routine
        print "Error: The number of vectors (lines=len(input_data)) in input file is diferent from expected number of vectors"
        print "       Expected number of vectors: %i" % (number_of_vectors)
        print "       len(input data): %i" % (len(data))
        print " ----->  Code stoped!"
        exit()
    #**** ****#
    #exit()

    #**** making/defining the block_data ****#
    nol = len(data) # = number_of_lines    
    block_data = [[0. for j in range(6)] for i in range(nol)]
    for i in range(nol):
        data[i]=data[i].split(' ')  ;  data[i].remove("")
        #print data[i]    # debug line
        #** security routine:
        if len(data[i]) != 6:
            print " deu merda no len(data[%i]) ---> code stoped!" % (i)
            exit()
        for j in range(6):
            block_data[i][j] = float(data[i][j])
    #print block_data[0] ; print block_data[nol-1] ; print len(block_data)  # debug line
    #**** ****#
    #exit()

    #**** geting the frame number to write Cvv(dr*)=0 file (and for other files) ****#
    frame_number = in_file_name.replace("%s%s"%(local_path_to_field,label),"")
    frame_number = frame_number.replace("-piv%s.vff"%(extension),"")
    #print frame_number    # debug line
    #**** ****#
    #exit()

    #**** defining the absolutu "time" in hours for the frame ****#
    time = (int(frame_number)*0.25+tcf)
    #print time   # debug line
    #**** ****#
    #exit()
    
    #**** defining vx, vy and vr using in file data ****#
    nov = number_of_vectors
    vx = np.zeros(nov) ; vy = np.zeros(nov) ; vr = np.zeros(nov)
    for index in range(nov):
        line_data = block_data[index]
        vx[index] = line_data[2] ; vy[index] = line_data[3] ; vr[index] = line_data[4]
    #print vx[0], vy[0], vr[0] ; print vx[nov-1], vy[nov-1], vr[nov-1]  # debug line
    #print np.sqrt(vx[0]*vx[0] + vy[0]*vy[0]), vr[0]  # debug line
    #**** ****#
    #exit()
    
    #**** calculating activity, drift and vrms values ****#
    activity = 0. ;
    drift = 0. ;
    vrms = 0. ;

    mean_vx = 0. ; mean_vy = 0. ;

    N = nov ;
    
    mean_vx = sum(vx)/N ;
    mean_vy = sum(vy)/N ;
    
    activity = sum(vr)/N ;
    drift = np.sqrt(mean_vx*mean_vx + mean_vy*mean_vy) ;
    vrms = np.sqrt(sum(vr*vr)/N)
    #print time, activity, drift, vrms    # debug line
    #**** ****#
    #exit()

    #**** calculating activity standard deviation (this is actualy the fluction in the velocity module) ****#
    stdv_activity = 0. 
    for j in range(N):
        stdv_activity = stdv_activity + (vr[j]-activity)*(vr[j]-activity)
    stdv_activity = np.sqrt(stdv_activity/(N-1))
    #print time, activity, stdv_activity, drift, vrms  # debug line
    #**** ****#
    #exit()

    #**** appending adv calculated data to theyer lists ****#
    if d0 == "cal27-m01" and frame_number == "0130":
        activity_list.append(float("NaN"))
        fluctuation_in_activity_list.append(float("NaN"))    
        drift_speed_list.append(float("NaN"))
        vrms_list.append(float("NaN"))
    else:
        activity_list.append(activity)
        fluctuation_in_activity_list.append(stdv_activity)    
        drift_speed_list.append(drift)
        vrms_list.append(vrms)
    #**** ****#
    #exit()

    #****appending time tho its list ****#
    time_frames_list.append(time)
    #**** ****#
    #exit()
    
#*********************************************************************#
#**** END : Section 3 ************************************************#
#*********************************************************************#
#exit()
#print
    
#***************************************************#
#**** Section 4 : saving the adv out data files ****#
#***************************************************#

#**** writing the activity out file ****#
file_out_1 = open(local_path_to_adv+label_out_files+'-activity.adv','w')
file_out_1.write("# time is the abslut time in relation to the begining of the photo shooting\n")
file_out_1.write(r"# activity is the monolayer mean speed = the average of the velocities modules = V = activity = $(1/N)*(\sum_{i=1}^{N}(|v_i|))$"+"\n")
file_out_1.write(r"# the fluctuation in activity is only the stdv of this quantity = stdv = $\sqrt{ \frac{(\sum_{i=1}^{N}(|v_i|-activity)^2)}{N-1} }$"+"\n")
file_out_1.write('# time[h]   activity[um/h]   fluctuation_in_activity[um/h]\n')
for i in range(len(time_frames_list)):
    time = time_frames_list[i]
    activity = activity_list[i]
    fluctuation_in_activity = fluctuation_in_activity_list[i]
    file_out_1.write("%f %f %f\n" % (time, activity, fluctuation_in_activity))
file_out_1.write('#END')
file_out_1.close()
#**** ****#
#exit()

#**** writing the drift speed out file ****#
file_out_2 = open(local_path_to_adv+label_out_files+'-drift_speed.adv','w')
file_out_2.write("# time is the abslut time in relation to the begining of the photo shooting\n")
file_out_2.write(r"# drift speed is the module of the mean vectorial velocity = V = drift_speed = $|(1/N)*(\sum_{i=1}^{N}(v_i))|$"+"\n")
file_out_2.write('# time[h]   drif_speed[um/h]\n')
for i in range(len(time_frames_list)):
    time = time_frames_list[i]
    drift_speed = drift_speed_list[i]
    file_out_2.write("%f %f\n" % (time, drift_speed))
file_out_2.write('#END')
file_out_2.close()
#**** ****#
#exit()

#**** writing the vrms out file ****#
file_out_3 = open(local_path_to_adv+label_out_files+'-vrms.adv','w')
file_out_3.write("# time is the abslut time in relation to the begining of the photo shooting\n")
file_out_3.write(r"# vrms is the root mean squared speed (velocity) = vrms = $\sqrt{ (1/N) * (\sum_{i=1}^{N}|v_i|^2) }$"+"\n")
file_out_3.write("# vrms is similar to kinetic energy, but one should be VERY carefull when doing such direct comparison in the (epithelial) biology realm\n")
file_out_3.write('# time[h]   vrms[um/h]\n')
for i in range(len(time_frames_list)):
    time = time_frames_list[i]
    vrms = vrms_list[i]
    file_out_3.write("%f %f\n" % (time, vrms))
file_out_3.write('#END')
file_out_3.close()
#**** ****#
#exit()

#***************************************************#
#**** END : Section 4 ******************************#
#***************************************************#
#exit()
#print

#*********************************************************#
#**** Section 5 : ploting the adv calculations graphs ****#
#*********************************************************#

#**** opning the file that will save the exponential fit parameters for the activity cut curve ****#
file_exp_fit_name = "%sactivity-exponential-fit-parameters.adv" % (local_path_to_adv)
#print file_exp_fit_name # debug line
file_exp_fit = open(file_exp_fit_name,"w")
stp = "# fit function f(r) = A*exp(-B*r)\n"
stp += "# fit functions is \"scipy.optimize.curve_fit(lambda t,a,b: a*np.exp(-b*t),  x,  y,  p0=(1.0, 0.01))\"\n"
stp += "# this function returns A B and more four number that are related with fit quality (google it for more info)\n"
stp += r"# \"1/B\" is equal to the $\tau_{act}$ thats equal to the characteristic decay time for the speed (activity)"+"\n"
stp += "# tcf = time correction factor = %fh (for this movie)\n"%(tcf)
stp += "# frame_number[15min+tcf]  time[h]  A[ad]  B[1/um]  c[?]  d[?]  e[?]  f[?]\n"
file_exp_fit.write(stp)
#**** ****#
#exit()

#**** defining the activity start fiting time list for each linage ****#
strat_fiting_time = 1.0
start_fiting_time_list = []
start_fiting_time_list.append(["hacat-m01",30.0])
start_fiting_time_list.append(["hacat-m02",21.0])
start_fiting_time_list.append(["hacat-m03",15.0])
start_fiting_time_list.append(["hacat-m04",1.0])
start_fiting_time_list.append(["hacat-m05",15.0])
start_fiting_time_list.append(["cal27-m01",12.0])
start_fiting_time_list.append(["cal27-m02",20.0])
start_fiting_time_list.append(["cal27-m03",16.0])
start_fiting_time_list.append(["scc9-m01",15.0])
start_fiting_time_list.append(["scc9-m02",15.0])
start_fiting_time_list.append(["scc9-m03",15.0])
start_fiting_time_list.append(["fibroblastos-m01",45.0])
for ele in start_fiting_time_list:
    #print ele  # debug line
    if d0.replace("_aux","") in ele:
        #print ele[1]  #debug line
        start_fiting_time = ele[1]
        break
#**** ****#
#exit()

#**** defining the activity fit region ****#
start_fiting_time_index = time_frames_list.index(start_fiting_time)
x = time_frames_list[start_fiting_time_index:]
y = activity_list[start_fiting_time_index:]
#**** ****#
#exit()

#**** making the fit of the activity fit region ****#
x = np.array(x)
y = np.array(y)
if d0 == "cal27-m01":
    for i in range(len(y)):
        if x[i] == 33.00:
            #print y[i] #debug line
            y[i] = (y[i-1]+y[i+1])/2
            break
#print x ; print y    # debug line
fit_ex = scipy.optimize.curve_fit(lambda t,a,b: a*np.exp(-b*t),  x,  y,  p0=(1.0, 0.01))
ele1 = fit_ex[1]
ele1_0 = ele1[0] ; ele1_1 = ele1[1]
ele = fit_ex[0]
#print ele, type(ele)   # debug line
a = ele[0] ; b = ele[1]
#print "a%i=%f;b%i=%f;"%(int(frame_number),a,int(frame_number),b)  # debug line
file_exp_fit.write("%s %f %f %f %.12f %.12f %.12f %.12f\n" % (frame_number, time, a, b, ele1_0[0], ele1_0[1], ele1_1[0], ele1_1[1]))
file_exp_fit.write("#END")
file_exp_fit.close()
xp_fit_exp = np.linspace(x[0]-5,x[len(x)-1])
yp_fit_exp = a*np.exp(-b*xp_fit_exp)
tau_activity = (1/b)
#**** ****#
#exit()

#**** writing the fitted activity out file ****#
file_out_4 = open(local_path_to_adv+label_out_files+'-fitted-activity.adv','w')
file_out_4.write("# time is the abslut time in relation to the begining of the photo shooting\n")
file_out_4.write(r"# activity is the monolayer mean speed = the average of the velocities modules = V = activity = $(1/N)*(\sum_{i=1}^{N}(|v_i|))$"+"\n")
file_out_4.write('# this files holds the fitted activty information: $1 = time, $2 = activity but only fited region\n')
file_out_4.write('# to plot the fit: \"xp_fit_exp = np.linspace(x[0]-5,x[len(x)-1])\" and \"yp_fit_exp = a*np.exp(-b*xp_fit_exp)\"\n')
file_out_4.write("# fited_function = f(time) = A*exp(-B*time)\n")
file_out_4.write("# f(time) = %2.2f*exp(-%2.3f*time)\n"%(a,b))
file_out_4.write("# A = %2.2f[um/h]\n"%(a))
file_out_4.write("# B = %2.3f\n"%(b))
file_out_4.write('# tau_activity = (1/B) = %2.3fh\n'%(tau_activity))
file_out_4.write("# N = number_of_points_in_fit_region = %i\n"%(len(x)))
file_out_4.write('# time[h]   activity[um/h]\n')
for i in range(len(x)):
    time = x[i]
    activity = y[i]
    #print i, time, activity   # debug line
    file_out_4.write("%f %f\n" % (time, activity))
file_out_4.write('#END')
file_out_4.close()
#**** ****#
#exit()

#**** ploting the activity graph ****#
plt.rcParams.update({'font.size': 17})
x0 = time_frames_list
y0 = activity_list
#for i in range(len(x)): print x[i], y[i]  # debug line
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x0,y0,"g:",lw=2,label="full curve")
ax.plot(x,y,"g-",lw=2,label="fitted region")
ax.plot(xp_fit_exp,yp_fit_exp,color="orange",ls="--",lw=2,label="f(x)=%2.2f*exp(-%2.3fx)"%(a,b))
ax.set_ylim(0,28)
ax.set_xlim(-1.7,52.5)
if d0 == "fibroblastos-m01":
    ax.set_xlim(15.0,75)
ax.set_ylabel(r"activity [$\mu m/h$]")
ax.set_xlabel("time [h]")
ax.set_title(d0)
if d0 == "fibroblastos-m01":
    plt.text(54.0,17.3,r"$\tau_{act}$ = %2.3fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
else:
    plt.text(34.0,17.3,r"$\tau_{act}$ = %2.3fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
plt.legend(fontsize=15)
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-activity-adv.png" % (local_path_to_adv,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the combined adv graph ****#
plt.rcParams.update({'font.size': 17})
x = time_frames_list
y1 = activity_list
y2 = fluctuation_in_activity_list
y3 = drift_speed_list
y4 = vrms_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y1,"g-",label="activity") #%s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.plot(x,y2,"k-",label="fluctu activity") #%s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.plot(x,y3,"r-",label="drift speed") #%s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.plot(x,y4,"b-",label="vrms") #%s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.set_ylim(0,28)
#ax.set_xlim(0,50)
ax.set_xlim(-1.7,52.5)
if d0 == "fibroblastos-m01":
    ax.set_xlim(-1.7,73)    
ax.set_ylabel(r"speed [$\mu m/h$]")
ax.set_xlabel("time [h]")
ax.set_title(d0)
plt.legend(fontsize=15)
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-combined-adv.png" % (local_path_to_adv,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#*********************************************************#
#**** END : Section 5 ************************************#
#*********************************************************#
#exit()
print

print "\n END adv.py <<<  "
exit()
