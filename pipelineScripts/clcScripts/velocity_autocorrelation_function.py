# the version 1.0 of this script is a copy of "autocorrelation_function-1.8.py" from 'results_parci/velocity_coreelation/'
# in files: 'dr2_distribution.dat'
#           'linage-movie-treatment####.pivtif'
# out file: none --> print on the screan 'dr[um] Cvv(dr)[ad.] N_pair(dr)[#]'
# If this out data if you put on a file and run "making_dr_binSIZE_Cvv_files.py" you get a 'file out' with a binage in 'dr' with some given value
# The version 1.1 don't print images and takes 3min40s to run, so a 200 stacks movie would take about 12h to run
# the version 1.2 if not adding printing images, will take teh same time
# Uso:
# python velocity_autocorrelation_function-2.1.py <path_to_files> <binsize>

import sys
import os
import numpy as np
import subprocess as subp
import matplotlib.pyplot as plt
import scipy.optimize #.curve_fit

print "\n >>> from vaf.py:  "

#**** defining/geting necessary variables ****#
d0 = sys.argv[1] ; # d0 = <d0> = linage-movie directory
dr_bin_size = sys.argv[2]
local_path_to_fluctu = sys.argv[3]    # data files directory path
hyst_or_norm = sys.argv[4]   # "hyst" or "norm" images
extension = sys.argv[5]   # tif, bmp, png ...
number_of_frames = int(sys.argv[6])
number_of_vectors = int(sys.argv[7])    # after vff
local_path_to_vaf = sys.argv[8]
vaf_f90_executable = sys.argv[9]
path_to_pipeline = sys.argv[10]
absolut_time_correction_factor_in_hours = float(sys.argv[11]) ; tcf = absolut_time_correction_factor_in_hours

label = (d0+"-"+hyst_or_norm).replace("_aux","")
input_data_directory = local_path_to_fluctu # = <PATH_TO_VFF_FLUCTU>
N_frames = number_of_frames
N = number_of_vectors  #number of lines of "in file" = number of piv windows after vff 
NN = 2015 #number of different "dr's" sizes for a 77x56 grid
N_vectors = N
#print input_data_directory, label, N_frames, N_vectors  # debug line
#**** ****#
#exit()

#### ********** geting the bin size ************ ####
#** Security routines
if len(sys.argv) != 12:
    print "\n  Error: wrong number of input arguments: ver 'uso' no header do programa --> exit() \n"
    exit()
a=0 ; list = ['16','15','11','20']
for ele in list:
    if sys.argv[2] == ele:
        a+=1
if a != 1:
    print "\n  Error: non valued bin size: ver 'uso' no header do programa --> exit() \n"
    exit()
####*********************************************####
#exit()

#**** Geting list of files ("fluctu_frames.vff") to be used ****#
string_to_list_fluctu_field_frames = "ls %s*-fluctu.vff" % (input_data_directory)
#os.system(string_to_list_fluctu_field_frames) #debug line
f_string = subp.check_output(string_to_list_fluctu_field_frames,shell=True) # string of all "-fluctu.vff" files 
#print f_string    # debug line
string_of_fluctu_files_list = f_string
#print string_of_fluctu_files_list    # debug line
#**** ****#
#exit()

#**** geting the number of character (string len) of fluctu files name+path ****#
list_of_fluctu_files = string_of_fluctu_files_list.split("\n") ; list_of_fluctu_files.remove("")
#print list_of_fluctu_files     # debug line
#len_fluctu_files_name = len(list_of_fluctu_files[0])
#print len_fluctu_files_name   # debug line
#**** ****#
#exit()

#**** geting the frames indexes to be write out to f90 ****#
frames_indexes = string_of_fluctu_files_list.replace("%s%s" % (input_data_directory,label),"")
frames_indexes = frames_indexes.split("-pivtif-fluctu.vff\n") ; frames_indexes.remove("")
#print frames_indexes
#**** ****#
#exit()

#**** finishing the fluctuatin files string by ading the tmp_Cvv_f90 also and making tmp_f90 list to be loaded after execute "vaf_f90.ex" ****#
#print string_of_fluctu_files_list
list_of_Cvv_f90_tmp_files = []
for index in frames_indexes:
    Cvv_f90_tmp_file_name = "Cvv_f90-%s.tmp" % (index)
    list_of_Cvv_f90_tmp_files.append(Cvv_f90_tmp_file_name)
    #len_Cvv_f90_tmp_file_name = len(Cvv_f90_tmp_file_name)
    string_of_fluctu_files_list = string_of_fluctu_files_list.replace("\n"," %s<new_line>" % (Cvv_f90_tmp_file_name),1)
#print string_of_fluctu_files_list   # debug line
string_of_fluctu_files_list = string_of_fluctu_files_list.replace("<new_line>","\n")
string_of_fluctu_files_list = string_of_fluctu_files_list.replace("%s/"%(d0),"../../../%s/"%(d0))
#print string_of_fluctu_files_list    # debug line
#print len_Cvv_f90_tmp_file_name     # debug line
#**** ****#
#exit()

#**** saving files_names to be open with f90 to out file ****#
file_out_name = "%sf90_in_files_names.tmp"%(local_path_to_vaf)
file_out = open(file_out_name,"w")
file_out.write(string_of_fluctu_files_list)
file_out.close()
#**** ****#
#exit()

#**** calling ddm.py to make the distribution data file necessary for vaf.f90 ****#
#NOT IN USE for now --> data files alredy calculated
#string_to_call_ddm = "echo python dr2_distribution_maker.py"
#os.system(string_to_call_ddm)
#**** ****#
#exit()

#**** calling vaf.f90 ****#
#string_to_cal_vaf_f90 = "gfortran -O3 vaf-2.0.f90 -o vaf_f90.ex; ./vaf_f90.ex"
#string_to_cal_vaf_f90 = "gfortran -O3 vaf-1.8.f90 -o vaf_f90.ex"
string_to_cal_vaf_f90 = "cd %s ; ./%s ; cd %s" % (local_path_to_vaf,vaf_f90_executable,path_to_pipeline)
os.system(string_to_cal_vaf_f90)
#**** ****#
#exit()

#**** removing ".tmp" file ****#
string_to_remove_f90_files_name_tmp = "rm %s" % (file_out_name)
os.system(string_to_remove_f90_files_name_tmp)
#**** ****#
#exit()

#**** defining string that will recive the exp fit values for the gt0 cut plots****#
string_fit_ab = ""
#**** ****#
#exit()

#**** opning the string_fit_ab tmp file to clc.py read and modifing bash script gnu gt0cut  ****#
file_tmp_string_fit_ab_name = "%sstring-fit-ab.tmp" % (local_path_to_vaf)
#print file_tmp_string_fit_ab_name # debug line
file_tmp_string_fit_ab = open(file_tmp_string_fit_ab_name,"w")
#**** ****#
#exit()

#**** opning the file that will save the exponential fit parameters for the gt0 cut ****#
file_exp_fit_name = "%sCvv-gt0cut-exponential-fit-parameters.vaf" % (local_path_to_vaf)
#print file_exp_fit_name # debug line
file_exp_fit = open(file_exp_fit_name,"w")
stp = "# fit function f(r) = A*exp(-B*r)\n"
stp += "# fit functions is \"scipy.optimize.curve_fit(lambda t,a,b: a*np.exp(-b*t),  x,  y,  p0=(1.0, 0.01))\"\n"
stp += "# this function returns A B and more four number that are related with fit quality (google it for more info)\n"
stp += "# \"A\" should be equals to 1 if perfect feat, since Cvv(r=0) is by definition equals to 1\n"
stp += "# \"1/B\" is equal to the velocity correlation length\n"
stp += "# tcf = time correction factor = %fh (for this movie)\n"%(tcf)
stp += "# frame_number[15min+tcf]  time[h]  A[ad]  B[1/um]  c[?]  d[?]  e[?]  f[?]\n"
file_exp_fit.write(stp)
#**** ****#
#exit()

#**** opning the file that will save the X2 fit parameters for the Cvv minimum values ****#
file_x2_fit_name = "%sCvv-minvalues-x2-fit-parameters.vaf" % (local_path_to_vaf)
#print file_x2_fit_name # debug line
file_x2_fit = open(file_x2_fit_name,"w")
stp = "# fit function f(r) = A*r^2 + B*r + C\n"
stp += "# fit functions is \"np.polyfit(x,y,2)\"\n"
stp += "# this function returns \"coeficients(A, B, C), residuals, rank, singular_values[len=rank], rcond\" (google it for more info)\n"
stp += "# this fiting procedures is having a lot of problems and B>=0 is one of them (shuld be <0) \n"
stp += "# Swirl characteristic size is obtained by the derivative formula \"r_min = -B*0.5/A\"\n"
stp += "# tcf = time correction factor = %fh (for this movie)\n"%(tcf)
stp += "# frame_number[15min+tcf]  time[h]  Number_of_fited_points[#]  A[ad]  B[ad]  C[ad]  residue[?]  rank[#]  singular_values(len=rank)[?]  rcond[#]\n"
file_x2_fit.write(stp)
#**** ****#
#exit()

#**** defining the lits that will cointain the velocity correlation length ****#
velocity_correlation_lengths_list = []
#**** ****#
#exit()

#**** defining the list that will cointain the swirl sizes ****#
swirls_sizes_list = []
swirls_sizes_valid_and_invalid_list = []
Cvv_local_minima_list = []
reasons_to_eliminate_swirl_size_list = []
#**** ****#
#exit()

#**** defining the list that will cointaing the total degree of correlation ****#
total_degree_of_correlation_integrated_list = []
normalized_total_degree_of_correlation_integrated_list = []
dr_of_Cvv_eqt_0_list = []
#**** ****#
#exit()

#**** defining frames time list ****#
time_frames_list = []
#**** ****#
#exit()

print
### ********************************************************************************** ####
### Section CALCULATING THE BIN FILES AND EXPORTING THE NON BIN FILES IN PYTHON FORMAT ####
### ********************************************************************************** ####

#**********************************************************#
#***** Loop over frames: makig the out Cvv files **********#
#**********************************************************#
for index in range(N_frames):
    #**** defining the in and out files names
    name_in_file = local_path_to_vaf+list_of_Cvv_f90_tmp_files[index]
    name_out_file_1 = list_of_fluctu_files[index].replace(".vff","-Cvv.vaf")
    name_out_file_2 = list_of_fluctu_files[index].replace(".vff","-Cvv-drbin%s.vaf"%(sys.argv[2]))
    name_out_file_3 = list_of_fluctu_files[index].replace(".vff","-Cvv-gt0cut.vaf")
    name_out_file_4 = list_of_fluctu_files[index].replace(".vff","-Cvv-minvalues.vaf")
    
    name_out_file_1 = name_out_file_1.replace(local_path_to_fluctu,local_path_to_vaf)
    name_out_file_2 = name_out_file_2.replace(local_path_to_fluctu,local_path_to_vaf)
    name_out_file_3 = name_out_file_3.replace(local_path_to_fluctu,local_path_to_vaf)
    name_out_file_4 = name_out_file_4.replace(local_path_to_fluctu,local_path_to_vaf)
    #print name_in_file ; print name_out_file_1 ; print name_out_file_2 ; print name_out_file_3 ; print name_out_file_4   # debug line
    #**** ****#
    #exit()

    #**** loading vaf f90 calculations from ".tmp" file ****#
    in_file = open(name_in_file,"r")
    Cvv_data = in_file.read()
    Cvv_data = Cvv_data.split("\n") #data is a list now!
    #print Cvv_data[:10]       # debug line
    header_out_file_1 = Cvv_data[0]+"\n" ; header_out_file_1 = header_out_file_1.replace(" ","",1)
    #print header_out_file_1    # debug line
    Cvv_data.remove('') ; Cvv_data.remove(Cvv_data[0]) ; Cvv_data.remove(' #END')
    #print len(Cvv_data)    # debug line
    #print Cvv_data[:10] ; print Cvv_data[2010:]       # debug line
    if len(Cvv_data) != NN:     #security routine
        print "ERROR:   input 'Cvv_data' len is diferent from expected computed number (!= %s )" % (str(NN))
        print "         Expected computed dr's (or dr2's) number = ", NN
        print "         input 'Cvv_data' len =", len(Cvv_data)
        print "code stoped!"
        exit()
    #**** ****#
    #exit()

    #**** making the block_data to be bined ****# 
    block_data = [[0. for j in range(3)] for i in range(NN)]
    for i in range(NN):
        line_data = Cvv_data[i].split(" ")
        while len(line_data) > 3: line_data.remove("")
        block_data[i][0] = float(line_data[0])   # = dr_size[i]
        block_data[i][1] = float(line_data[1])   # = Cvv[i] 
        block_data[i][2] = int(line_data[2])   # = N_pair[i]
        #print block_data[i][0], block_data[i][1], block_data[i][2]    # debug line
    #**** ****#
    #exit()

    #*** open and writing file out 1 ***#
    file_out_1 = open(name_out_file_1,"w")
    file_out_1.write(header_out_file_1)
    for i in range(NN):
        file_out_1.write("%f %f %i\n" % (block_data[i][0], block_data[i][1], block_data[i][2]))
    file_out_1.write("#END")
    file_out_1.close()
    #*** ***#
    #exit()

    #*** changing N value to NN ***#
    # ou seja, N agora eh os bin sizes diferentes ao invez do numero de janelas
    N = NN
    #***  ***##

    #**** Defining dl size to be used for a given value ****# dl=dr bin size in microns
    if N_vectors == 5440:
        if sys.argv[2] == "16":
            dl = 16. ; npt=68 ; um=0 #smaller size that dr bin max and min have M>1
            
        if sys.argv[2] == "15":
            dl = 15. ; npt=73 ; block_data.remove(block_data[2469]) ; npt=npt-1 ; N=N-1 ; um=0 #smaller size that dr bin min have M>1
            
        if sys.argv[2] == "11":
            dl = 11. ; npt=99 ; block_data.remove(block_data[0]) ; block_data.remove(block_data[2468]) ; npt=npt-2 ; N=N-2 ; um=1 #dr bin max and min have M=1
            
        if sys.argv[2] == "20":
            dl = 20. ; npt=55 ; block_data.remove(block_data[2469]) ; npt=npt-1 ; N=N-1 ; um=0 #dr bin max have M=1
            
    elif N_vectors == 4312:
        if sys.argv[2] == "16":
            dl = 16. ; npt=61 ; um=0 #dr bin max and min have M=2 (M>1)
            
        if sys.argv[2] == "15":
            dl = 15. ; npt=65 ; um=0 #smaller size that dr bin min and max have M>1 (M=2)
            
        if sys.argv[2] == "11":
            dl = 11. ; npt=89 ; um=1 ; block_data.remove(block_data[0]) ; block_data.remove(block_data[2013]) ; npt=npt-2 ; N=N-2 ; #dr bin max and min have M=1
    
        if sys.argv[2] == "20":
            dl = 20. ; npt=49 ; um=0 #dr bin max and min have M=2 (M>1)
    else:
        print "find my line and understand me: closingthe code because N_vectros is not valid"
        exit()
    #**** ****#
    #exit()

    #**** defining variables ****#
    M = np.zeros(npt)
    sum_Cvv = np.zeros(npt) ; mean_Cvv = np.zeros(npt)
    sum_Cvv2 = np.zeros(npt) ; mean_Cvv2 = np.zeros(npt)
    stdv_Cvv = np.zeros(npt)
    
    sum_dr = np.zeros(npt) ; mean_dr = np.zeros(npt)
    sum_dr2 = np.zeros(npt) ; mean_dr2 = np.zeros(npt)
    stdv_dr = np.zeros(npt)
    #**** ****#
    #exit()

    #**** making the mean "Cvv", "dr" and related "stdv" ****#
    for i in xrange(1,120,1):
        for j in range(N):
            if (i-1)*dl < block_data[j][0] < i*dl:
                #print i-1-um, j, block_data[j][0], i*dl    # debug line
                sum_Cvv[i-1-um] += block_data[j][1]
                sum_Cvv2[i-1-um] += block_data[j][1]*block_data[j][1]
                
                sum_dr[i-1-um] += block_data[j][0]
                sum_dr2[i-1-um] += block_data[j][0]*block_data[j][0]
                
                M[i-1-um] += 1.
    #print M ; print M-1 ; print M/(M-1)  # debug line
    #for i in range(npt): print i+1, sum_dr[i], sum_dr[i]/M[i], M[i]   # debug line

    mean_Cvv = sum_Cvv/M ; mean_Cvv2 = sum_Cvv2/M
    stdv_Cvv = np.sqrt( (M/(M-1)) * (mean_Cvv2 - mean_Cvv*mean_Cvv) )
    
    mean_dr = sum_dr/M ; mean_dr2 = sum_dr2/M
    stdv_dr = np.sqrt( (M/(M-1)) * (mean_dr2 - mean_dr*mean_dr) )
    #**** ****#
    #exit()
    
    #*** open and writing file out 2 ***#
    file_out_2 = open(name_out_file_2,"w")
    file_out_2.write("# bin size = "+sys.argv[2]+" um \n")
    file_out_2.write("# mean_dr[um] mean_Cvv(dr)[ad.] stdv_dr[um] stdv_Cvv(dr)[ad.] N_points_in_bin[#] \n")
    for i in range(len(M)):
        file_out_2.write("%f %f %f %f %i \n" % (mean_dr[i], mean_Cvv[i], stdv_dr[i], stdv_Cvv[i], M[i]))
    file_out_2.write("#END")
    file_out_2.close()
    #*** ***#
    #exit()

    #**** removing ".tmp" file ****#
    string_to_remove_Cvv_f90_tmp_file = "rm %s" % (name_in_file)
    os.system(string_to_remove_Cvv_f90_tmp_file)
    #**** ****#
    #exit()

    #**** geting the frame number to write Cvv(dr*)=0 file (and for other files) ****#
    frame_number = name_in_file.replace("%sCvv_f90-" % (local_path_to_vaf),"")
    frame_number = frame_number.replace(".tmp","")
    #print frame_number    # debug line
    #**** ****#
    #exit()

    #**** defining the absolutu "time" in hours for the frame ****#
    time = (int(frame_number)*0.25+tcf)
    #**** ****#
    #exit()

    #**** writing the fit paramters to bash gnu script gt0cut (out the zero Cvv position for each frame) ****#
    # AND wiritng fit parameters data to file 
    x = [0.0] ; y = [1.0]
    for i in range(npt):
        if mean_Cvv[i] < 0: break
        x.append(mean_dr[i]) ; y.append(mean_Cvv[i])
            #print frame_number, mean_dr[i], mean_Cvv[i], mean_dr[i-1], mean_Cvv[i-1]    # debug line
    #print frame_number  # debug line
    x = np.array(x)
    y = np.array(y)
    #print x ; print y    # debug line
    fit_ex = scipy.optimize.curve_fit(lambda t,a,b: a*np.exp(-b*t),  x,  y,  p0=(1.0, 0.01))
    ele1 = fit_ex[1]
    ele1_0 = ele1[0] ; ele1_1 = ele1[1]
    ele = fit_ex[0]
    #print ele, type(ele)   # debug line
    a = ele[0] ; b = ele[1]
    file_exp_fit.write("%s %f %f %f %.12f %.12f %.12f %.12f\n" % (frame_number, time, a, b, ele1_0[0], ele1_0[1], ele1_1[0], ele1_1[1]))
    #print "a%i=%f;b%i=%f;"%(int(frame_number),a,int(frame_number),b)  # debug line
    string_fit_ab += "a%i=%f;b%i=%f;"%(int(frame_number),a,int(frame_number),b)
    #**** ****#
    #exit()

    #*** open and writing file out 3 ***#
    file_out_3 = open(name_out_file_3,"w")
    file_out_3.write("# bin size = "+sys.argv[2]+" um \n")
    file_out_3.write("# mean_dr[um] mean_Cvv(dr)[ad.] stdv_dr[um] stdv_Cvv(dr)[ad.] N_points_in_bin[#] \n")
    for i in range(npt):
        if mean_Cvv[i] < 0: break
        file_out_3.write("%f %f %f %f %i \n" % (mean_dr[i], mean_Cvv[i], stdv_dr[i], stdv_Cvv[i], M[i]))
    file_out_3.write("#END")
    file_out_3.close()
    #*** ***#
    #exit()

    if 2 == 2:
        #**** geting the minimum Cvv value and its respective dr position ****#
        is_there_a_local_minimum = "NO"
        for i in xrange(1,npt-1,1):
            if mean_Cvv[i] < 0 and mean_Cvv[i-1] > mean_Cvv[i] and mean_Cvv[i] < mean_Cvv[i+1]:
                #print frame_number, time, mean_Cvv[i], mean_dr[i]    # debug line
                minimum_Cvv = mean_Cvv[i] ; dr_of_minimum_Cvv = mean_dr[i]
                is_there_a_local_minimum = "YES"
                break
        if is_there_a_local_minimum == "NO":
            print frame_number, time, "No local minimum ----> code stoped"
            exit()
        x = mean_dr ; y = mean_Cvv
        x1 = float(dr_of_minimum_Cvv) ; y1 = float(minimum_Cvv)
        #**** ****#
        #exit()

        #**** defining the lits that will cointain the minimuns Cvv points ****#
        cvv_lt0_points_around_local_minima_list = []
        #**** ****#
        #exit()

        #**** geting the lt0 Cvv points that are (suposed to be) around the (first) local minima ****#
        the_zero_line_was_crossed = "NO"
        for i in xrange(1,npt-1,1):
            index = i
            #print index   # debug line
            if mean_Cvv[i] < 0.0:
                the_zero_line_was_crossed = "YES"
                cvv_lt0_points_around_local_minima_list.append([mean_dr[i],mean_Cvv[i],index])
                if mean_dr[i] > dr_of_minimum_Cvv:
                    if abs(mean_Cvv[i]) < abs(mean_Cvv[i]-mean_Cvv[i-1]): break
                    if abs(mean_Cvv[i]) < abs(mean_Cvv[i]-mean_Cvv[i+1]): break
                    #if abs(mean_Cvv[i]) < (abs(mean_Cvv[i])-abs(mean_Cvv[i+1])): break
            if the_zero_line_was_crossed == "YES" and mean_Cvv[i] > 0.0: break
        #print cvv_lt0_points_around_local_minima_list   # debug line
        #**** ****#
        #exit()

        #**** extracting the minimum Cvv dr position via x^2 fiting ****#
        x2 = [] ; y2 = []
        for ele in cvv_lt0_points_around_local_minima_list:
            #print ele[0], ele[1], ele[2]   # debug line
            x2.append(float(ele[0])) ; y2.append(float(ele[1]))
        fit_x2 = np.polyfit(x2,y2,2,full=True)
        #print time, fit_x2, len(fit_x2)    # debug line
        coefici = fit_x2[0] ; residue = fit_x2[1] ; rank = fit_x2[2] ; sing_val = fit_x2[3] ; rcond = fit_x2[4]
        if rank == 1:
            stp = "%s %2.2f %i %.6e %.6e %.6e %s %i %.6e %s %s %.6e\n"%(frame_number,time,len(x2),coefici[0],coefici[1],coefici[2],"NaN",rank,sing_val[0],"NaN","NaN",rcond)
            print time, fit_x2, len(fit_x2)
        elif rank == 2:
            stp = "%s %2.2f %i %.6e %.6e %.6e %s %i %.6e %.6e %s %.6e\n"%(frame_number,time,len(x2),coefici[0],coefici[1],coefici[2],"NaN",rank,sing_val[0],sing_val[1],"NaN",rcond)
            print time, fit_x2, len(fit_x2)
        elif rank == 3:
            if len(residue) == 0:
                stp = "%s %2.2f %i %.6e %.6e %.6e %s %i %.6e %.6e %.6e %.6e\n"%(frame_number,time,len(x2),coefici[0],coefici[1],coefici[2],"NaN",rank,sing_val[0],sing_val[1],sing_val[2],rcond)
                print time, fit_x2, len(fit_x2)
            else:
                stp = "%s %2.2f %i %.6e %.6e %.6e %.6e %i %.6e %.6e %.6e %.6e\n"%(frame_number,time,len(x2),coefici[0],coefici[1],coefici[2],residue,rank,sing_val[0],sing_val[1],sing_val[2],rcond)
        else:
            print "  Error, something wrong with X2 polyfit --> invalid rank ---> code stoped!"
            exit()
        #print stp  #debug line
        file_x2_fit.write(stp)
        fit_x2 = np.polyfit(x2,y2,2)
        #print time, fit_x2    # debug line
        xp_fit_x2 = np.linspace(0,800)
        p2_fit_x2 = np.poly1d(fit_x2)
        fit_dr_of_minimum_Cvv = -(fit_x2[1]*0.5)/(fit_x2[0])
        if len(residue) == 0 or fit_x2[1] > 0 or fit_x2[0] < 0 or len(x2) < 5 or fit_dr_of_minimum_Cvv >= 700:
            #print time, dr_of_minimum_Cvv, "NaN", fit_dr_of_minimum_Cvv, len(x2)    # debug line
            swirls_sizes_list.append(float("NaN"))
            reasons = []
            if len(residue) == 0:
                reasons.append("yes")
            else:
                reasons.append("no")
            if fit_x2[1] > 0:
                reasons.append("yes")
            else:
                reasons.append("no")
            if fit_x2[0] < 0:
                reasons.append("yes")
            else:
                reasons.append("no")
            if len(x2) < 5:
                reasons.append("yes")
            else:
                reasons.append("no")
            if fit_dr_of_minimum_Cvv >= 700:
                reasons.append("yes")
            else:
                reasons.append("no")
            reasons_to_eliminate_swirl_size_list.append(reasons)
        else:
            #print time, dr_of_minimum_Cvv, fit_dr_of_minimum_Cvv, fit_dr_of_minimum_Cvv, len(x2)   # debug line
            swirls_sizes_list.append(fit_dr_of_minimum_Cvv)
            reasons = ["---","---","---","---","---"]
            reasons_to_eliminate_swirl_size_list.append(reasons)
        swirls_sizes_valid_and_invalid_list.append(fit_dr_of_minimum_Cvv)
        Cvv_local_minima_list.append(dr_of_minimum_Cvv)
        #print time, fit_dr_of_minimum_Cvv, dr_of_minimum_Cvv    # debug line
        #**** ****#
        #exit()

        #**** ploting the Cvv minimum curve ****#
        if 2 == 2:
            fig, ax = plt.subplots()
            plt.ylim(-0.4,1)
            plt.xlim(0,800)
            ax.plot(x,y,"bo",ms=8,label="Cvv(dr) time = %2.2fh"%(time))
            ax.plot(x2,y2,"ro",ms=8,label="Cvv(dr) < 0.0")
            ax.plot(x1,y1,"ko",ms=8,label="local minima")
            ax.plot(xp_fit_x2,p2_fit_x2(xp_fit_x2),color="crimson",ls="dashed",label="f(x)=%.1ex^2+%.1ex+%.1e"%(fit_x2[0],fit_x2[1],fit_x2[2]))
            ax.plot((-100,850),(0,0),"y-",lw=1.0)
            ax.set_xlabel(r"dr [$\mu m$]")
            ax.set_ylabel("Velocity autocorrelation function [ad.]")
            ax.set_title(name_out_file_4.replace(local_path_to_vaf,""))
            ax.legend()
            name_out_image = name_out_file_4.replace(".vaf","-vaf.png")
            plt.savefig(name_out_image, bbox_inches='tight')
            #plt.show()
            plt.close()
        #**** ****#
        #exit()

        #*** open and writing file out 4 ***#
        file_out_4 = open(name_out_file_4,"w")
        file_out_4.write("# bin size = "+sys.argv[2]+" um \n")
        file_out_4.write("# mean_dr[um] mean_Cvv(dr)[ad.] stdv_dr[um] stdv_Cvv(dr)[ad.] N_points_in_bin[#] \n")
        for ele in cvv_lt0_points_around_local_minima_list:
            #print ele[0], ele[1], ele[2]   # debug line
            out_dr = ele[0] ; out_Cvv = ele[1] ; index = ele[2]
            file_out_4.write("%f %f %f %f %i \n" % (out_dr, out_Cvv, stdv_dr[index], stdv_Cvv[index], M[index]))
        file_out_4.write("#END")
        file_out_4.close()
        #*** ***#
        #exit()

    #**** making the Trapezoidal integration of the gt0 Cvv points (making the susectibility) ****#
    Chi = 0 # = trapezoidal integration
    x = [0.0] ; y = [1.0]
    for i in range(npt):
        if mean_Cvv[i] < 0: break
        x.append(mean_dr[i]) ; y.append(mean_Cvv[i])
        #print frame_number, mean_dr[i], mean_Cvv[i], mean_dr[i-1], mean_Cvv[i-1]    # debug line
    #print frame_number  # debug line
    x = np.array(x)
    y = np.array(y)
    #print x ; print y    # debug line
    delChi = []
    delX = []
    X = []
    for i in xrange(1,len(y),1):
        #print "Chi += (%f + %f)/2. * (%f - %f)" % (y[i-1],y[i],x[i],x[i-1])   # debug line
        Chi += (y[i-1] + y[i])/2. * (x[i] - x[i-1])
        delChi.append((y[i-1] + y[i])/2.)
        delX.append(x[i] - x[i-1])
        X.append(x[i-1])
    #print frame_number, Chi, x[len(x)-1]   # debug line
    total_degree_of_correlation_integrated_list.append(Chi)
    normalized_total_degree_of_correlation_integrated_list.append(Chi/x[len(x)-1])
    dr_of_Cvv_eqt_0_list.append(x[len(x)-1])
    #**** ****#
    #exit()

    #**** ploting the trapzoidal graph and gt0 graph ****#
    if 2 == 2:
        delChi = np.array(delChi)
        delX = np.array(delX)
        X = np.array(X)
        xp_fit_exp = np.linspace(0,x[len(x)-1])
        yp_fit_exp = a*np.exp(-b*xp_fit_exp)
        fig, ax = plt.subplots()
        ax.bar(X,delChi,delX,color="paleturquoise",label="Chi = Cvv trapezoidal integration",align="edge")
        ax.plot(x,y,"bo-",label="Cvv > 0")
        ax.plot(xp_fit_exp,yp_fit_exp,"g--",lw=2,label="f(x)=%2.2f*exp(-%2.3fx)"%(a,b))
        ax.set_ylabel("Cvv(dr) [ad.]")
        ax.set_xlabel(r"dr [$\mu m$]")
        ax.set_title(name_out_file_3.replace(local_path_to_vaf,""))
        ax.legend()
        #plt.show()
        name_out_image = name_out_file_3.replace(".vaf","-vaf.png")
        plt.savefig(name_out_image, bbox_inches='tight')
        plt.close()
    #**** ****#
    #exit()

    #**** apending plot list variables ****#
    time_frames_list.append(time)
    velocity_correlation_lengths_list.append(1/b)
    #**** ****#
    #exit()

    
#**********************************************************#
#***** END: Loop over frames ******************************#
#**********************************************************#
print
#exit()

#**** writing string ab data to tmp file to read by clc.py ****#
#print string_fit_ab   # debug line
file_tmp_string_fit_ab.write(string_fit_ab)
#**** ****#
#exit()


#****************************************#
#***** swirls sizes graph and data ******#
#****************************************#

#**** ploting the BAD graph swirls sizes charcateristic length graph ****#
x = time_frames_list
y = swirls_sizes_valid_and_invalid_list 
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"ro-",label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(0,600)
#ax.set_xlim(1,64)
ax.set_ylabel(r"swirls characteristic sizes [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-swirls_characteristic_sizes-BADGRAPH-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the swirls sizes charcateristic length graph ****#
x = time_frames_list
y = swirls_sizes_list 
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"ro-",label="fit_x2 %s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(0,800)
#ax.set_xlim(1,64)
ax.set_ylabel(r"swirls characteristic sizes [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-swirls_characteristic_sizes-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the swirls sizes charcateristic length graph in log ****#
from matplotlib.ticker import ScalarFormatter
x = time_frames_list
y = swirls_sizes_list 
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"ro-",label="fit_x2 %s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.set_xscale("log",basex=2)#,subsx=[10,20,30,40,50])
ax.set_yscale("log",basey=2)#,subsy=[40,60,100,140])
for axis in [ax.xaxis, ax.yaxis]:
    axis.set_major_formatter(ScalarFormatter())
#ax.set_ylim(100,710)
ax.set_xlim(1,64)
#ax.set_ylim(0,800)
#ax.set_xlim(1,64)
ax.set_ylabel(r"swirls characteristic sizes [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-swirls_characteristic_sizes-log-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the swirls sizes charcateristic length local minima graph ****#
x = time_frames_list
y = Cvv_local_minima_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"ko-",label="local minima %s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(0,710)
#ax.set_xlim(1,64)
ax.set_ylabel(r"swirls characteristic sizes [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-swirls_characteristic_sizes-LOCALMINIMA-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the swirls sizes charcateristic length local minima graph in log ****#
from matplotlib.ticker import ScalarFormatter
x = time_frames_list
y = Cvv_local_minima_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"ko-",label="local minima %s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.set_xscale("log",basex=2)#,subsx=[10,20,30,40,50])
ax.set_yscale("log",basey=2)#,subsy=[40,60,100,140])
for axis in [ax.xaxis, ax.yaxis]:
    axis.set_major_formatter(ScalarFormatter())
#ax.set_ylim(100,710)
ax.set_xlim(1,64)
#ax.set_ylim(0,710)
#ax.set_xlim(1,64)
ax.set_ylabel(r"swirls characteristic sizes [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-swirls_characteristic_sizes-LOCALMINIMA-log-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the swirls sizes charcateristic length and local minima graph ****#
x = time_frames_list
y = swirls_sizes_list
y1 = Cvv_local_minima_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"ro-",label="fit_x2 %s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.plot(x,y1,"ko-",label="local minima %s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(0,710)
#ax.set_xlim(1,64)
ax.set_ylabel(r"swirls characteristic sizes [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-swirls_characteristic_sizes-FITandLOCALMINIMA-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the swirls sizes charcateristic length and local minima graph in log ****#
from matplotlib.ticker import ScalarFormatter
x = time_frames_list
y = swirls_sizes_list
y1 = Cvv_local_minima_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"ro-",label="fit_x2 %s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.plot(x,y1,"ko-",label="local minima %s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.set_xscale("log",basex=2)#,subsx=[10,20,30,40,50])
ax.set_yscale("log",basey=2)#,subsy=[40,60,100,140])
for axis in [ax.xaxis, ax.yaxis]:
    axis.set_major_formatter(ScalarFormatter())
#ax.set_ylim(100,710)
ax.set_xlim(1,64)
#ax.set_ylim(0,710)
#ax.set_xlim(1,64)
ax.set_ylabel(r"swirls characteristic sizes [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-swirls_characteristic_sizes-FITandLOCALMINIMA-log-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** writing the swirls sizes file ****#
file_out_name = "%s%s-%s-piv%s-vff-swirls_sizes.vaf" % (local_path_to_vaf,d0,hyst_or_norm,extension)
file_out = open(file_out_name,"w")
file_out.write("# time is the abslut time in relation to the begining of the photo shooting\n")
file_out.write("# colunms $2 and $3 are the swirls sizes via python polifit and (ours) local minima routine\n")
file_out.write("# colunms $2 is \"NaN\" with one of the \"reasons_to_eliminate\" is equal to \"yes\"\n")
file_out.write("# reasons_to_aliminate = [ len(residue) == 0 , fit_x2[1] > 0 , fit_x2[0] < 0 , len(x2) < 5 , fit_dr_of_minimum_Cvv >= 700 ]\n")
file_out.write("# time[h]  fit_x2(valid)[um]  local_minima[um]  fit_x2(raw)[um]  reasons_to_eliminate[ 1 -> 5 ]\n")
for i in range(len(time_frames_list)):
    rte = reasons_to_eliminate_swirl_size_list[i]
    ssraw = swirls_sizes_valid_and_invalid_list[i]
    stp = "%f %f %f %f %s %s %s %s %s\n" % (time_frames_list[i], swirls_sizes_list[i], Cvv_local_minima_list[i], ssraw, rte[0],rte[1],rte[2],rte[3],rte[4])
    file_out.write(stp)
file_out.write("#END")
file_out.close()
#**** ****#
#exit()

#******************************#
#***** END : swirls sizes *****#
#******************************#
#exit()


#**********************************************#
#***** Correlation length graph and data ******#
#**********************************************#

#**** ploting the correlation length graph ****#
x = time_frames_list
y = velocity_correlation_lengths_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"go-",label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(40,160)
#ax.set_xlim(1,64)
ax.set_ylabel(r"velocity correlation length [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-velocity_correlation_length-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the correlation length graph in log ****#
from matplotlib.ticker import ScalarFormatter
x = time_frames_list
y = velocity_correlation_lengths_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"go-",label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.set_xscale("log",basex=2)#,subsx=[10,20,30,40,50])
ax.set_yscale("log",basey=2)#,subsy=[40,60,100,140])
for axis in [ax.xaxis, ax.yaxis]:
    axis.set_major_formatter(ScalarFormatter())
ax.set_ylim(40,160)
ax.set_xlim(1,64)
ax.set_ylabel(r"velocity correlation length [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-velocity_correlation_length_log-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** writing the velocity correlation length file ****#
file_out_name = "%s%s-%s-piv%s-vff-velocity_correlation_length.vaf" % (local_path_to_vaf,d0,hyst_or_norm,extension)
file_out = open(file_out_name,"w")
file_out.write("# time is the abslut time in relation to the begining of the photo shooting\n")
file_out.write("# velocity correlation was obtained via expoetila fiting of Cvv curves with values gt 0\n")
file_out.write("# time[h]  velocity_correlation_length[um]\n")
for i in range(len(time_frames_list)):
    file_out.write("%f %f\n" % (time_frames_list[i],velocity_correlation_lengths_list[i]))
file_out.write("#END")
file_out.close()
#**** ****#
#exit()

#**********************************************#
#***** END : Correlation length ***************#
#**********************************************#
#exit()


#*******************************************************#
#***** Total degree of correlation graph and data ******#
#*******************************************************#

#**** ploting the (integrated) total degre of correlation graph ****#
x = time_frames_list
y = total_degree_of_correlation_integrated_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"bo-",label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(40,160)
#ax.set_xlim(1,64)
ax.set_ylabel(r"integrated total degree of correlation [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-integrated_total_degre_of_correlation-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the (integrated) total degre of correlation graph in log****#
from matplotlib.ticker import ScalarFormatter
x = time_frames_list
y = total_degree_of_correlation_integrated_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x,y,"bo-",label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
ax.set_xscale("log",basex=2)#,subsx=[10,20,30,40,50])
ax.set_yscale("log",basey=2)#,subsy=[40,60,100,140])
for axis in [ax.xaxis, ax.yaxis]:
    axis.set_major_formatter(ScalarFormatter())
ax.set_ylim(40,160)
ax.set_xlim(1,64)
ax.set_ylabel(r"integrated total degree of correlation [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-integrated_total_degre_of_correlation-LOG-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the normalized (integrated) total degre of correlation graph ****#
plt.rcParams.update({'font.size': 17})
x = time_frames_list
y = normalized_total_degree_of_correlation_integrated_list
#*making mean nChi*#
five = 5.0 ; forty_five = 45.0
if d0 == "fibroblastos-m01":
    five = 20.0 ; forty_five = 60.0
N_time_leq45 = 0 ; Chi = 0. ; N_time_eq5 = 0
sum_nChi = 0. ; sum_nChi2 = 0.
mean_nChi = 0. ; stdv_nChi = 0.
for i in range(len(x)):
    Chi = y[i]
    N_time_eq5 += 1
    if x[i] >= five:
        N_time_eq5 -= 1
        if x[i] > forty_five: break
        N_time_leq45 += 1 
        sum_nChi += Chi
        sum_nChi2 += Chi*Chi
        #print x[i], y[i], Chi # debug line
N_time_leq45 += N_time_eq5
#print x[N_time_eq5], x[N_time_leq45-1] # debug line
#print N_time_eq5, N_time_leq45, (N_time_leq45-N_time_eq5) # debug line
Nt = (N_time_leq45-N_time_eq5)
mean_nChi = sum_nChi/Nt  ;  mean_nChi2 = sum_nChi2/Nt
stdv_nChi = np.sqrt( (Nt/(Nt-1)) * (mean_nChi2 - mean_nChi*mean_nChi) )
#print " %f +- %f "%(mean_nChi,stdv_nChi)   # debug line
#**#
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.plot(x,y,"bo-",label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
if d0 == "fibroblastos-m01":
    ax.plot((x[N_time_eq5],x[N_time_leq45-1]),(mean_nChi,mean_nChi),color="orange",lw=3,ls="dashed",label=r"$<\chi_{I}/\xi>_{20h\leq t \leq60h}$")
else:
    ax.plot((x[N_time_eq5],x[N_time_leq45-1]),(mean_nChi,mean_nChi),color="orange",lw=3,ls="dashed",label=r"$<\chi_{I}/\xi>_{5h\leq t \leq45h}$")
ax.plot(x,y,color="cornflowerblue",lw=3)#,label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
if d0 == "fibroblastos-m01":
    ax.set_ylim(0,0.65)
else:
    ax.set_ylim(0,0.55)
    ax.set_xlim(-1.7,52.5)
ax.set_ylabel(r"$\chi_{I}$ / $\xi$ [ad.]")
#ax.set_ylabel(r"integrated total degree of correlation / $\xi$ [ad.]")
ax.set_xlabel("time [h]")
ax.set_title(d0)
if d0 == "fibroblastos-m01":
    plt.text(0.4,0.14,r"$<\chi_{I}/\xi>$ = %2.3f $\pm$ %2.3f"%(mean_nChi,stdv_nChi),fontsize=14, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
else:
    plt.text(0.4,0.11,r"$<\chi_{I}/\xi>$ = %2.3f $\pm$ %2.3f"%(mean_nChi,stdv_nChi),fontsize=14, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
plt.legend(loc="lower left",fontsize=17)
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-integrated_total_degre_of_correlation-NORMALIZED-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the \xi = dr of Cvv eqt0, i.e. \Chi normalization factor, graph ****#
plt.rcParams.update({'font.size': 17})
x = time_frames_list
y = dr_of_Cvv_eqt_0_list
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.plot(x,y,"bo-",label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.plot((x[N_time_eq5],x[N_time_leq45-1]),(mean_nChi,mean_nChi),color="orange",lw=3,ls="dashed",label=r"$<\chi_{I}/\xi>_{5h\leq t \leq45h}$")
ax.plot(x,y,color="blueviolet",lw=3,label=r"$\xi$ is the $dr$ that Cvv($dr$)=0")
#ax.set_ylim(0,0.55)
#ax.set_xlim(-1.7,52.5)
ax.set_ylabel(r"$\xi$ [$\mu m$]")
#ax.set_ylabel(r"integrated total degree of correlation / $\xi$ [ad.]")
ax.set_xlabel("time [h]")
ax.set_title(d0)
#plt.legend(loc="lower left",fontsize=17)
plt.legend(fontsize=14)
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-dr_of_cvv_eqt0-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** writing the total degree of correlation file ****#
file_out_name = "%s%s-%s-piv%s-vff-total_degree_of_correlation.vaf" % (local_path_to_vaf,d0,hyst_or_norm,extension)
file_out = open(file_out_name,"w")
file_out.write("# time is the abslut time in relation to the begining of the photo shooting\n")
file_out.write(r"# total degree of correlation was obtained is equal to the integration of Cvv from dr=0 to dr=$\xi$ | Cvv($\xi$)=0 (i.e. for Cvv values gt 0)"+"\n")
file_out.write(r"# normalized_total_degree_of_correlation = total_degree_of_correlation/$\xi$ ---> = 0.5 diagonal Cvv"+"\n")
file_out.write(r"# $<\chi/\xi>$, i.e. the mean normalized total degre of correlation, was obtained in a time window of time = [5h,45h]"+"\n")
file_out.write(r"# $<\chi/\xi>$ = %2.3f $\pm$ %2.3f"%(mean_nChi,stdv_nChi)+"\n")
file_out.write(r"# time[h]  total_degre_of_correlation[um]  $\xi$[um]=dr_of_Cvv=0[um]  normalized_total_degre_of_correlation"+"\n")
for i in range(len(time_frames_list)):
    Chi = total_degree_of_correlation_integrated_list[i]
    xsi = dr_of_Cvv_eqt_0_list[i]
    nChi = normalized_total_degree_of_correlation_integrated_list[i]
    file_out.write("%f %f %f %f\n" % (time_frames_list[i], Chi, xsi, nChi))
file_out.write("#END")
file_out.close()
#**** ****#
#exit()

#*********************************************#
#***** END : Total degree of correlation *****#
#*********************************************#
#exit()


#****************************************************#
#***** Ploting combined Cvv measurements graphs *****#
#****************************************************#

#**** ploting the Cvv measurements combined graph graph ****#
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = time_frames_list
y1 = velocity_correlation_lengths_list
ax.plot(x,y1,"go-",label="fit_exp %s-%s-piv%s"%(d0,hyst_or_norm,extension))
y2 = total_degree_of_correlation_integrated_list
ax.plot(x,y2,"bo-",label=r"Chi %s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(40,160)
#ax.set_xlim(1,64)
ax.set_ylabel(r"length [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-Chi_fit_exp_combined_graph-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the Cvv measurements combined graph graph ****#
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = time_frames_list
y1 = swirls_sizes_list
ax.plot(x,y1,"ro-",label="fit_x2 %s-%s-piv%s"%(d0,hyst_or_norm,extension))
y2 = velocity_correlation_lengths_list
ax.plot(x,y2,"go-",label="fit_exp %s-%s-piv%s"%(d0,hyst_or_norm,extension))
y3 = total_degree_of_correlation_integrated_list
ax.plot(x,y3,"bo-",label=r"Chi %s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(40,160)
#ax.set_xlim(1,64)
ax.set_ylabel(r"length [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-Cvv_combined_measurements_graph-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#**** ploting the Cvv measurements combined graph graph ****#
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = time_frames_list
y1 = swirls_sizes_list
ax.plot(x,y1,"ro-",label="fit_x2 %s-%s-piv%s"%(d0,hyst_or_norm,extension))
y1l = Cvv_local_minima_list
ax.plot(x,y1l,"ko-",label="local minima %s-%s-piv%s"%(d0,hyst_or_norm,extension))
y2 = velocity_correlation_lengths_list
ax.plot(x,y2,"go-",label="fit_exp %s-%s-piv%s"%(d0,hyst_or_norm,extension))
y3 = total_degree_of_correlation_integrated_list
ax.plot(x,y3,"bo-",label=r"Chi %s-%s-piv%s"%(d0,hyst_or_norm,extension))
#ax.set_ylim(40,160)
#ax.set_xlim(1,64)
ax.set_ylabel(r"length [$\mu m$]")
ax.set_xlabel("time [h]")
plt.legend()
#plt.show()
name_out_image = "%s%s-%s-piv%s-vff-Cvv_combined_measurements_graph_local_minima-vaf.png" % (local_path_to_vaf,d0,hyst_or_norm,extension)
plt.savefig(name_out_image, bbox_inches='tight')
plt.close()
#**** ****#
#exit()

#***************************************************#
#***** END : Ploting combined Cvv measurements *****#
#***************************************************#
#exit()

print "\n END : vaf.py <<<  "
exit()
