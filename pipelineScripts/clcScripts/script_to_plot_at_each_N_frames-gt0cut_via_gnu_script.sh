#!/bin/bash
#Uso:
# bash script_to_plot_at_each_N_frames-gt0cut_via_gnu_script.sh 1 199 --> ( $1 = first frame ) and ( $2 = last frame )
#
index=0
for i in `ls *gt0cut.vaf`
do
    files_array+=($i)
    ((index++))
done
#echo ${files_array[198]}
#echo ${files_array[@]}

as='"'
pv=';'
hst='#'
dol='$'
pxp='(x)'
po='('
pc=')'
pf="%f"

echo
echo -e " #From ${as}${0}${as} :"

echo
mkdir -p N_by_N_plots-gt0cut ; echo "        N_by_N_plots-gt0cut folder created (via 'mkdir -p')"

for N in 1 #2 3 4 5 6 7 8
do
    echo set macros > tmp_file
    echo -e "XY = ${as}u 1:2:(${dol}1-${dol}3):(${dol}1+${dol}3):(${dol}2-${dol}4):(${dol}2+${dol}4):5 w xyerrorbars${as}" >> tmp_file 
    #echo set term png >> tmp_file
    echo set term pngcairo font ${as}Arial, 14${as} >> tmp_file
    echo set yrange[-1.5:1.0] >> tmp_file
    echo set xrange[0:600] >> tmp_file
    echo set grid >> tmp_file
    echo set title ${as} dr Bin 16 {/Symbol m}m ${as} >> tmp_file
    echo -e "set ylabel ${as}mean Cvv(dr)${as}" >> tmp_file
    echo -e "set xlabel ${as}mean dr [{/Symbol m}m]${as}" >> tmp_file
    echo -e "<a0=a0;b0=b0;>" >> tmp_file
   
    START=$(($1-1)) ; END=$(($2-1)) ;JUMP=$N
    #for i in $(eval echo "{$START..$END..$JUMP}"); do echo "$i"; done
    #exit $?
    for (( i=START; i<=END; i+=JUMP ))
    do
	a=$((i)); b=$((i+1)); c=$((i+2)); d=$((i+3)); e=$((i+4)); f=$((i+5)); g=$((i+6)); h=$((i+7));
	echo -e "f${b}(x) = a${b}*exp(-b${b}*x)" >> tmp_file;
	echo -e "A = ${as} t 'frame ${b}'${as} ${pv} B = ${as} t 'frame ${c}'${as} ${pv} C = ${as} t 'frame ${d}'${as} ${pv} D = ${as} t 'frame ${e}'${as} ${pv} E = ${as} t 'frame ${f}'${as} ${pv} F = ${as} t 'frame ${g}'${as} ${pv} G = ${as} t 'frame ${h}'${as} ${pv} H = ${as} t 'frame $((${h}+1))'${as}" >> tmp_file #; exit $?
	if [ $b -lt 10 ] ; then file="N${N}_000${b}.png" ; fi
	if [ $b -ge 10 ] && [ $b -lt 100 ] ; then file="N${N}_00${b}.png" ; fi
	if [ $b -ge 100 ] && [ $b -lt 1000 ] ; then file="N${N}_0${b}.png" ; fi

	if [ $N -eq 1 ] ; then echo set output $as$file$as $pv plot $as${files_array[$a]}$as @XY@A, f${b}$pxp t sprintf${po}${as}f${b}${pxp} = ${pf}*exp${po}-${pf}*x${pc}${as},a${b},b${b}${pc}  $pv replot >> tmp_file ; fi 
    done

    echo
    mv tmp_file script_to_plot_Cvv_at_each_N_frames-gt0cut.gnu ;
    gnuplot script_to_plot_Cvv_at_each_N_frames-gt0cut.gnu ; echo -e "        ${N}_by_${N}_plots-gt0cut done"
    
    mv N${N}_* N_by_N_plots-gt0cut/. ; echo -e "        ${N}_by_${N}_plots-gt0cut moved to folder"
done

echo
echo " #By by"
exit $?
