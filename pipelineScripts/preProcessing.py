import sys
import os
import numpy as np
import subprocess as subp
import matplotlib.pyplot as plt


#**********************************************************************#
#**** Section 0: Making directories tree and defining in variables ****#
#**********************************************************************#

#**** in variables ****#
d0 = sys.argv[1] ; # d0 = <d0> = linage-movie directory
path_and_file_name = sys.argv[2] ; # of the lamoc movie to be opened
path_to_pipeline = sys.argv[3];  # path to pipeline
path_to_imagej_fiji_executable = sys.argv[4]
path_to_pipeline_scripts = sys.argv[5]
path_to_imagej_fiji_scripts  = sys.argv[6]
file_name = sys.argv[7]
local_path_to_dsoi = sys.argv[8]
#print d0+"\n"+path_and_file_name+"\n"+path_to_pipeline+"\n"+path_to_imagej_fiji_executable+"\n"+path_to_pipeline_scripts+"\n"+path_to_imagej_fiji_scripts+"\n"+file_name+"\n"+local_path_to_dsoi   # debug line
#**** ****#
#exit()

#**** making directories ****#
d1 = "preProcessing"  ; os.system("mkdir -p %s/%s" % (d0,d1))
d1d = "brightness_profile"  ; os.system("mkdir -p %s/%s/%s" % (d0,d1,d1d))
d1da = "before_cut_and_normalization"  ; os.system("mkdir -p %s/%s/%s/%s" % (d0,d1,d1d,d1da))
d1db = "after_cut_and_normalization"  ; os.system("mkdir -p %s/%s/%s/%s" % (d0,d1,d1d,d1db))
d1tmp = "tmp_tif"  ; os.system("mkdir -p %s/%s/%s" % (d0,d1,d1tmp))
d1cut = "tmp_cut"  ; os.system("mkdir -p %s/%s/%s" % (d0,d1,d1cut))
d1log = "LOG"  ; os.system("mkdir -p %s/%s/%s" % (d0,d1,d1log))
d1logd = "scripts_used"  ; os.system("mkdir -p %s/%s/%s/%s" % (d0,d1,d1log,d1logd))
d2 = "norm"  ; os.system("mkdir -p %s/%s" % (d0,d2))
d3 = "hyst"  ; os.system("mkdir -p %s/%s" % (d0,d3))
d4 = "manual_counting"  ; os.system("mkdir -p %s/%s" % (d0,d4))
d5 = "movies_and_images"  ; os.system("mkdir -p %s/%s" % (d0,d5))
d5jpg = "jpg"  ; os.system("mkdir -p %s/%s/%s" % (d0,d5,d5jpg))
d5merged = "merged"  ; os.system("mkdir -p %s/%s/%s" % (d0,d5,d5merged))
#**** ****#
#exit()

#**** defining necessary paths ****#
#path_to_d0 = "%s%s/" % (path_to_pipeline,d0)
local_path_to_d0 = "%s/" % (d0)

path_to_tmp_tif = "%s%s/%s/%s/" % (path_to_pipeline,d0,d1,d1tmp)
local_path_to_tmp_tif = "%s/%s/%s/" % (d0,d1,d1tmp)

path_to_tmp_cut = "%s%s/%s/%s/" % (path_to_pipeline,d0,d1,d1cut)
local_path_to_tmp_cut = "%s/%s/%s/" % (d0,d1,d1cut)

#path_to_log = "%s%s/%s/%s/" % (path_to_pipeline,d0,d1,d1log)
local_path_to_log = "%s/%s/%s/" % (d0,d1,d1log)

#path_to_used_scripts = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d1,d1log,d1logd)
local_path_to_used_scripts = "%s/%s/%s/%s/" % (d0,d1,d1log,d1logd)

path_to_norm = "%s%s/%s/" % (path_to_pipeline,d0,d2)
local_path_to_norm = "%s/%s/" % (d0,d2)

path_to_hyst = "%s%s/%s/" % (path_to_pipeline,d0,d3)
local_path_to_hyst = "%s/%s/" % (d0,d3)

path_to_manual_counting = "%s%s/%s/" % (path_to_pipeline,d0,d4)
#local_path_to_manual_counting = "%s/%s/" % (d0,d4)

path_to_movies_and_images = "%s%s/%s/" % (path_to_pipeline,d0,d5)
#local_path_to_movies_and_images = "%s/%s/" % (d0,d5)
#**** ****#
#exit()

#**** defining scripts and executables names ****#
fiji_executable = "ImageJ-linux64"
fiji_script1 = "pipeline_script_preprocessing-1.ijm"
fiji_script2 = "pipeline_script_preprocessing-2.ijm"
fiji_script3 = "pipeline_script_preprocessing-3.ijm"
fiji_script4a = "pipeline_script_preprocessing-4a.ijm"
fiji_script4b = "pipeline_script_preprocessing-4b.ijm"

#octave_executable = "octave"
normalization_script1 = "normalize_16bit.m"
normalization_script2 = "loopimg.sh"

string_to_execute_fiji = path_to_imagej_fiji_executable + fiji_executable
#print string_to_execute_fiji+' '+path_and_file_name      # debug line
#**** ****#
#exit()

#******************************************************************#
#****** A - Fiji operations 1: histogran and pre Cut procudres ****#
#******************************************************************#

#**** manual procedure 1 talks ****#
print
print "*********************************"
print "****** Manual procedure 1 *******"
print "*********************************"
print " After Fiji has started:"
print " (1) press \"[\" key > \"crtl+o\" > open \"%s\" > \"crtl+r\"" % (fiji_script1)
print "            >>> histogran window on top of the  movie window will apeare"
print " (2) mouse drag histogran bellow label bar (and inside the figure) > press \"live\" buton > \"fn+F12 (print screen)\" at each 20 frames (scrolling in time)"
print " (3) open nautilus > pictures > copy and paste the screanshots to \"%s/%s/%s/%s/\" directory" % (d0,d1,d1d,d1da)
print " (4) rename them as \"001.png ... 121.png\" acordingly to the label of the images in the label bar"
print " (5) scroll fiji movie window and take note of wich frames to eliminate and whay (drift, contrasct/bright bad)"
print " (6) take note of total number of stacks > close Fiji"
#**** ****#
#exit()

#**** modifing fiji script 1 ****#
file_in_to_open = "%sVOID-%s" % (path_to_pipeline_scripts,fiji_script1)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
file_out_to_open = "%s%s" % (path_to_imagej_fiji_scripts,fiji_script1)
file_out = open(file_out_to_open,"w")
data=data.replace("<PATH_AND_FILE_NAME>",path_and_file_name)
file_out.write(data)
file_in.close()
file_out.close() ; print
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** executing fiji first time ****#
print"\n ***** Fiji Talks 1: "
os.system(string_to_execute_fiji)
print" ***** End Fiji Talks 1. \n"
#**** ****#
#exit()

#**** moving script fiji 1 to used folder ****#
string_to_move_used_script = "mv %s%s %s." % (path_to_imagej_fiji_scripts,fiji_script1, local_path_to_used_scripts)
#print string_to_move_used_script    # debug line
os.system(string_to_move_used_script)
#**** ****#
#exit()

#******************************************************************#
#** END A *********************************************************#
#******************************************************************#
#exit()
print

#******************************************************************#
#****** B - Geting the frames that will be cuted ******************#
#******************************************************************#

#**** Geting from user the frames to be cut and the reasons for it ****#
total_number_of_stacks = int(raw_input("Enter the total frames: "))
#print "Enter the total number frames: 201" ; total_number_of_stacks = 201    # debug line
input_string = raw_input("Enter the frames to cut (1, 1-5, 197-201 or enter to skip): ")
#print "Enter the frames to cut (1, 1-5, 197-201): 1" ; input_string ="1" # debug line
frame_blocks_and_reasons_to_cut=[]
input_string=input_string.replace(" ","")
frame_blocks_to_cut = input_string.split(",")
if "" in frame_blocks_to_cut:     #this if is reuired for when no frame is deleted (user chose "enter")
    frame_blocks_to_cut.remove("")
for ele in frame_blocks_to_cut:
    input_string = raw_input("reason to cut %s: " % (ele))
    #print "reason to cut 1: drift and bright profile higher than stack avarege" ; input_string = "drift and bright profile higher than stack avarege" # debug line
    frame_blocks_and_reasons_to_cut.append([ele,input_string])
#**** ****#
#exit()

#**** making the lists that will have the frames to be cuted and the reasons (and printing then back to user) ****#
original_movie_frames = ["" for i in range(total_number_of_stacks)]
cuted_movie_frames = ["" for i in range(total_number_of_stacks)]
reasons_to_cut = ["" for i in range(total_number_of_stacks)]
frames_to_be_cuted = []
for i in range(total_number_of_stacks):
    original_movie_frames[i]=str(i+1)
    cuted_movie_frames[i]=str(i+1)
for ele in frame_blocks_and_reasons_to_cut:
    if "-" not in ele[0]:
        cuted_movie_frames[int(ele[0])-1]="000"
        reasons_to_cut[int(ele[0])-1]=ele[1]
        frames_to_be_cuted.append(ele[0])
    else:
        block_split=ele[0].split("-")
        for index in xrange(int(block_split[0]),int(block_split[1])+1):
            cuted_movie_frames[index-1]="000"
            reasons_to_cut[index-1]=ele[1]
            frames_to_be_cuted.append(original_movie_frames[index-1])
print "** frames to be cuted: ", frames_to_be_cuted
#**** ****#
#exit()

#**** Defining what is the original to cuted relation ****#
for index in range(total_number_of_stacks):
    if cuted_movie_frames[index] != "000":
        correction_value = index
        first_cuted_valid_index = index
        break
for index in xrange(total_number_of_stacks-1,-1,-1):
    if cuted_movie_frames[index] != "000":
        last_cuted_valid_index = index+1
        break
for i in xrange(first_cuted_valid_index,last_cuted_valid_index):
    value = cuted_movie_frames[i]
    cuted_movie_frames[i] = str(int(value)-correction_value)
#**** ****#
#exit()

#******************************************************************#
#** END B *********************************************************#
#******************************************************************#
#exit()
print

#******************************************************************#
#****** C - Printing frames original to cut relation in PDF *******#
#******************************************************************#

#**** passing the string value for the variables that go in the header off original to cut PDF ****#
original_movie_number_of_stacks = str(total_number_of_stacks)
cuted_movie_number_of_stacks = cuted_movie_frames[last_cuted_valid_index-1]
#**** ****#
#exit()

#**** Making and exporting PDF with the 'original to cuted' relation ****#
from fpdf import FPDF
pdf = FPDF('P', 'mm', 'A4')
pdf.add_page()
pdf.set_xy(0, 0)
pdf.set_font('arial', 'B', 13.0)
pdf.cell(ln=0, h=5.0, align='L', w=0, txt="", border=0)
pdf.ln()#h = '')
pdf.cell(ln=0, h=5.0, align='L', w=0, txt="                       Frame Number Relation of Original to Cuted Movie", border=0)
pdf.ln()#h = '')
pdf.ln()#h = '')
text = "Original Movie:  %s frames " % (original_movie_number_of_stacks)
pdf.cell(ln=0, h=5.0, align='L', w=0, txt=text, border=0)
pdf.ln()#h = '')
text = "Cuted Movie:     %s frames " % (cuted_movie_number_of_stacks)
pdf.cell(ln=0, h=5.0, align='L', w=0, txt=text, border=0)
pdf.ln()#h = '')
pdf.ln()#h = '')
pdf.cell(ln=0, h=5.0, align='L', w=0, txt="Original   -->   Cuted           Reason to cut (the cuted ones have the value 000)", border=0)
for index in range(total_number_of_stacks):
    pdf.ln()#h = '')
    text = "      %s   -->    %s        %s" % (original_movie_frames[index],cuted_movie_frames[index],reasons_to_cut[index])
    pdf.cell(ln=0, h=5.0, align='L', w=0, txt=text, border=0)
string_to_save_cuted_original_frame_relation_pdf = "%s/%s/original_to_cuted_frame_relation.pdf" % (d0,d1)
pdf.output(string_to_save_cuted_original_frame_relation_pdf, 'F')
#**** ****#
#exit()

#******************************************************************#
#** END C *********************************************************#
#******************************************************************#
#exit()
print

#******************************************************************#
#****** D - Updating the main variable 'total_number_of_stacks' ***#
#******************************************************************#
original_movie_number_of_stacks = str(total_number_of_stacks)
total_number_of_stacks = int(cuted_movie_number_of_stacks)
#print total_number_of_stacks    # debug line
#******************************************************************#
#** END D *********************************************************#
#******************************************************************#
#exit()
#print

#******************************************************************#
#****** E - Fiji operations 2: histogran and pre Cut procudres ****#
#******************************************************************#

#**** manual procedure 2 talks ****#
#print
print "*********************************"
print "****** Manual procedure 2 *******"
print "*********************************"
print " After Fiji has started:"
print " (1) press \"[\" key > \"crtl+o\" > open \"%s\" > \"crtl+r\"" % (fiji_script2)
print "            >>> The chosen frames will be cuted"
print "            >>> The remaning movie will be saved as a image sequence \".tiff\" in \"%s/%s/%s/\" directory" % (d0,d1,d1tmp)
print "            >>> Fiji will close automatically" 
print "            >>> Normalization process with octave will start"
#**** ****#
#exit()

#**** modifing fiji script 2 ****#
#print "writing the new script"
file_in_to_open = "%sVOID-%s" % (path_to_pipeline_scripts,fiji_script2)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
file_out_to_open = "%s%s" % (path_to_imagej_fiji_scripts,fiji_script2)
file_out = open(file_out_to_open,"w")
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
data=data.replace("<PATH_AND_FILE_NAME>",path_and_file_name)
data=data.replace("<SLICE_i>",original_movie_frames[first_cuted_valid_index])
data=data.replace("<SLICE_f>",original_movie_frames[last_cuted_valid_index-1])
data=data.replace("<FILE_NAME>",file_name)
data=data.replace("<PATH_TO_SAVE>",path_to_tmp_tif)
file_out.write(data)
file_in.close()
file_out.close() ; print
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** executing fiji second time ****#
print"\n ***** Fiji Talks 2: "
os.system(string_to_execute_fiji)
print" ***** End Fiji Talks 2. \n"
#**** ****#
#exit()

#**** moving script fiji 2 to used folder ****#
string_to_move_used_script = "mv %s%s %s." % (path_to_imagej_fiji_scripts,fiji_script2, local_path_to_used_scripts)
#print string_to_move_used_script    # debug line
os.system(string_to_move_used_script)
#**** ****#
#exit()

#******************************************************************#
#**** making cuted copy to do histogram screanshots later ****#
string_to_copy_tiff_to_tmp_cut = "cp %s*.tif %s." % (local_path_to_tmp_tif,local_path_to_tmp_cut)
os.system(string_to_copy_tiff_to_tmp_cut)
#**** ****#
#exit()
#******************************************************************#

#******************************************************************#
#** END E *********************************************************#
#******************************************************************#
#exit()
print

#******************************************************************#
#****** F - Octave operations: Normalization,rename and move  *****#
#******************************************************************#
print "*********************************************  "
print "****** Normalization procedures section ******  "
print "*********************************************  "
#**** geting from user the mean intenisty level to normalize (and showing some options) ****#
input_string = raw_input("Enter the mean intensity valeu to normalize (use 10k=dark, 12k=good or 15k=saturated): ")
#print "Enter the mean intensity valeu to normalize (use 10k=dark, 12k=good or 15k=saturated): 12k" ; input_string ="12k" # debug line
print "** for a 16bit image (the type we use) the max pixel intesty value is 65535"
print "** the images are normalized to the distrution has this max valeu and a mean equals the one given above"
mean_intensity_value = input_string.replace("k","000")
#**** ****#
#exit()

#**** modifing the normalization script ****#
file_in_to_open = "%sVOID-%s" % (path_to_pipeline_scripts,normalization_script1)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
#file_out_to_open = "%s%s" % (local_path_to_tmp_tif,normalization_script1)
file_out_to_open = "%s" % (normalization_script1)
file_out = open(file_out_to_open,"w")
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
data=data.replace("<MEAN_INTENSITY_LEVEL>",mean_intensity_value)
file_out.write(data)
file_in.close()
file_out.close() ; print
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** modifing (or just copying) the bash loop script that call the 'normalize' script ****#
file_in_to_open = "%sVOID-%s" % (path_to_pipeline_scripts,normalization_script2)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
#file_out_to_open = "%s%s" % (local_path_to_tmp_tif,normalization_script2)
file_out_to_open = "%s" % (normalization_script2)
file_out = open(file_out_to_open,"w")
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
data=data.replace("<PATH_TO_TMP_TIF>",path_to_tmp_tif)
file_out.write(data)
file_in.close()
file_out.close()
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** executing normalization process (bash loop and octave) ****#
string_to_execute_normalization_loop = "bash %s" % (normalization_script2)
print"\n ***** Octave Talks : "
os.system(string_to_execute_normalization_loop)
print" ***** End Octave Talks. \n"
#**** ****#
#exit()

#**** moving normalization scripts to used folder ****#
string_to_move_used_scripts = "mv %s %s %s." % (normalization_script1,normalization_script2, local_path_to_used_scripts)
#print string_to_move_used_scripts    # debug line
os.system(string_to_move_used_scripts)
#**** ****#
#exit()

#**** moving/rename tiff to norm folder with linage name and 'rm -r tmp_tif' directory ****#
string_to_move_normalized_tif_to_norm = "for i in `ls %s`;do mv %s$i %s%s-norm$i;done" % (local_path_to_tmp_tif,local_path_to_tmp_tif,local_path_to_norm,d0)
os.system(string_to_move_normalized_tif_to_norm)
string_to_remove_tmp_tif_directory = "rm -r %s" % (local_path_to_tmp_tif)
os.system(string_to_remove_tmp_tif_directory)
#**** ****#
#exit()

#**** debug lines ****#
#string_to_list_tmp_tiff = "ls -d %s*/" % (path_to_tmp_tif)
#os.system(string_to_list_tmp_tiff) #debug line
#tiff_string = subp.check_output(string_to_list_lamoc_movies,shell=True) 
#**** ****#
#exit()

#******************************************************************#
#** END F *********************************************************#
#******************************************************************#
#exit()
print

#******************************************************************#
#****** G - Fiji operations 3:  ****#
#******************************************************************#

#**** manual procedure 3 talks ****#
#print
print "*********************************"
print "****** Manual procedure 3 *******"
print "*********************************"
print " After Fiji has started:"
print " (1) press \"[\" key > \"crtl+o\" > open \"%s\" > \"crtl+r\"" % (fiji_script3)
print "            >>> The normalized tiff frames will be loaded"
print "            >>> The manual_counting '.tiff' file will be saved in \"%s/%s/\" directory" % (d0,d4)
print "            >>> '.avi' movie and '.jpg' images (that will be used to merge) will be saved in \"%s/%s/\" directory" % (d0,d5)
print " (2) do histogran print screan in 50%s windows and moving to right folder stuff" % ("%")
print " (3) open nautilus > pictures > copy and paste the screanshots to \"%s/%s/%s/%s/\" directory" % (d0,d1,d1d,d1db)
print " (4) Close Fiji"
#**** ****#
#exit()

#**** modifing fiji script 3 ****#
#print "writing the new script"
file_in_to_open = "%sVOID-%s" % (path_to_pipeline_scripts,fiji_script3)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
file_out_to_open = "%s%s" % (path_to_imagej_fiji_scripts,fiji_script3)
file_out = open(file_out_to_open,"w")
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
data=data.replace("<PATH_TO_NORM>",path_to_norm)
data=data.replace("<LINAGE>",d0)
data=data.replace("<NUMBER_OF_STACKS>",str(total_number_of_stacks))
data=data.replace("<PATH_TO_MOVIES_AND_IMAGES>",path_to_movies_and_images)
data=data.replace("<PATH_TO_MANUAL_COUNTING>",path_to_manual_counting)
data=data.replace("<PATH_TO_TMP_CUT>",path_to_tmp_cut)
file_out.write(data)
file_in.close()
file_out.close() ; print
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** executing fiji third time ****#
print"\n ***** Fiji Talks 3: "
os.system(string_to_execute_fiji)
print" ***** End Fiji Talks 3. \n"
#**** ****#
#exit()

#**** moving script fiji 3 to used folder ****#
string_to_move_used_script = "mv %s%s %s." % (path_to_imagej_fiji_scripts,fiji_script3, local_path_to_used_scripts)
#print string_to_move_used_script    # debug line
os.system(string_to_move_used_script)
#**** ****#
#exit()

#**** deliting the tmp_cut images and 'rm -r tmp_cut' directory ****#
string_to_remove_tmp_cut_directory_and_files = "rm -rf %s" % (local_path_to_tmp_cut)
os.system(string_to_remove_tmp_cut_directory_and_files)
#**** ****#
#exit()

#*****************************************************#
#** END G ********************************************#
#*****************************************************#
#exit()
print

#******************************************************#
#****** H - Making histeresis images and avi movie ****#
#******************************************************#

#**** manual procedure 3 talks ****#
#print
print "*********************************"
print "****** Manual procedure 4 *******"
print "*********************************"
print " After Fiji has started:"
print " (1) press \"[\" key > \"crtl+o\" > open \"%s\" > \"crtl+r\"" % (fiji_script4a)
print "            >>> The normalized tiff frames will be loaded and the treshold plugin will be loaded"
print " (2) Place the plugin box in the lower right corner of the image Do histogran print screan in 50%s windows and moving to right folder stuff" % ("%")
print " (3) Set the lower bound (around) to the peak of the histogran       >>> Close Fiji"
print " (4) Set the upper bound a little before the manbranes start to desapear (this will elimicate very brigth debry pixels)"
print " (5) Take a screen shot to save the treshold reagion info"
print " (6) Press the \"Hyst\" button and wait for the hyst stack to be created (pop up on the screen)"
print " (7) close th script window > press \"[\" key > \"crtl+o\" > open \"%s\" > \"crtl+r\"" % (fiji_script4b)
print "            >>> The 'hyst.tif' files will be saved in \"%s/%s/\" directory" % (d0,d3)
print "            >>> the 'hyst.avi' and 'hyst.jpg' images (that can be used to merge) movie will be saved in \"%s/%s/\" directory" % (d0,d5)
print "            >>> Fiji will close automatically"
print " (8) open nautilus > pictures > copy and paste the screanshots to \"%s/%s/%s/\" directory" % (d0,d1,d1d)
print " (9) rename the image as \"hyst-tresholding-<stack_number>\""
print " (10) mouse mark all the out text in the terminal window and paste to the \"log.prp\" file in \"%s\"" % (local_path_to_log)
#**** ****#
#exit()

#**** modifing fiji script 4a ****#
#print "writing the new script"
file_in_to_open = "%sVOID-%s" % (path_to_pipeline_scripts,fiji_script4a)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
file_out_to_open = "%s%s" % (path_to_imagej_fiji_scripts,fiji_script4a)
file_out = open(file_out_to_open,"w")
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
data=data.replace("<PATH_TO_NORM>",path_to_norm)
data=data.replace("<LINAGE>",d0)
data=data.replace("<NUMBER_OF_STACKS>",str(total_number_of_stacks))
file_out.write(data)
file_in.close()
file_out.close() ; print
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** modifing fiji script 4b ****#
#print "writing the new script"
file_in_to_open = "%sVOID-%s" % (path_to_pipeline_scripts,fiji_script4b)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
file_out_to_open = "%s%s" % (path_to_imagej_fiji_scripts,fiji_script4b)
file_out = open(file_out_to_open,"w")
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
data=data.replace("<LINAGE>",d0)
data=data.replace("<PATH_TO_HYST>",path_to_hyst)
data=data.replace("<PATH_TO_MOVIES_AND_IMAGES>",path_to_movies_and_images)
file_out.write(data)
file_in.close()
file_out.close() ; print
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** executing fiji fourth time ****#
print"\n ***** Fiji Talks 4: "
os.system(string_to_execute_fiji)
print" ***** End Fiji Talks 4. \n"
#**** ****#
#exit()

#**** moving script fiji 4a and 4b to used folder ****#
string_to_move_used_script = "mv %s%s %s." % (path_to_imagej_fiji_scripts,fiji_script4a, local_path_to_used_scripts)
#print string_to_move_used_script    # debug line
os.system(string_to_move_used_script)

string_to_move_used_script = "mv %s%s %s." % (path_to_imagej_fiji_scripts,fiji_script4b, local_path_to_used_scripts)
#print string_to_move_used_script    # debug line
os.system(string_to_move_used_script)
#**** ****#
#exit()

#******************************************************#
#** END H *********************************************#
#******************************************************#
#exit()
print

#***************************************************************************************************#
#**** I - saving the section out information necessary for the rest of pipeline in a "file.prp" ****#
#***************************************************************************************************#
# pipeline will read this file and load the information for teh rest of the sections
file_out_path_and_name = "%sout-info-%s.prp" % (local_path_to_dsoi,d0)
string_to_remove_prp_out_info_file = "rm %s" % (file_out_path_and_name) ; os.system(string_to_remove_prp_out_info_file)
#print file_out_path_and_name    # debug line
file_out = open(file_out_path_and_name,"w")
file_out.write("%s\n"%(local_path_to_norm))
file_out.write("%s\n"%(local_path_to_hyst))
file_out.write("%i\n"%(total_number_of_stacks))
file_out.close()
string_to_make_prp_out_info_file_read_only = "chmod 444 %s" % (file_out_path_and_name)
os.system(string_to_make_prp_out_info_file_read_only)
#***************************************************************************************************#
#** END I ******************************************************************************************#
#***************************************************************************************************#
#exit()
print


#**** making/opening the "log.prp" file ****#
input_string = raw_input("Make log file (yes or no): ")
if input_string == "yes":
    file_out_path_and_name = "%slog-%s.prp" % (local_path_to_log,d0)
    #print file_out_path_and_name  # debug line
    file_out = open(file_out_path_and_name,"w")
    file_out.close()
    string_to_open_log_file = "emacs %s" % (file_out_path_and_name)
    os.system(string_to_open_log_file)
#**** ****#
#exit()


print "******* End of preProcessing *******\n"



#**** moving scripts to used folder (and making it readable only or immutable) ****#
#string_to_move_used_script = "mv %s%s %s." % (path_to_imagej_fiji_scripts,fiji_script1, local_path_to_used_scripts)
#string_to_make_script_readable_only = "chmod 444 %s%s" % (local_path_to_used_scripts,fiji_script1)
#string_to_make_script_immutable = "sudo chattr +i %s%s" % (local_path_to_used_scripts,fiji_script1)
#print string_to_move_used_script ; print string_to_make_script_readable_only ; print string_to_make_script_immutable  # debug line
#os.system(string_to_move_used_script)
#os.system(string_to_make_script_readable_only)
#os.system(string_to_make_script_immutable)
#**** ****#
#exit()

#**** trying to do fiji in headless mode ****#
#when fiji runs headlessly it acts like a one-off program that execute only the requested and then exit
# to run a fiji headlessly run: ./ImageJ-linux64 --ij2 --headless 
# to run a scipt headlessly run: ./ImageJ-linux64 --ij2 --headless --run path/to/script [key1=value1,key2=value2,...]

#string_to_execute_headless_fiji = path_to_imagej_fiji + './' + fiji_executable + ' --ij2 --headless'
#print string_to_execute_headless_fiji+" --console --run hello.py 'name=\"Ms Karen\"'"
#os.system(string_to_execute_headless_fiji+" --console --run hello.py 'name=\"Ms Karen\"'")

#for ele in m_list:
#    extension = ele[len(ele)-4]+ele[len(ele)-3]+ele[len(ele)-2]+ele[len(ele)-1] 
#    print ele, extension
#**** ****#
#exit()

#if len(files_list)!=number_of_files:                                # security routine
#    print 'Error: Te input value "number of files" is different from the number of files in the curent directory.'
#    print '       Closing program...        Obs.: no new .dtl files generated'
#    exit()
