#
# The 1.0 version of this code is the copy of '3sigma_clipping-2.0.py' from 'resultados-parc.../velocity_correlation_function/hacat-m01-hyst_pivtif_data/3sigma_clip/'
#
# Version 2.3 takes ~1min30seg para fazer 200 frames SEM imagens (in pcpaulo) 
#         and takes ~20seg parafazer 1 frame com as 12 imagens --> ~1h15 min para fazer 200 frames (in pcpaulo) 
#
#Uso:
# python Velocity_Field_Frame_maker-Vr_4and3sigma_clipping-2.5.py <LINAGE_MOVIE> <PIV_FOLDER> <NUMBER_OF_FRAMES> <HYST_OR_NORM> <NUMBER_OF_VECTORS> <IMAGE_ON_OR_OFF>
# python Velocity_Field_Frame_maker-Vr_4and3sigma_clipping-2.5.py cal27-m01 PIV 199 hyst 5440 OFF
import sys
import os
import numpy as np
import subprocess as subp
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection

#*********************************************************#
#**** Section (-1): defining plot function ***************#
#*********************************************************#
def plot_quiver(N,block_data,vr,NaN,cmap_type,cmap_max,name_out_image,title,
                vectors_to_eliminate_0, vectors_to_eliminate_1, vectors_to_eliminate_2, vectors_to_replace,
                lista4,lista5,lista6,to_merge):
    X, Y, Vx, Vy, Vr = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N)
    x, y, vx, vy = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N)
    for i in range(N):
        if block_data[i][2] == NaN and block_data[i][3] == NaN:
            X[i], Y[i], Vx[i], Vy[i] = block_data[i][0], block_data[i][1], "NaN", "Nan"
            x[i], y[i], vx[i], vy[i] = block_data[i][0], block_data[i][1], "NaN", "Nan"
            Vr[i] = "NaN"            
        else:
            X[i], Y[i], Vx[i], Vy[i] = block_data[i][0], block_data[i][1], block_data[i][2], block_data[i][3]
            x[i], y[i], vx[i], vy[i] = block_data[i][0], block_data[i][1], block_data[i][2], block_data[i][3]
            Vr[i] = vr[i]
    X, Y, Vx, Vy, Vr = X*0.645, Y*0.645, Vx*0.645, Vy*0.645, Vr*0.645
        
    fig, ax = plt.subplots()
    if to_merge == 'ON':
        # do not mess with  this line below!!!
        plt.figure(figsize=(138.8*1.289, 104.0*1.297), dpi=100) #resolution to merge with 1388x1040 px image of cells monolayer
        # with the 'save_fig dpi=10' the output is a 1394x104? px image
        # do not mess with  this line above!!!
        #** "one yes one not" plot **#
        for i in range(N):
            if (-1)**i == 1:
                vx[i], vy[i] = "NaN","NaN"
        #** **#
        Q = plt.quiver(x,y,2*vx,2*vy,units='xy' ,scale=1.0,color=cmap_type,headwidth=3.0,headlength=5.0,headaxislength=4.5)
        testex=[0,0,1388,1388,48,48,1328,1328]
        testey=[0,1040,0,1040,48,992,48,992]
        q6 = plt.plot(testex,testey,"ro",ms=80.0)
        

        #plt.axis('off')
    else:
        fig.set_size_inches(8.5*1.5, 6.5*1.3)
        ax.set_xlabel("X [$\mu$m]") ; ax.set_ylabel("Y [$\mu$m]") ; ax.set_title(title,fontsize=10)
        #ax.set_aspect('equal')
    
        #some cmap_types: 'RdPu', 'plasma', 'Greens', 'Blues', 'Reds'
        Q = plt.quiver(X,Y,2*Vx,2*Vy,Vr,units='xy' ,scale=1.0,cmap=cmap_type,headwidth=3.0,headlength=5.0,headaxislength=4.5)
        plt.clim(0,cmap_max) ; plt.colorbar(label='Vr [$\mu$m/h]')
        #plt.quiverkey(Q,700.0,700.0,U=2,label="Original field",coordinates="data")
        
    if len(vectors_to_eliminate_0) > 0:
        n=len(vectors_to_eliminate_0)
        #x0, y0, vx0, vy0 = np.zeros(n), np.zeros(n), np.zeros(n), np.zeros(n)
        x0, y0, vx0, vy0 = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N) ; vx0[:],vy0[:]='NaN','NaN'
        for i in range(n):
            index = vectors_to_eliminate_0[i]
            if block_data[index][2] == NaN and block_data[index][3] == NaN:
                x0[i], y0[i] = block_data[index][0], block_data[index][1]
            else:    
                index = vectors_to_eliminate_0[i]
                x0[i], y0[i], vx0[i], vy0[i] = block_data[index][0], block_data[index][1], block_data[index][2]*0.645, block_data[index][3]*0.645
        x0, y0 = x0*0.645, y0*0.645
        
        q0 = ax.quiver(x0,y0,2*vx0,2*vy0,color='black', units='xy',scale=1.0, headwidth=3.0,headlength=5.0,headaxislength=4.5)

    if len(vectors_to_eliminate_1) > 0:
        n=len(vectors_to_eliminate_1)
        #x1, y1, vx1, vy1 = np.zeros(n), np.zeros(n), np.zeros(n), np.zeros(n)
        x1, y1, vx1, vy1 = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N) ; vx1[:],vy1[:]='NaN','NaN'
        for i in range(n):
            index = vectors_to_eliminate_1[i]
            x1[i], y1[i], vx1[i], vy1[i] = block_data[index][0], block_data[index][1], block_data[index][2]*0.645, block_data[index][3]*0.645
        x1, y1 = x1*0.645, y1*0.645
        
        q1 = ax.quiver(x1,y1,2*vx1,2*vy1,color='green', units='xy',scale=1.0, headwidth=3.0,headlength=5.0,headaxislength=4.5)

    if len(vectors_to_eliminate_2) > 0:
        n=len(vectors_to_eliminate_2)
        x2, y2, vx2, vy2 = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N) ; vx2[:],vy2[:]='NaN','NaN'
        for i in range(n):
            index = vectors_to_eliminate_2[i]
            x2[i], y2[i], vx2[i], vy2[i] = block_data[index][0], block_data[index][1], block_data[index][2]*0.645, block_data[index][3]*0.645
        x2, y2 = x2*0.645, y2*0.645
        
        q2 = ax.quiver(x2,y2,2*vx2,2*vy2,color='blue', units='xy',scale=1.0, headwidth=3.0,headlength=5.0,headaxislength=4.5)

    if len(vectors_to_replace) > 0:
        n=len(vectors_to_replace)
        x3, y3, vx3, vy3 = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N) ; vx3[:],vy3[:]='NaN','NaN'
        for i in range(n):
            index = vectors_to_replace[i]
            x3[i], y3[i], vx3[i], vy3[i] = block_data[index][0], block_data[index][1], block_data[index][2]*0.645, block_data[index][3]*0.645
        x3, y3 = x3*0.645, y3*0.645
        
        q3 = ax.quiver(x3,y3,2*vx3,2*vy3,color='red', units='xy',scale=1.0, headwidth=3.0,headlength=5.0,headaxislength=4.5)

    if len(lista4) > 0:
        n=len(lista4)
        x4, y4, vx4, vy4 = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N) ; vx4[:],vy4[:]='NaN','NaN' ; x4[:],y4[:]='NaN','NaN'
        for i in range(n):
            index = lista4[i]
            x4[i], y4[i], vx4[i], vy4[i] = block_data[index][0], block_data[index][1], block_data[index][2]*0.645, block_data[index][3]*0.645
        x4, y4 = x4*0.645, y4*0.645
        
        q4 = ax.plot(x4,y4,"go")

    if len(lista5) > 0:
        n=len(lista5)
        x5, y5, vx5, vy5 = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N) ; vx5[:],vy5[:]='NaN','NaN' ; x5[:],y5[:]='NaN','NaN'
        for i in range(n):
            index = lista5[i]
            x5[i], y5[i], vx5[i], vy5[i] = block_data[index][0], block_data[index][1], block_data[index][2]*0.645, block_data[index][3]*0.645
        x5, y5 = x5*0.645, y5*0.645
        
        q5 = ax.plot(x5,y5,"bo")

    if len(lista6) > 0:
        n=len(lista6)
        x6, y6, vx6, vy6 = np.zeros(N), np.zeros(N), np.zeros(N), np.zeros(N) ; vx6[:],vy6[:]='NaN','NaN' ; x6[:],y6[:]='NaN','NaN'
        for i in range(n):
            index = lista6[i]
            x6[i], y6[i], vx6[i], vy6[i] = block_data[index][0], block_data[index][1], block_data[index][2]*0.645, block_data[index][3]*0.645
        x6, y6 = x6*0.645, y6*0.645
        
        q6 = ax.plot(x6,y6,"bo",ms=0.8)


    # fake up the array of the scalar mappable. urgh...
    #sm = plt.cm.ScalarMappable(cmap='RdPu', norm=plt.Normalize(vmin=0, vmax=120))
    #sm._A = [] ; cbar = plt.colorbar(sm) ; cbar = plt.colorbar(q,clim(0,120)) ; cbar.set_label('Vr [um/h]') 
    
    #plt.grid() ;
    if N == 5440:
        lx=35 ; xmin=X[0]-lx ; xmax=X[84]+lx
        ly=45 ; ymin=Y[5439]-ly ; ymax=Y[0]+ly
    else:
        lx=35 ; xmin=10.32-lx ; xmax=877.20+lx
        ly=45 ; ymin=10.32-ly ; ymax=660.48+ly

    #print (xmax-xmin)/(ymax-ymin)
    #print 1388./1040.
    #exit()

    if to_merge == 'ON':
        plt.xlim(0,1388) ; plt.ylim(0,1040)
        plt.savefig(name_out_image, dpi=10,bbox_inches='tight',pad_inches=0)
    else:
        plt.xlim(xmin,xmax) ; plt.ylim(ymin,ymax)
        plt.savefig(name_out_image, bbox_inches='tight')
    #plt.show()
    plt.close() 
#*********************************************************#
#**** END : Section (-1) *********************************#
#*********************************************************#
#exit()
print

#**********************************************************************#
#**** Section 0: Making directories tree and defining in variables ****#
#**********************************************************************#

#**** defining the local "global" NaN variable ****# 
NaN = 999999.0
#**** ****#
#exit()

#**** in variables ****#
d0 = sys.argv[1] ; # d0 = <d0> = linage-movie directory
local_path_to_piv_data = sys.argv[2] ; 
hyst_or_norm = sys.argv[3] ; # hyst_or_norm = <hyst_or_norm> = "hyst" or "norm", the piv file type to be used
extension = sys.argv[4];  # path to pipeline
N_frames = int(sys.argv[5]) ; # N_frames = <number_of_piv_files> = <number_of_stacks-1>
original_N_vectors = int(sys.argv[6]) ; # origial_N_vectors = <original_N_vectors> = <number_of_piv_windows>
images_ON_or_OFF = sys.argv[7] ; # images_ON_or_OFF = "ON" or "OFF"
path_to_pipeline = sys.argv[8];  # path to pipeline
local_path_to_dsoi = sys.argv[9]
non_interactive_mode = sys.argv[10]
#print d0+"\n"+local_path_to_piv_data+"\n"+hyst_or_norm+"\n"+extension+"\n"+str(N_frames)+"\n"+str(original_N_vectors)+"\n"+images_ON_or_OFF+"\n"+path_to_pipeline+"\n"+local_path_to_dsoi+"\n"+non_interactive_mode   # debug line
#**** ****#
#exit()

#**** making directories ****#
d7 = "VFF"  ; os.system("mkdir -p %s/%s" % (d0,d7))
d7field = "Field"  ; os.system("mkdir -p %s/%s/%s" % (d0,d7,d7field)) # velocity field frames and data
d7fluctu = "FluctuField" ; os.system("mkdir -p %s/%s/%s" % (d0,d7,d7fluctu)) # fluctuation field and data
d7log = "LOG" ; os.system("mkdir -p %s/%s/%s" % (d0,d7,d7log)) # Log files
d7logf = "frames_log" ; os.system("mkdir -p %s/%s/%s/%s/" % (d0,d7,d7log,d7logf))
d7bors = "BORS" ; os.system("mkdir -p %s/%s/%s" % (d0,d7,d7bors)) # Outliers Removal Steps images
d7borsd = "0_detailed_steps" ; os.system("mkdir -p %s/%s/%s/%s" % (d0,d7,d7bors,d7borsd)) # Outliers Removal detailed Steps images
d7ftm = "FTM" ; os.system("mkdir -p %s/%s/%s/" % (d0,d7,d7ftm)) # Frames To Merge (images)
d7ftmY = "Y" ; os.system("mkdir -p %s/%s/%s/%s/" % (d0,d7,d7ftm,d7ftmY)) # Yellow frames
d7ftmL = "L" ; os.system("mkdir -p %s/%s/%s/%s/" % (d0,d7,d7ftm,d7ftmL)) # Lime frames
d7ftmM = "M" ; os.system("mkdir -p %s/%s/%s/%s/" % (d0,d7,d7ftm,d7ftmM)) # Magenta frames
d7quali = "Quality" ; os.system("mkdir -p %s/%s/%s" % (d0,d7,d7quali)) # Quality files/images related to the bors
d7qualif = "frames_M13" ; os.system("mkdir -p %s/%s/%s/%s/" % (d0,d7,d7quali,d7qualif)) # Quality files/images related to the bors
#**** ****#
#exit()

#**** defining necessary paths ****#
#path_to_vff = "%s%s/%s/" % (path_to_pipeline,d0,d7)
local_path_to_vff = "%s/%s/" % (d0,d7)

#path_to_field = "%s%s/%s/%s/" % (path_to_pipeline,d0,d7,d7field)
local_path_to_field = "%s/%s/%s/" % (d0,d7,d7field)

#path_to_fluctu = "%s%s/%s/%s/" % (path_to_pipeline,d0,d7,d7fluctu)
local_path_to_fluctu = "%s/%s/%s/" % (d0,d7,d7fluctu)

#path_to_log = "%s%s/%s/%s/" % (path_to_pipeline,d0,d7,d7log)
local_path_to_log = "%s/%s/%s/" % (d0,d7,d7log)

#path_to_bors = "%s%s/%s/%s/" % (path_to_pipeline,d0,d7,d7bors)
local_path_to_bors = "%s/%s/%s/" % (d0,d7,d7bors)

#path_to_ftm = "%s%s/%s/%s/" % (path_to_pipeline,d0,d7,d7ftm)
local_path_to_ftm = "%s/%s/%s/" % (d0,d7,d7ftm)

#path_to_quali = "%s%s/%s/%s/" % (path_to_pipeline,d0,d7,d7quali)
local_path_to_quali = "%s/%s/%s/" % (d0,d7,d7quali)

#path_to_qualif = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d7,d7quali,d7qualif)
local_path_to_qualif = "%s/%s/%s/%s/" % (d0,d7,d7quali,d7qualif)

#path_to_logf = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d7,d7log,d7logf)
local_path_to_logf = "%s/%s/%s/%s/" % (d0,d7,d7log,d7logf)

#path_to_borsd = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d7,d7bors,d7borsd)
local_path_to_borsd = "%s/%s/%s/%s/" % (d0,d7,d7bors,d7borsd)

#path_to_Y = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d7,d7ftm,d7ftmY)
local_path_to_Y = "%s/%s/%s/%s/" % (d0,d7,d7ftm,d7ftmY)
#path_to_L = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d7,d7ftm,d7ftmL)
local_path_to_L = "%s/%s/%s/%s/" % (d0,d7,d7ftm,d7ftmL)
#path_to_M = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d7,d7ftm,d7ftmM)
local_path_to_M = "%s/%s/%s/%s/" % (d0,d7,d7ftm,d7ftmM)
#**** ****#
#exit()

#**** loading the list of in (".piv") files to be used ****#
string_to_list_piv_files  = "ls %s*%s????.piv%s" % (local_path_to_piv_data,hyst_or_norm,extension)
#os.system(string_to_list_piv_files) # debug line
f_string = subp.check_output(string_to_list_piv_files,shell=True) # string of all ".piv" files to be used 
#print f_string    # debug line
list_of_piv_files = f_string.split("\n")  ;  list_of_piv_files.remove("")
#print list_of_piv_files, len(list_of_piv_files)   # debug line

#N_frames = 85   #  debug line
if len(list_of_piv_files) != N_frames:     #security routine
    print "ERROR:   number of in \".piv%s\" files is diferent from expected number of frames" % (extension)
    print "         Expected number of frames = ", N_frames
    print "         number of in files =", len(list_of_piv_files)
    print "code stoped!"
    exit()
#**** ****#
#exit()

#**********************************************************************#
#**** END : Section 0 *************************************************#
#**********************************************************************#
#exit()
print

#****************************************************#
#**** Section 1: Pre "Super section" definitions ****#
#****************************************************#
# Defining lists and opening files that need to be open out of the super section (1)  

#**** making quality list for plot ****#
list_M12_index_and_value = []
list_M13_index_and_value = []
#**** ****#
#exit()

#**** making quality block data to be make graphs (for plot) ****#
Quality_block_data = []
#**** ****#
#exit()

#**** opening and writing the header of quality file (here still with "tmp" name)
tmp_file_out_4_name = local_path_to_vff+"tmp_file_out_4.vaf"
tmp_file_out_4 = open(tmp_file_out_4_name,"w")
stp = "# Outliers_over_total[%] = Number_of_replaced_vectors/total_number_of_vectors\n"  
stp += "# Outliers_over_final[%] = Number_of_replaced_vectors/final_number_of_vectors\n"
stp += "#\n"
stp += "# frame   Outliers_over_final[%]   Outliers_over_total[%]   mean_M12   stdv_M12  mean_M13   stdv_M13  -->  M12=12 and M13=13 is equal to max quality\n"
tmp_file_out_4.write(stp)
#**** ****#
#exit()

#**** defining list the will recive the frmes with local M<5 ****#
list_of_frames_with_M13_cross_lt_8 = []    # this M value is equal to the number of valid neighbors for the 13 cross of vectors
list_of_frames_with_M12_cross_lt_7 = []    # this M value is equal to the number of valid neighbors for the 13 cross of vectors
#**** ****#
#exit()

#****************************************************#
#**** END : Section 1 *******************************#
#****************************************************#
#exit()
print

### ******************************************************* ###
###      *********************************************      ### 
###                    Super Section (1)                    ###
###                LOOP OVER IN FILES LISTS                 ###
###      *********************************************      ###
### ******************************************************* ###

print "   VFF loop:\n\n"
for frame_index in range(N_frames):

    #************************************************************************#
    #**** (A) - defining in and out data files names and open log file ******#
    #************************************************************************#

    #**** defining in file name and labels ****#
    in_piv_file_path_and_name = list_of_piv_files[frame_index]
    #print in_piv_file_path_and_name    # debug line
    name_and_extension_list = in_piv_file_path_and_name.split("%s"%(local_path_to_piv_data)) ; name_and_extension_list.remove(name_and_extension_list[0])
    name_and_extension_list = name_and_extension_list[0].split(".")
    #print name_and_extension_list    # debug line
    in_piv_file_name = name_and_extension_list[0]+"."+name_and_extension_list[1]
    #print in_piv_file_name # debug line
    frame_number = name_and_extension_list[0].split(hyst_or_norm) ; frame_number = frame_number[1]
    #print frame_number  # debug line
    name_label_quality_file = in_piv_file_name.replace(frame_number+".","-")
    #print name_label_quality_file  # debug line
    #**** ****#
    #exit()

    #**** defining out files names ****#
    #** Field files:
    file_out_1_path_and_name = local_path_to_field + name_and_extension_list[0]+"-"+name_and_extension_list[1]+".vff"
    file_out_1_name = file_out_1_path_and_name.replace(local_path_to_field,"")
    #print file_out_1_path_and_name, file_out_1_name    # debug line
    #** Fluctu files:
    file_out_2_path_and_name = local_path_to_fluctu + name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-fluctu.vff"
    file_out_2_name = file_out_2_path_and_name.replace(local_path_to_fluctu,"")
    #print file_out_2_path_and_name, file_out_2_name    # debug line
    #** LOG files:
    file_out_3_path_and_name = local_path_to_logf + "log-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+".vff"
    #print file_out_3_path_and_name    # debug line
    file_out_4_path_and_name = local_path_to_quali + "quality-"+name_label_quality_file+".vff"
    file_out_4_name = file_out_4_path_and_name.replace(local_path_to_quali,"")
    #print file_out_4_path_and_name, file_out_4_name    # debug line
    file_out_5_path_and_name = local_path_to_qualif + "quality-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-M13.vff"
    #print file_out_5_path_and_name    # debug line
    #**** ****#
    #exit()

    #**** defining BORS out images names ****#
    #* BORS images:                                          
    bors_A_image_out_path_and_name = local_path_to_bors + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-A.png"
    #print bors_A_image_out_path_and_name # debug line
    bors_B_image_out_path_and_name = local_path_to_bors + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-B.png"
    #print bors_B_image_out_path_and_name # debug line
    bors_C_image_out_path_and_name = local_path_to_bors + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-C.png"
    #print bors_C_image_out_path_and_name # debug line
    bors_D_image_out_path_and_name = local_path_to_bors + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-D.png"
    #print bors_D_image_out_path_and_name # debug line
    #*** detailed steps bors images: 
    bors_A1_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-A1.png"
    #print bors_A1_image_out_path_and_name # debug line
    bors_A2_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-A2.png"
    #print bors_A2_image_out_path_and_name # debug line
    bors_A3_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-A3.png"
    #print bors_A3_image_out_path_and_name # debug line
    bors_B1_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-B1.png"
    #print bors_B1_image_out_path_and_name # debug line
    bors_B2_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-B2.png"
    #print bors_B2_image_out_path_and_name # debug line
    bors_B3_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-B3.png"
    #print bors_B3_image_out_path_and_name # debug line
    bors_B4_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-B4.png"
    #print bors_B4_image_out_path_and_name # debug line
    bors_C1_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-C1.png"
    #print bors_C1_image_out_path_and_name # debug line
    bors_C2_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-C2.png"
    #print bors_C2_image_out_path_and_name # debug line
    bors_C3_image_out_path_and_name = local_path_to_borsd + "bors-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-STEP-C3.png"
    #print bors_C3_image_out_path_and_name # debug line
    #**** ****#
    #exit()
    
    #**** defining Field  and Fluctu out images names ****#
    #** Field images:
    field_image_out_path_and_name = local_path_to_field + name_and_extension_list[0]+"-"+name_and_extension_list[1]+".png"
    #print field_image_out_path_and_name # debug line
    #** Fluctu images:
    fluctu_image_out_path_and_name = local_path_to_fluctu + name_and_extension_list[0]+"-"+name_and_extension_list[1]+"-fluctu.png"
    #print fluctu_image_out_path_and_name # debug line
    #**** ****#
    #exit()
    
    #**** defining the FTM out images names ****#
    #** field:
    ftm_fiY_image_out_path_and_name = local_path_to_Y + "ftm-field-Y-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+".png"
    #print ftm_fiY_image_out_path_and_name # debug line
    ftm_fiL_image_out_path_and_name = local_path_to_L + "ftm-field-L-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+".png"
    #print ftm_fiL_image_out_path_and_name # debug line
    ftm_fiM_image_out_path_and_name = local_path_to_M + "ftm-field-M-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+".png"
    #print ftm_fiM_image_out_path_and_name # debug line
    #** fluctu:
    ftm_fuY_image_out_path_and_name = local_path_to_Y + "ftm-fluctu-Y-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+".png"
    #print ftm_fuY_image_out_path_and_name # debug line
    ftm_fuL_image_out_path_and_name = local_path_to_L + "ftm-fluctu-L-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+".png"
    #print ftm_fuL_image_out_path_and_name # debug line
    ftm_fuM_image_out_path_and_name = local_path_to_M + "ftm-fluctu-M-"+name_and_extension_list[0]+"-"+name_and_extension_list[1]+".png"
    #print ftm_fuM_image_out_path_and_name # debug line
    #**** ****#
    #exit()

    #**** opening the frame log file ****#
    log_file = open(file_out_3_path_and_name,"w")
    #**** ****#
    #exit()

    #**** opening the quali frame M13 file ****#
    M13_file = open(file_out_5_path_and_name,"w")
    #**** ****#
    #exit()
    
    #*****************************************************************#
    #**** END : (A) **************************************************#
    #*****************************************************************#
    #exit()
    #print

    #*****************************************************************************#
    #**** (B) - reading/loading the in data and making/saving to a block_data ****#
    #*****************************************************************************#
    
    #**** loading the piv in data ****#
    in_piv_file = open(in_piv_file_path_and_name,"r")
    in_piv_data = in_piv_file.read()
    in_piv_data = in_piv_data.split("\n")   # it is a list now
    in_piv_data.remove("") ; in_piv_data.remove(in_piv_data[0]) ; in_piv_data.remove(in_piv_data[0])
    in_piv_file.close()
    #print in_piv_data, len(in_piv_data)    # debug line
    
    if len(in_piv_data) != original_N_vectors:     #security routine
        print "ERROR:   in piv file number of (data) lines is diferent from expected numer of vectors (piv windows)"
        print "         Expected number of vectors = ", original_N_vectors
        print "         in file number of lines =", len(in_piv_data)
        print "code stoped!"
        exit()
        
    nol = len(in_piv_data) # = number_of_lines
    for j in range(nol):
        in_piv_data[j]=in_piv_data[j].split('\t')
    #print in_piv_data, len(in_piv_data)    # debug line
    #**** ****#
    #exit()

    #**** making/saving the vector field (original piv) in data ****# 
    original_block_data = [[0. for j in range(5)] for i in range(original_N_vectors)]
    for i in range(original_N_vectors):
        counter=0
        for ele in in_piv_data[i]:
            original_block_data[i][counter] = float(ele)
            counter+=1
    #for ele in original_block_data: print ele   # debug line
    #**** ****#
    #exit()

    #**** making the "original vr" (used only for the firsts BORS plots) ****#
    original_vr = np.zeros(original_N_vectors)
    for i in range(original_N_vectors):
        original_vr[i] = np.sqrt(original_block_data[i][2]*original_block_data[i][2] + original_block_data[i][3]*original_block_data[i][3])
        #print vr[i] # debug line
        #if vr[i] < 1.00: print i, vr[i]  # debug line
    #**** ****#
    #exit()
    
    #**** check with there are V=0 in "in piv" data ****#
    stp = "Relation of vectors with V=0.0 in \"in PIV\" data:\n"
    stp += "                 index  X   Y   Vx   Vy\n"
    counter = 0
    for index in range(original_N_vectors):
    #for line_data in original_block_data:
        line_data = original_block_data[index]
        if line_data[2] == 0.0 and line_data[3] == 0.0:
            stp += "Vr == 0.0:       %i %f %f %f %f\n" % (index,line_data[0],line_data[1],line_data[2],line_data[3])
            counter += 1
        elif line_data[2] == 0.0:
            stp += "Vx or Vy == 0.0: %i %f %f %f %f\n" % (index,line_data[0],line_data[1],line_data[2],line_data[3])
            counter += 1
        elif line_data[3] == 0.0:
            stp += "Vx or Vy == 0.0: %i %f %f %f %f\n" % (index,line_data[0],line_data[1],line_data[2],line_data[3])
            counter += 1
    if counter == 0:
        stp +=      "No vector with Vx, Vy, or Vr equals to zero in \"in PIV\" data.\n"
    #** log statement:
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()
    
    #*******************************************************************#
    #**** END : (B) ****************************************************#
    #*******************************************************************#
    #exit()
    #print

    #***********************************************************#
    #**** (B-C) Removing the borders from PIV original data ****#
    #***********************************************************#

    #**** making list of bulk vectors indexes (i.e. to be mantained for BORS) ****#
    bulk_index=[]
    for i in xrange(2,62,1):
        a=85*i+3 ; b=85*(i+1)-2
        for j in xrange(a,b+1,1):
            bulk_index.append(j-1)
    #** log statement:
    stp = "bulk_index[0] = %i                                # bulk[0] must be equal to (173-1 = 172) for 85x64 grid\n"%(bulk_index[0])
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()

    #**** making list of border vectors indexes (i.e. will be removed in this section) ****#
    border_index=[]
    for index in range(original_N_vectors): border_index.append(index)
    for index in bulk_index: border_index.remove(index)
    #print border_index ; print len(border_index)  # debug line
    #** log statement:
    stp = "len(bulk_index)+len(border_index) = %i    # must be equal to N (N=5440 for 85x64 grid)\n"%(len(bulk_index)+len(border_index))
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()

    #**** updating the "number of vectors" ****#
    #old_N_vectors = N_vectors
    N_vectors = original_N_vectors - len(border_index)
    #print  original_N_vectors, N_vectors   # debug line
    #**** ****#
    #exit()

    #**** initializing/bulding "Vector field" block_data (not clean data) ****#
    # ps: block_data only becomes "VectorField" after BORS are fineshed
    block_data = [[0. for j in range(5)] for i in range(N_vectors)]
    #** log statement:
    stp = "Borders removed\n"
    stp += "Grid size updated: The new grid is 81x60 lattice --> N = 4860 vectors\n"
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()

    #**** defining the vector field not clean data ****#
    j=-1
    for index in bulk_index:
        i=index ; j+=1
        block_data[j][:] = original_block_data[i][:]
    #**** ****#
    #exit()
    
    #***************************************************************#
    #**** END : (B-C) **********************************************#
    #***************************************************************#
    #exit()
    #print
    
    #****************************************************************************************************#
    #**** (C) - Making/defining other required structures: clean block data, (non)cruzable index, vr ****#
    #****************************************************************************************************#

    #**** making vector field that will have the CLEAN data ****#
    block_data_clean = [[0. for j in range(5)] for i in range(N_vectors)]
    for i in range(N_vectors):
        counter=0
        for ele in block_data[i]:
            block_data_clean[i][counter] = float(ele)
            counter+=1
        #block_data_clean[i][counter-1] = float("NaN")  #definindo a quinta coluna "M12": M12 = NaN ---> vetor original (non replaced)
        block_data_clean[i][counter-1] = -1 #definindo a quinta coluna "M12": M12 = -1 ---> vetor original (non replaced)
    #for ele in block_data_clean: print ele   # debug line
    #**** ****#
    #exit()
    
    #**** making list of index that are cruzables (13 vector cross) ****#
    cruzable_index=[]
    for i in xrange(2,58,1):
    #for i in xrange(2,62,1):
        a=81*i+3 ; b=81*(i+1)-2
        #a=85*i+3 ; b=85*(i+1)-2
        for j in xrange(a,b+1,1):
            cruzable_index.append(j-1)
    #** log statement:
    stp = "cruzable_index[0] = %i                                # cruzable[0] must be equal to (165-1 = 164) for 81x60 grid\n"%(cruzable_index[0])
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()    
    
    #**** making list of index that are NOT cruzables ****#
    non_cruzable_index=[]
    for index in range(N_vectors): non_cruzable_index.append(index)
    for index in cruzable_index: non_cruzable_index.remove(index)
    #print non_cruzable_index ; print len(non_cruzable_index)  # debug line
    #** log statement:
    stp = "len(cruzable_index)+len(non_cruzable_index) = %i    # must be equal to N (N=4860 for 81x60 grid)\n"%(len(cruzable_index)+len(non_cruzable_index))
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()

    #**** making the "vr" and "vr_clean" vectors ****#
    vr = np.zeros(N_vectors)
    vr_clean = np.zeros(N_vectors)
    for i in range(N_vectors):
        vr[i] = np.sqrt(block_data[i][2]*block_data[i][2] + block_data[i][3]*block_data[i][3])
        vr_clean[i] = vr[i]
        #print vr[i], vr_clean[i] # debug line
        #if vr[i] < 1.00: print i, vr[i],vr_clean[i]  # debug line
    #**** ****#
    #exit()
    
    #**************************************************************************************************#
    #**** END : (C) ***********************************************************************************#
    #**************************************************************************************************#
    #exit()
    #print

    
    ###### not in use
    #**** Ploting NaN grids to check if cruzable sets are okay ****#
    ##n=len(cruzable_index)
    ##
    ##X1, Y1, X2, Y2  = np.zeros(N), np.zeros(N), np.zeros(n), np.zeros(n)
    ##for i in range(N):
    ##    X1[i], Y1[i] = block_data[i][0]*0.645, block_data[i][1]*0.645
    ##
    ##for i in range(n):
    ##    index=cruzable_index[i]
    ##    X2[i], Y2[i] = block_data[index][0]*0.645, block_data[index][1]*0.645
    ##          
    ##fig, ax = plt.subplots()
    ##q = ax.plot(X1,Y1,'b+',X2,Y2,'ro',ms=1.5)
    ##plt.xlabel("X [um]")
    ##plt.ylabel("Y [um]")
    ##plt.title('Red dots are the "cruzable" vectors',fontsize=10)
    ##
    ##fig = plt.gcf()
    ##fig.set_size_inches(8.5*1.2, 6.5*1.2)
    ##
    ##plt.savefig('cruzable_vectors_in_grid.png', bbox_inches='tight', dpi=100)
    ##plt.show()
    ##plt.close()
    #**** ****#
    #exit()
    ###### END not in use

    
    #**************************************************************************#
    #**** (D) - First elimination process: 4 sigma "mean vr" clip (global) ****#
    #**************************************************************************#

    #**** initialzazing loop variables ****#
    sum_vr = 0. ; mean_vr = 0.
    sum_vr2 = 0. ; mean_vr2 = 0.
    stdv_vr = 0.
    M = N_vectors
    #**** ****#
    #exit()

    #**** calculating "mean vr" and "stdv vr" for the frame ****#
    for i in range(N_vectors):
        sum_vr += vr[i]
        sum_vr2 += vr[i]*vr[i]
        
    mean_vr = sum_vr/M ; mean_vr2 = sum_vr2/M
    stdv_vr = np.sqrt( (M/(M-1)) * (mean_vr2 - mean_vr*mean_vr) )
    #**** ****#
    #exit()

    #**** First elimination loop! ****#
    stp = ""
    vectors_to_eliminate_1 = []
    for index in range(N_vectors):
        i=index
        if vr[i] > mean_vr+4*stdv_vr:
            vectors_to_eliminate_1.append(index)
            #print index, mean_vr, stdv_vr, "     ", vr[i], ">", mean_vr+4*stdv_vr  # debug line
            stp += "  %i %f %f     %f > %f\n" % (index, mean_vr,stdv_vr,vr[i],mean_vr+4*stdv_vr)
            block_data_clean[i][2], block_data_clean[i][3] = NaN,NaN
            vr_clean[i] = NaN
    #print vectors_to_eliminate_1   #  debug line
    #** log statement:
    stp = "Vectors eliminated via 1:\n  index mean_vr stdv_vr       vr[index] > mean_vr+4*stdv_vr)\n" + stp
    stp = stp + " Number of vectors eliminated: %i\n"% (len(vectors_to_eliminate_1))
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()
    
    #***************************************#
    #**** END : (D) ************************#
    #***************************************#
    #exit()
    #print

    #***************************************************************************************************************************************************#
    #**** (D-E) - Exporting Borders and Outliers Removal Steps (BORS) images (the first five) and define the axiliar clean data for next BORS plot *****#
    #***************************************************************************************************************************************************#

    if images_ON_or_OFF == "ON":
        #**** defining plot function intent in variables ****#
        title = in_piv_file_name
        out_image_A = bors_A_image_out_path_and_name    
        out_image_1 = bors_A1_image_out_path_and_name    
        out_image_2 = bors_A2_image_out_path_and_name    
        out_image_3 = bors_A3_image_out_path_and_name    
        out_image_4 = bors_B1_image_out_path_and_name    
        out_image_5 = bors_B2_image_out_path_and_name    
        #**** ****#
        #exit()
        
        #**** making and exporting the images ****#
        plot_quiver(original_N_vectors,original_block_data,original_vr,NaN, 'RdPu',120,out_image_A,title, [],[],[],[],[],[],[],'')

        plot_quiver(original_N_vectors,original_block_data,original_vr,NaN, 'RdPu',120,out_image_1,title, [],[],[],[],[],[],[],'')
        plot_quiver(original_N_vectors,original_block_data,original_vr,NaN, 'RdPu',120,out_image_2,title, border_index,[],[],[],[],[],[],'')
        plot_quiver(N_vectors,block_data,vr,NaN, 'RdPu',120,out_image_3,title, [],[],[],[],[],[],[],'')
        
        plot_quiver(N_vectors,block_data,vr,NaN, 'RdPu',120,out_image_4,title, [],vectors_to_eliminate_1,[],[],[],[],[],'')
        plot_quiver(N_vectors,block_data_clean,vr_clean,NaN, 'RdPu',120,out_image_5,title, [],[],[],[],vectors_to_eliminate_1,[],[],'')
        #**** ****#
        #exit()

        #**** making auxiliar block data for BORS plot step 5 ****#
        block_data_clean_bors = [[0. for j in range(5)] for i in range(N_vectors)]
        for i in range(N_vectors):
            counter=0
            for ele in block_data_clean[i]:
                block_data_clean_bors[i][counter] = ele
                counter+=1
        #for ele in block_data_clean_bors: print ele   # debug line
        #**** ****#
        #exit()

    #***************************************************************************************************************************************************#
    #**** END : (D-E) **********************************************************************************************************************************#
    #***************************************************************************************************************************************************#
    #exit()
    #print
    

    #***********************************************************************************#
    #**** (E) - Second elimination process: 3 sigma "vr" clip over 13 cross (local) ****#
    #***********************************************************************************#

    #**** making the valid cruzables indexes list after the First Elimination Loop (FEL) ****#
    # this list is used in the second elimination process
    valid_cruzable_indexes_after_FEL = []
    for index in cruzable_index: valid_cruzable_indexes_after_FEL.append(index)
    for index in vectors_to_eliminate_1:
        if index not in non_cruzable_index:
            #print index, "cruzable index"    # debug line
            valid_cruzable_indexes_after_FEL.remove(index)
        #else: print index, "non cruzable index"    # debug line
    #for index in vectors_to_eliminate_1: print block_data_clean[index][:], vr_clean[index]   # debug line
    #**** ****#
    #exit()
        
    #**** initializing loop variables ****#
    sum_vr13 = np.zeros(N_vectors) ; mean_vr13 = np.zeros(N_vectors)
    sum_vr13_2 = np.zeros(N_vectors) ; mean_vr13_2 = np.zeros(N_vectors)
    stdv_vr13 = np.zeros(N_vectors)
    M13_after_FEL = ["NaN"]*N_vectors  # after First Elimination Loop --> max valeu == 13
    for index in non_cruzable_index:
        M13_after_FEL[index] = "non_cruzable"
    mean_vr13[:] = float("NaN")
    stdv_vr13[:] = float("NaN")
    #**** ****#
    #exit()

    #**** calculating 13 cross "mean vr" and "sdtv vr" ****#
    is_this_frame_already_added_as_an_M13_lt_8 = "NO"
    for index in valid_cruzable_indexes_after_FEL:
        i=index
        cruz13 = [i,(i+1),(i+2),(i-1),(i-2),(i-1*81),(i-2*81),(i+1*81),(i+2*81),(i-1*81+1),(i-1*81-1),(i+1*81+1),(i+1*81-1)]
        #print cruz13  # debug line
        M13_after_FEL[i] = 0 ; mean_vr13[i] = 0. ; stdv_vr13[i] = 0.
        #M13_after_FEL[i]=1 # debug line
        for cross_index in cruz13:
            #print cross_index, vr_clean[cross_index], type(vr_clean[cross_index])  # debug line
            if vr_clean[cross_index] != NaN:
                sum_vr13[i] += vr_clean[cross_index]
                sum_vr13_2[i] += vr_clean[cross_index]*vr_clean[cross_index]
                M13_after_FEL[i] += 1
                
        M13 = M13_after_FEL[i]
        if M13 < 8:
            if is_this_frame_already_added_as_an_M13_lt_8 == "NO":
                list_of_frames_with_M13_cross_lt_8.append(frame_number)
                is_this_frame_already_added_as_an_M13_lt_8 = "YES"
            print "M13 < 8--> this frame should not be used (but this will be user choice though)", index, M13, M13_after_FEL[i], cross_index
            if M13 == 0:
                print "deu merda (1): M13 == 0 ---> codigo stoped"
                exit()
            elif M13 == 1:
                mean_vr13[i] = sum_vr13[i]/M13 ; mean_vr13_2[i] = sum_vr13_2[i]/M13
                stdv_vr13[i] = 0.
            else:
                mean_vr13[i] = sum_vr13[i]/M13 ; mean_vr13_2[i] = sum_vr13_2[i]/M13
                stdv_vr13[i] = np.sqrt( (M13/(M13-1)) * (mean_vr13_2[i] - mean_vr13[i]*mean_vr13[i]) )
        else:
            #print M13, sum_vr13[i], sum_vr13_2[i]   # debug line
            mean_vr13[i] = sum_vr13[i]/M13 ; mean_vr13_2[i] = sum_vr13_2[i]/M13
            stdv_vr13[i] = np.sqrt( (M13/(M13-1)) * (mean_vr13_2[i] - mean_vr13[i]*mean_vr13[i]) )
    #**** ****#
    #exit()
    
    #**** writing data and closing M13 frame quality file ****#
    stp = "# X[px]  Y[px]  Cruz13_mean_vr[um/h]  Cruz13_stdv_vr[um/h]  M13[#]  ---> M13=13 max quality, M13=1 lowest quality\n"
    for index in range(N_vectors):
        stp += "%f %f %f %f %s\n" % (block_data_clean[index][0],block_data_clean[index][1],mean_vr13[index]*0.645,stdv_vr13[index]*0.645,str(M13_after_FEL[index]))
    M13_file.write(stp)
    #**** ****#
    #exit()    
    
    #**** calculating the mean M13 quality (and stdv M13) of the replaced vectors (it will be used only in "section L") ****# 
    sum_M13 = 0 ; sum_M13_2 = 0
    mean_M13 = 0. ; stdv_M13 = 0.
    N13 = 0
    for M13 in M13_after_FEL:
        if M13 != "non_cruzable" and M13 != "NaN":
            if M13 not in [1,2,3,4,5,6,7,8,9,10,11,12,13]:
                print "deu merda (2): M13 has invalid value --> code stoped"
                exit()
            else:
                #print M13     # debug line
                sum_M13 += float(M13)
                sum_M13_2 += float(M13)*float(M13)
                N13 += 1
    #print N13, sum_M13, sum_M13_2  # debug line
    mean_M13 = sum_M13/N13 ;  mean_M13_2 = sum_M13_2/N13
    stdv_M13 = np.sqrt( (N13/(N13-1)) * (mean_M13_2 - mean_M13*mean_M13) )
    #print N13, mean_M13, stdv_M13  # debug line
    #**** ****#
    #exit()
    
    #**** Second elimination loop! ****#
    stp = ""
    vectors_to_eliminate_2 = []
    for index in valid_cruzable_indexes_after_FEL:
        i=index
        if vr_clean[i] > mean_vr13[i]+3*stdv_vr13[i]:
            vectors_to_eliminate_2.append(index)
            stp += "  %i %f %f     %f > %f\n" % (index, mean_vr13[i],stdv_vr13[i],vr_clean[i],mean_vr13[i]+3*stdv_vr13[i])
            block_data_clean[i][2], block_data_clean[i][3] = NaN, NaN
            vr_clean[i] = NaN
    #** log statement:
    stp = "Vectors eliminated via 2:\n  index mean_vr13 stdv_vr13       vr[index] > mean_vr13+3*stdv_vr13)\n" + stp
    stp = stp + " Number of vectors eliminated: %i\n"% (len(vectors_to_eliminate_2))
    log_file.write(stp)
    log_file.write("\n")
    #** log statement:
    stp = "Total number of vectors (N_vectors): %i\n" % (N_vectors)
    stp += "Number of outliers vectors: %i\n" % (len(vectors_to_eliminate_1)+len(vectors_to_eliminate_2))
    stp += "Outliers percentage (over all vectors): %2.2f%s\n" % ((len(vectors_to_eliminate_1)+len(vectors_to_eliminate_2))/float(N_vectors)*100,"%")
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()

    #**********************************************************************************#
    #**** END : (E) *******************************************************************#
    #**********************************************************************************#
    #exit()
    #print

    #****************************************************************************#
    #**** (E-F) - Exporting Borders and Outliers Removal Steps (BORS) images ****#
    #****************************************************************************#

    if images_ON_or_OFF == "ON":
        #**** defining plot function intent in variables ****#
        title = in_piv_file_name
        out_image_B = bors_B_image_out_path_and_name
        out_image_6 = bors_B3_image_out_path_and_name
        out_image_7 = bors_B4_image_out_path_and_name    
        #**** ****#
        #exit()
        
        #**** making and exporting the images ****#
        plot_quiver(N_vectors,block_data,vr,NaN, 'RdPu',120,out_image_B,title, [],vectors_to_eliminate_1,vectors_to_eliminate_2,[],[],[],[],'')

        plot_quiver(N_vectors,block_data_clean_bors,vr_clean,NaN, 'RdPu',120,out_image_6,title, [],[],vectors_to_eliminate_2,[],vectors_to_eliminate_1,[],[],'')
        plot_quiver(N_vectors,block_data_clean,vr_clean,NaN, 'RdPu',120,out_image_7,title, [],[],[],[],vectors_to_eliminate_1,vectors_to_eliminate_2,[],'')
        #**** ****#
        #exit()
        #print 
    
    #****************************************************************************#
    #**** END : (E-F) ***********************************************************#
    #****************************************************************************#
    #exit()
    #print

    #***********************************************************************************#
    #**** (F) - Replacing the vectors by vectorial mean value of 12 cross neighbors ****#
    #***********************************************************************************#

    #**** Making cruzables and non_cruzables to replace index lists ****#
    cruzables_to_replace=[]
    non_cruzables_to_replace=[]
    for index in vectors_to_eliminate_2: cruzables_to_replace.append(index)
    #print cruzables_to_replace ; print vectors_to_eliminate_2   # debug line
    for index in vectors_to_eliminate_1:
        if index not in non_cruzable_index:
            cruzables_to_replace.append(index)
        else:
            non_cruzables_to_replace.append(index)
    cruzables_to_replace.sort()
    non_cruzables_to_replace.sort()
    #print cruzables_to_replace ; print non_cruzables_to_replace # debug line
    #** log statement:
    stp = "Number of cruzables to replace: %i\n" % (len(cruzables_to_replace))
    stp += "Number of non cruzables to replace: %i\n" % (len(non_cruzables_to_replace))
    log_file.write(stp)
    log_file.write("\n")
    #** Security routine:   this two routines are here to make shure that the only "Vr=NaN" in vector field are been replaced and
    #                     that there are not a single "Vr=NaN" out of the "to replace lists"  
    counter=0
    for i in range(N_vectors):
        if vr_clean[i] == NaN: counter+=1
    if counter != len(cruzables_to_replace)+len(non_cruzables_to_replace):
        print "ERROR: number of vectors with 'Vr=NaN' in 'clean_data' is not equal to number of vectors in 'to_replace' lists"
        print "       Number of zeros in clean data:", counter
        print "       Number of vectors to replace:", len(cruzables_to_replace)+len(non_cruzables_to_replace)
        exit()
    #** Security routine:
    lista_auxiliar = cruzables_to_replace+non_cruzables_to_replace
    for index in lista_auxiliar:
        if vr_clean[index] != NaN:
            print "ERROR: Non 'Vr=NaN' vector in 'to_replace' lists --> there are vectors with 'Vr=NaN' out of the 'to_replace' lists"
            print "       index, vr_clean[index]:", index, vr_clean[index]
            exit()
    #**** ****#
    #exit()

    #**** replace loop! ****#
    is_this_frame_already_added_as_an_M12_lt_7 = "NO"
    for vector_index in cruzables_to_replace:
        i = vector_index
        #print i, block_data_clean[i][:], vr_clean[i]   # debug line
        cruz12=[(i+1),(i+2),(i-1),(i-2),(i-1*81),(i-2*81),(i+1*81),(i+2*81),(i-1*81+1),(i-1*81-1),(i+1*81+1),(i+1*81-1)]
        M12 = 0
        sum_vx12 = 0. ; mean_vx12 = 0. 
        sum_vy12 = 0. ; mean_vy12 = 0.
        for cross_index in cruz12:
            if vr_clean[cross_index] != NaN:
                sum_vx12 += block_data_clean[cross_index][2]
                sum_vy12 += block_data_clean[cross_index][3]
                M12 += 1
        if M12 < 7:
            if is_this_frame_already_added_as_an_M12_lt_7 == "NO":
                list_of_frames_with_M12_cross_lt_7.append(frame_number)
                is_this_frame_already_added_as_an_M12_lt_7 = "YES"
            print "M12 < 7--> this frame should not be used (but this will be user choice though)", vector_index, M12
            if M12 == 0:
                print "deu merda: M12 == 0 ---> codigo stoped"
                #exit()
                #mean_vx12 = "NaN" ; mean_vy12 = "NaN"
                mean_vx12 = 0. ; mean_vy12 = 0.
            else:
                mean_vx12 = sum_vx12/M12 
                mean_vy12 = sum_vy12/M12
        else:
            mean_vx12 = sum_vx12/M12 ; mean_vy12 = sum_vy12/M12
            
        block_data_clean[i][2], block_data_clean[i][3], block_data_clean[i][4] = mean_vx12, mean_vy12, M12
        vr_clean[i] = np.sqrt( mean_vx12*mean_vx12 + mean_vy12*mean_vy12 )        
        #print i, block_data_clean[i][:], vr_clean[i]   # debug line
    #** log statement:    
    stp = "Number of vectors replaced: %i               (only the cruzables ones are replaced)\n"%(len(cruzables_to_replace))
    stp += "Vectors replaced (index): \n  ["
    for ele in cruzables_to_replace[:len(cruzables_to_replace)-1]: stp += str(ele) + ", "
    if len(cruzables_to_replace) == 0:
        stp += str(cruzables_to_replace) + "]\n"         
    else:
        stp += str(cruzables_to_replace[len(cruzables_to_replace)-1]) + "]\n" 
    log_file.write(stp)
    log_file.write("\n")
    #** log statement:
    stp = "Remaining outliers vectors (index):    (these ones belong to the \"border\" and will be eliminating in the \"border cut\" procedure)\n["
    for ele in non_cruzables_to_replace[:len(non_cruzables_to_replace)-1]: stp += str(ele) + ", "
    if len(non_cruzables_to_replace) == 0:
        stp += str(non_cruzables_to_replace) + "]\n"                 
    else:
        stp += str(non_cruzables_to_replace[len(non_cruzables_to_replace)-1]) + "]\n" 
    log_file.write(stp)
    log_file.write("\n")
    #**** ****#
    #exit()

    #***********************************************************************************#
    #**** END : (F) ********************************************************************#
    #***********************************************************************************#
    #exit()
    #print

    #*******************************************************************#
    #**** (G) - Exporting Borders and Outliers Removal Steps images ****#
    #*******************************************************************#

    if images_ON_or_OFF == "ON":
        #**** defining plot function intent in variables ****#
        title = in_piv_file_name
        out_image_C = bors_C_image_out_path_and_name    
        out_image_8 = bors_C1_image_out_path_and_name    
        out_image_9 = bors_C2_image_out_path_and_name    
        #**** ****#
        #exit()
        
        #**** making and exporting the images ****#
        plot_quiver(N_vectors,block_data_clean,vr_clean,NaN,'RdPu',120,out_image_C,title,[],[],[],cruzables_to_replace,non_cruzables_to_replace,[],[],'')

        plot_quiver(N_vectors,block_data_clean,vr_clean,NaN,'RdPu',120,out_image_8,title,[],[],[],cruzables_to_replace,non_cruzables_to_replace,[],[],'')
        plot_quiver(N_vectors,block_data_clean,vr_clean,NaN,'RdPu',120,out_image_9,title,non_cruzable_index,[],[],cruzables_to_replace,non_cruzables_to_replace,[],[],'')
        #**** ****#
        #exit()
    
    #******************************************************************#
    #**** END : (G) ***************************************************#
    #******************************************************************#
    #exit()
    #print

    
    #*************************************************************************************************#
    #**** (H) - Making the final "Field" data and making/calculating the "fluctuation field" data ****#
    #*************************************************************************************************#
    # the "border cut" happens in this section   
    #    ATENTION:      updating 'N_vectors' and 'vr' values in this section

    #**** updating the "number of vectors" ****#
    old_N_vectors = N_vectors
    N_vectors = N_vectors - len(non_cruzable_index)
    #print  old_N_vectors, N_vectors   # debug line
    #**** ****#
    #exit()

    #**** initializing/bulding "Vector field", "vr", "replaced vectors list index", "fluctu field" and "fluctu_vr" ****#
    VectorField = [[0. for j in range(5)] for i in range(N_vectors)]
    vr = np.zeros(N_vectors)
    replaced_vectors = []    # the list with index of replaced vectors: is equivalent to "cruzables_to_replace" list, but in for the "final" data
     
    FluctuField = [[0. for j in range(5)] for i in range(N_vectors)]
    Fluctu_vr = np.zeros(N_vectors)
    #**** ****#
    #exit()

    #**** defining the vector field final data AND updating "vr" AND defining the replaced vectors index list ****#
    new_index = -1
    for index in cruzable_index:
        i = index ; new_index += 1
        j = new_index
        VectorField[j][:] = block_data_clean[i][:]
        vr[j] = vr_clean[i]
        if index in cruzables_to_replace:
            replaced_vectors.append(new_index)
            #print i, block_data_clean[i][:], vr_clean[i]   # debug line
            #print j, VectorField[j][:], vr[j]      # debug line
    #** Security routine:
    if len(cruzables_to_replace) != len(replaced_vectors):
        print "ERROR: len(cruzables_to_replace) != len(replaced_vectors)"
        print "       len(cruzables_to_replace), len(replaced_vectors): ", len(cruzables_to_replace), len(replaced_vectors)
        print "code forced to stop"
        exit()
    #** Security routine:
    for index in replaced_vectors:
        M12 = VectorField[index][4]
        if M12 == -1:
            print "ERROR: \"M12 = -1\" for vector in replaced vectors list"
            print "       index, VectorField[index][:], vr[index]: ",index, VectorField[index][:], vr[index]
            print "code forced to stop"
            exit()
    #**** ****#
    #exit()

    #**** making/calculating the fluctu field ****#
    mean_Vx, mean_Vy = 0.0, 0.0
    sum_Vx, sum_Vy = 0.0, 0.0
    for i in range(N_vectors):
        sum_Vx += VectorField[i][2]
        sum_Vy += VectorField[i][3]
    mean_Vx = sum_Vx/N_vectors
    mean_Vy = sum_Vy/N_vectors
    #print mean_Vx    # debug line
    for i in range(N_vectors):
        FluctuField[i][:] = VectorField[i][:]
        #print VectorField[i][:] ; print FluctuField[i][:]   # debug line
        FluctuField[i][2] = VectorField[i][2] - mean_Vx
        FluctuField[i][3] = VectorField[i][3] - mean_Vy
        #print VectorField[i][:] ; print FluctuField[i][:]   # debug line
        Fluctu_vr[i] = np.sqrt(FluctuField[i][2]*FluctuField[i][2] + FluctuField[i][3]*FluctuField[i][3])
    #**** ****#
    #exit()
        
    #*************************************************************************************************#
    #**** END : (H) **********************************************************************************#
    #*************************************************************************************************#
    #exit()
    #print

    #*****************************************************************************#
    #**** (I) - Exporting BORS final images, "Field" image and "Fluctu" image ****#
    #*****************************************************************************#

    #**** defining plot function intent in variables ****#
    title = in_piv_file_name
    out_image_D = bors_D_image_out_path_and_name
    out_image_10 = bors_C3_image_out_path_and_name
    
    title_1 = file_out_1_name
    title_2 = file_out_2_name
    out_image_field = field_image_out_path_and_name
    out_image_fluctu = fluctu_image_out_path_and_name
    #**** ****#
    #exit()

    if images_ON_or_OFF == "ON":        
        #**** making and exporting the images ****#
        plot_quiver(N_vectors,VectorField,vr,NaN, 'RdPu',120,out_image_D,title,[],[],[],[],[],[],[],'')
        
        plot_quiver(N_vectors,VectorField,vr,NaN, 'RdPu',120,out_image_10,title,[],[],[],[],[],[],[],'')
        
        plot_quiver(N_vectors,VectorField,vr,NaN, 'plasma',50,out_image_field,title_1,[],[],[],[],[],[],[],'')
        plot_quiver(N_vectors,FluctuField,Fluctu_vr,NaN, 'plasma',30,out_image_fluctu,title_2,[],[],[],[],[],[],[],'')
        #**** ****#
        #exit()
    
    #****************************************************************************#
    #**** END : (I) *************************************************************#
    #****************************************************************************#
    #exit()
    #print

    #*****************************************************************#
    #**** (J) - Writing/Saving "Field.vff" and "Fluctu.vff" files ****#
    #*****************************************************************#

    #**** writing final vector field data to out file ****#
    file_out_1 = open(file_out_1_path_and_name,"w")
    file_out_1.write("# X[px]   Y[px]   Vx[um/h]   Vy[um/h]   Vr[um/h]=sqrt(Vx^2+Vy^2)   M[#]=quality: -1=original, 12=max quality, 1=worst (accepted) quality, 0=worst quality: V-->0 \n")
    for i in range(N_vectors):
        file_out_1.write("%f %f %f %f %f %i \n" % (VectorField[i][0], VectorField[i][1], VectorField[i][2]*0.645, VectorField[i][3]*0.645, vr[i]*0.645, VectorField[i][4]))
    file_out_1.write("#END")
    file_out_1.close()
    #**** ****#
    #exit()

    #**** writing fluctuation vector field data to out file ****#
    file_out_2 = open(file_out_2_path_and_name,"w")
    file_out_2.write("# V* = fluctu -> V* = V - <V>\n")
    file_out_2.write("# X[px]   Y[px]   Vx*[um/h]   Vy*[um/h]   Vr*[um/h]=sqrt(Vx^2+Vy^2)   M[#]=quality: -1=original, 12=max quality, 1=worst (accepted) quality, 0=worst quality: V-->0 \n")
    for i in range(N_vectors):
        file_out_2.write("%f %f %f %f %f %i \n"%(FluctuField[i][0],FluctuField[i][1],FluctuField[i][2]*0.645,FluctuField[i][3]*0.645,Fluctu_vr[i]*0.645,FluctuField[i][4]))
    file_out_2.write("#END")
    file_out_2.close()
    #**** ****#
    #exit()

    #***************************************************************************#
    #**** END : (J) ************************************************************#
    #***************************************************************************#
    #exit()
    #print

    #****************************************************#
    #**** (K) - Making/saving Frames To Merge images ****#
    #****************************************************#

    #**** defining plot function intent in variables ****#
    out_image_11 = ftm_fiY_image_out_path_and_name        
    out_image_12 = ftm_fiL_image_out_path_and_name        
    out_image_13 = ftm_fiM_image_out_path_and_name        
    
    out_image_14 = ftm_fuY_image_out_path_and_name        
    out_image_15 = ftm_fuL_image_out_path_and_name        
    out_image_16 = ftm_fuM_image_out_path_and_name        
    #**** ****#
    #exit()
        
    if images_ON_or_OFF == "ON":    
        #**** making and exporting the field images ****#
        plot_quiver(N_vectors,VectorField,vr,NaN, 'yellow',50,out_image_11,[],[],[],[],[],[],[],[],'ON')
        plot_quiver(N_vectors,VectorField,vr,NaN, 'lime',50,out_image_12,[],[],[],[],[],[],[],[],'ON')
        plot_quiver(N_vectors,VectorField,vr,NaN, 'magenta',50,out_image_13,[],[],[],[],[],[],[],[],'ON')
        #**** ****#
        #exit()
        
        #**** making and exporting the fluctu images ****#
        plot_quiver(N_vectors,FluctuField,Fluctu_vr,NaN, 'yellow',30,out_image_14,[],[],[],[],[],[],[],[],'ON')
        plot_quiver(N_vectors,FluctuField,Fluctu_vr,NaN, 'lime',30,out_image_15,[],[],[],[],[],[],[],[],'ON')
        plot_quiver(N_vectors,FluctuField,Fluctu_vr,NaN, 'magenta',30,out_image_16,[],[],[],[],[],[],[],[],'ON')
        #**** ****#
        #exit()
    
    #***************************************************************************#
    #**** END : (K) ************************************************************#
    #***************************************************************************#
    #exit()
    #print
    
    #*************************************************************************#
    #**** (L) - saving/making Quality final information, files and images ****#
    #*************************************************************************#

    #**** defining some important section variables ****#
    N_replaced_vectors = len(replaced_vectors)
    N_discarted_vectors = len(non_cruzable_index)
    int_frame_number = int(frame_number)
    #**** ****#
    #exit()

    #**** making M12 "quality graphs" data ****#
    M12_index = [0]*(N_replaced_vectors)
    M12_value = [0]*(N_replaced_vectors)
    counter = 0
    for index in range(N_vectors):
        M12 = int(VectorField[index][4])
        if M12 != -1:
            counter += 1
            M12_index[counter-1] = index
            M12_value[counter-1] = M12            
            #print M12, counter, index  # debug line
    #print counter, N_replaced_vectors # debug line
    #print M12_index ; print M12_value  # debug line
    #** security routine:
    if counter != N_replaced_vectors:
        print " error:  M12 number problem"
        print "         Expected number of M12 = ", N_replaced_vectors
        print "         counted number of M12 =", counter
        exit()
    #**** ****#
    #exit()
    
    #**** making M13 "quality graphs" data ****#
    M13_index = [0]*(N13)
    M13_value = [0]*(N13)
    new_index = -1
    counter = 0
    for M13 in M13_after_FEL:
        if M13 != "non_cruzable":
            if M13 == "NaN":
                #print counter, new_index, M13   # debug line
                new_index += 1
            else:
                new_index += 1
                counter += 1
                M13_index[counter-1] = new_index
                M13_value[counter-1] = M13
    #print counter, new_index    # debug line
    #**** ****#
    #exit()

    #**** apending quality data to list to plot ****#
    list_M12_index_and_value.append([int_frame_number,M12_index,M12_value])
    list_M13_index_and_value.append([int_frame_number,M13_index,M13_value])
    #print list_M12_index_and_value[0]   # debug line
    #print list_M13_index_and_value[0]   # debug line
    #**** ****#
    #exit()
    
    #**** calculating the mean M12 quality (and stdv M12) of the replaced vectors ****# 
    Nr = N_replaced_vectors
    sum_M12 = 0 ; sum_M12_2 = 0
    mean_M12 = 0. ; stdv_M12 = 0.
    #print cruzables_to_replace   # debug line
    if Nr < 2:
        if Nr == 0:
            mean_M12 = float("NaN") ; stdv_M12 = float("NaN")
            #print Nr, mean_M12, stdv_M12  # debug line
        else:
            if Nr != 1:
                print "deu merda no Nr"
                exit()
            else:
                for index in replaced_vectors:
                    M12 = VectorField[index][4]
                    if M12 == -1:
                        print "deu merda no M12 (1)"
                        print "index, M12 :", index, M12
                        exit()
                    else:
                        sum_M12 += float(M12)
                        #print Nr, sum_M12, sum_M12_2  # debug line
                        mean_M12 = sum_M12 ; stdv_M12 = 0.
    else:
        for index in replaced_vectors:
            #print VectorField[index][:]    # debug line
            M12 = VectorField[index][4]
            if M12 == -1:
                print "deu merda no M12 (2)"
                print "index, M12 :", index, M12
                exit()
            else:
                sum_M12 += float(M12)
                sum_M12_2 += float(M12)*float(M12)
        #print Nr, sum_M12, sum_M12_2  # debug line
        mean_M12 = sum_M12/Nr ;  mean_M12_2 = sum_M12_2/Nr
        stdv_M12 = np.sqrt( (Nr/(Nr-1)) * (mean_M12_2 - mean_M12*mean_M12) )
        #print Nr, mean_M12, stdv_M12  # debug line
    #**** ****#
    #exit()

    #**** apending quality block data for plot ****#
    quali_list = [int_frame_number, Nr/float(N_vectors)*100, Nr/float(old_N_vectors)*100, mean_M12, stdv_M12, mean_M13, stdv_M13]
    Quality_block_data.append(quali_list)
    #**** ****#
    #exit()
    
    #**** writing quality info to "file out 4" ****#
    stp = "%i %2.2f %2.2f %2.2f %2.2f %2.2f %2.2f\n" % (quali_list[0], quali_list[1], quali_list[2], quali_list[3], quali_list[4], quali_list[5], quali_list[6])
    tmp_file_out_4.write(stp)
    #**** ****#
    #exit()
    
    #** log statement:
    stp = "*************** Sum up:\n"
    stp += "Initial number of vectors : %i\n" % (old_N_vectors) 
    stp += "Number of discarted vectors = borders vectors = len(non_cruzable_index) = %i\n" % (N_discarted_vectors)
    stp += "Outliers percentage (over final number of vectors): %2.2f%s\n" % (len(cruzables_to_replace)/float(N_vectors)*100,"%")
    stp += "\n"
    stp += "Mean M13 (for the cruzable vectors) = %f +- %f  # M13=13 is equal to max quality\n" % (mean_M13,stdv_M13)
    stp += "Mean M12 (for the replaced vectors) = %f +- %f  # M12=12 is equal to max quality\n" % (mean_M12,stdv_M12)
    stp += "\n"
    stp += "Final number of vectors: %i      of wich %i have been replaced with a mean_M12 of %2.2f +- %2.2f\n" % (N_vectors,len(cruzables_to_replace),mean_M12,stdv_M12)
    log_file.write(stp)
    log_file.write("\n")
    #*************************************************************************#
    #**** END : (L) **********************************************************#
    #*************************************************************************#
    #exit()
    #print    

    #*************************************************************************************#
    #**** (M) - End of lap stuf: reseting variables, closing files, and progres check ****#
    #*************************************************************************************#
    
    #**** closing the log file ****#
    log_file.close()
    #**** ****#
    #exit()

    #**** reseting variables ****#
    final_N_vectors = N_vectors
    N_vectors = old_N_vectors
    #**** ****#
    #exit()
    
    #**** progress statement ****#
    print "lap %i/%i end    (frame number: %s)" % (frame_index+1,N_frames,frame_number)
    #**** ****#
    #exit()

    #*************************************************************************************#
    #**** END : (M) **********************************************************************#
    #*************************************************************************************#
    #exit()
    #print

### ******************************************************* ###
###      *********************************************      ### 
###                          END                            ###
###                    Super Section (1)                    ###
###      *********************************************      ###
### ******************************************************* ###
#exit()
print
print

#*******************************************************#
#**** Section 1: After "Super section" riquerements ****#
#*******************************************************#
# Closing "out of the loop" file, moving and renaming 
    
#**** closing quality file and 
tmp_file_out_4.close()
#**** ****#
#exit()

#**** moving/giving the real name to quality file ****#
string_to_rename_and_move_quality_file = "mv %s %s" % (tmp_file_out_4_name,file_out_4_path_and_name)
os.system(string_to_rename_and_move_quality_file)
#**** ****#
#exit()

#*****************************************************#
#**** END : Section 1 ********************************#
#*****************************************************#
print
#exit()

#*****************************************************************#
#**** Section 2: making M13<8 and M12<7 percentage quali file ****#
#*****************************************************************#

#**** opening file ****#
file_out_6_path_and_name = local_path_to_quali+"quality-"+name_label_quality_file+"-ltM12M13%.vff"
#print file_out_6_path_and_name      # debug line
file_out_6 = open(file_out_6_path_and_name,"w")
#**** ****#
#exit()

#**** making necessary lists to print on file ****#
OR_list = []
for ele in list_of_frames_with_M12_cross_lt_7:
    OR_list.append(ele)
for ele in list_of_frames_with_M13_cross_lt_8:
    if ele not in list_of_frames_with_M12_cross_lt_7:
        OR_list.append(ele)
OR_list.sort()
#print OR_list   # debug line
AND_list = []
for ele in list_of_frames_with_M12_cross_lt_7:
    AND_list.append(ele)
for ele in list_of_frames_with_M12_cross_lt_7:
    if ele not in list_of_frames_with_M13_cross_lt_8:
        AND_list.remove(ele)
AND_list.sort()
#print AND_list   # debug line
#**** ****#
#exit()

#**** writing file data ****#
stp = "# A = Percentage of frames with M13<8 OR M12<7\n"
stp += "# B = Percentage of frames with M13<8 AND M12<7\n"
stp += "# C = Percentage of frames with M13<8\n"
stp += "# D = Percentage of frames with M12<7\n"
stp += "#\n"
stp += "# total number of frames: %i\n"%(N_frames)
stp += "# %i frames A : [ " % (len(OR_list))
for ele in OR_list:
    stp += "%s "%(ele)
stp += "]\n"
stp += "# %i frames B : [ " % (len(AND_list))
for ele in AND_list:
    stp += "%s "%(ele)
stp += "]\n"
stp += "# %i frames C : [ " % (len(list_of_frames_with_M13_cross_lt_8))
for ele in list_of_frames_with_M13_cross_lt_8:
    stp += "%s "%(ele)
stp += "]\n"
stp += "# %i frames D : [ " % (len(list_of_frames_with_M12_cross_lt_7))
for ele in list_of_frames_with_M12_cross_lt_7:
    stp += "%s "%(ele)
stp += "]\n"
stp += "#\n"
stp += "# A[%s] B[%s] C[%s] D[%s]\n" % ("%","%","%","%")
stp += "%2.2f %2.2f %2.2f %2.2f\n" % ((len(OR_list)/float(N_frames))*100,(len(AND_list)/float(N_frames))*100,(len(list_of_frames_with_M12_cross_lt_7)/float(N_frames))*100,(len(list_of_frames_with_M13_cross_lt_8)/float(N_frames))*100)
#print stp    # debug line
#**** ****#
#exit()

#**** saving data to file and closing file ****#
file_out_6.write(stp)
file_out_6.close()
#**** ****#
#exit()

#*****************************************************#
#**** END : Section 2 ********************************#
#*****************************************************#
#print
#exit()

#*******************************************#
#**** Section 3: Ploting quality graphs ****#
#*******************************************#
# This section should probably be a separated independent file (in the feature)

#**** Defining X,Y for plot 1 and plot 2****#
#print len(Quality_block_data)   # debug line
x = np.zeros(N_frames) ; y1 = np.zeros(N_frames) ; y2 = np.zeros(N_frames)
y3 = np.zeros(N_frames) ; stdv_y3 = np.zeros(N_frames)
y4 = np.zeros(N_frames) ; stdv_y4 = np.zeros(N_frames)
counter = 0
for ele in Quality_block_data:
    i = counter
    x[i] = ele[0] ; y1[i] = ele[1] ; y2[i] = ele[2] ; y3[i] = ele[3] ; stdv_y3[i] = ele[4] ; y4[i] = ele[5] ; stdv_y4[i] = ele[6]
    counter += 1
#**** ****#
#exit()
    
#**** plot 1: outliers percentage plot ****#
fig, ax = plt.subplots()
ax.plot(x,y2,"bo",label="over all %i vectors"%(N_vectors))

ax.plot(x,y1,"ro",label="over the remaning %i vectors"%(final_N_vectors))

plt.ylabel("Outliers vectors percentage [%]")
plt.xlabel("Frame number [#]")
plt.title("%s"%(file_out_4_name))
plt.legend()
plt.xlim(0,N_frames+10) ; plt.ylim(0,10)
plt.yticks((0,1.00,2.00,3.00,4.00,5.00,6.00,7.00,8.00,9.00,10.00))
#plt.show()
plt.savefig("%s%s-outliers.png"%(local_path_to_quali,file_out_4_name.replace(".vff","")))
plt.close()
#**** ****#
#exit()

#**** plot 2: mean M values (M13 and M12) for each frame ****#
fig, ax = plt.subplots()
ax.errorbar(x, y3, stdv_y3, fmt="bo",ecolor="blue",capsize=2,ms=2,label="<M12>")
ax.errorbar(x, y4, stdv_y4, fmt="ro",ecolor="red",capsize=2,ms=2,label="<M13>")
plt.ylabel("<M> [number of neighbors]")
plt.xlabel("Frame number [#]")
plt.title("%s\n \"M12 = 12\" and \"M13=13\" is the max quality"%(file_out_4_name))
plt.legend()
plt.xlim(0,N_frames+10) ; plt.ylim(0,15)
plt.yticks((0,1,2,3,4,5,6,7,8,9,10,11,12,13,14))
#plt.show()
plt.savefig("%s%s-M.png"%(local_path_to_quali,file_out_4_name.replace(".vff","")))
plt.close()
#**** ****#
#exit()

#**** plot 3: mean M12 value for the replaced vectors ****#
fig, ax = plt.subplots()
ax.errorbar(x, y3, stdv_y3, fmt="bo",ecolor="red",capsize=2,ms=2,label="<M12> = Mean quality value for the replaced vectors")
plt.ylabel("<M12> [number of neighbors]")
plt.xlabel("Frame number [#]")
plt.title("%s\n \"M12 = 12\" is the max quality"%(file_out_4_name))
plt.legend()
plt.xlim(0,N_frames+10) ; plt.ylim(0,15)
#plt.show()
plt.savefig("%s%s-M12.png"%(local_path_to_quali,file_out_4_name.replace(".vff","")))
plt.close()
#**** ****#
#exit()

#**** plot 4: mean M13 value for all cruzables (not nan) vectors ****#
fig, ax = plt.subplots()
ax.errorbar(x, y4, stdv_y4, fmt="bo",ecolor="red",capsize=2,ms=2,label="<M13> = Mean quality value for the cruzables vectors")
plt.ylabel("<M13> [number of neighbors]")
plt.xlabel("Frame number [#]")
plt.title("%s\n \"M13 = 13\" is the max quality"%(file_out_4_name))
plt.legend()
plt.xlim(0,N_frames+10) ; plt.ylim(0,15)
plt.yticks((0,2,4,6,8,10,12,13,14))
#plt.show()
plt.savefig("%s%s-M13.png"%(local_path_to_quali,file_out_4_name.replace(".vff","")))
plt.close()
#**** ****#
#exit()

#**** plot 5: M12 values for each replaced vectors ****#
import matplotlib.cm as cm
fig, ax = plt.subplots()
#plt.figure()
ax.plot((-150,5150),(7,7),"y--")
for ele in list_M12_index_and_value[0:1]:
    Q = ax.plot(ele[1],ele[2],marker="+",linewidth=0.5,color=cm.seismic(ele[0]),label="M12 = quality value for each replaced vector")
for ele in list_M12_index_and_value[1:]:
    Q = ax.plot(ele[1],ele[2],marker="+",linewidth=0.5,color=cm.seismic(ele[0]))
    #plt.plot(ele[1],ele[2],marker="+",linewidth=0.5,color=cm.seismic(ele[0]))
    #Q = plt.scatter(ele[1],ele[2],marker="+",linewidth=0.5,color=cm.seismic(ele[0]),cmap='viridis')
ax.set_xlim([-100,5000]) ; ax.set_ylim([0,15])
plt.yticks((0,2,4,6,7,8,10,12,14))
ax.set_title("%s\n \"M12 = 12\" is the max quality"%(file_out_4_name))
ax.set_xlabel('Vector index [ad.]')
ax.set_ylabel('M12 [number of neighbors]')
plt.legend()
# fake up the array of the scalar mappable. urgh...
sm = plt.cm.ScalarMappable(cmap='seismic', norm=plt.Normalize(vmin=0, vmax=N_frames+50))
sm._A = [] ; cbar = plt.colorbar(sm) ; #cbar = plt.colorbar(Q,clim(0,120)) ;
cbar.set_label('Frame Number [#]') #plt.clim(0,198) ; plt.colorbar(label='Vr [$\mu$m/h]')
#plt.show()
plt.savefig("%s%s-vectorsM12.png"%(local_path_to_quali,file_out_4_name.replace(".vff","")))
plt.close()
#**** ****#
#exit()

#**** plot 6: M13 values for each cruzable (valid) vector ****#
import matplotlib.cm as cm
fig, ax = plt.subplots()
#plt.figure()
ax.plot((-150,5150),(8,8),"y--")
for ele in list_M13_index_and_value[0:1]:
    Q = ax.plot(ele[1],ele[2],marker="+",linewidth=0.5,color=cm.seismic(ele[0]),label="M13 = quality value for each cruzable vector")
for ele in list_M13_index_and_value[1:]:
    Q = ax.plot(ele[1],ele[2],marker="+",linewidth=0.5,color=cm.seismic(ele[0]))
    #plt.plot(ele[1],ele[2],marker="+",linewidth=0.5,color=cm.seismic(ele[0]))
    #Q = plt.scatter(ele[1],ele[2],marker="+",linewidth=0.5,color=cm.seismic(ele[0]),cmap='viridis')
ax.set_xlim([-100,5000]) ; ax.set_ylim([0,15])
plt.yticks((0,2,4,6,8,10,12,13,14))
ax.set_title("%s\n \"M13 = 13\" is the max quality"%(file_out_4_name.replace(".vff","-M13.vff")))
ax.set_xlabel('Vector index [ad.]')
ax.set_ylabel('M13 [number of neighbors]')
plt.legend()
# fake up the array of the scalar mappable. urgh...
sm = plt.cm.ScalarMappable(cmap='seismic', norm=plt.Normalize(vmin=0, vmax=N_frames+50))
sm._A = [] ; cbar = plt.colorbar(sm) ; #cbar = plt.colorbar(Q,clim(0,120)) ;
cbar.set_label('Frame Number [#]') #plt.clim(0,198) ; plt.colorbar(label='Vr [$\mu$m/h]')
#plt.show()
plt.savefig("%s%s-vectorsM13.png"%(local_path_to_quali,file_out_4_name.replace(".vff","")))
plt.close()
#**** ****#
#exit()

#*******************************************#
#**** END : Section 3 **********************#
#*******************************************#
#exit()
#print

#************************************************************************************************************#
#**** 4 - saving the section out information necessary for the rest of pipeline in a "file.vff" ****#
#************************************************************************************************************#
# pipeline will read this file and load the information for teh rest of the sections
file_out_path_and_name = "%sout-info-%s-%s-piv%s.vff" % (local_path_to_dsoi,d0,hyst_or_norm,extension)
#print file_out_path_and_name    # debug line
string_to_remove_vff_out_info_file = "rm %s" % (file_out_path_and_name) ; os.system(string_to_remove_vff_out_info_file)
file_out = open(file_out_path_and_name,"w")
file_out.write("%s\n"%(local_path_to_field))
file_out.write("%s\n"%(local_path_to_fluctu))
file_out.write("%s\n"%(local_path_to_ftm))
#file_out.write("%s\n"%(local_path_to_quali))
file_out.write("%s\n"%(N_frames))
file_out.write("%i\n"%(N_vectors))
file_out.write("%i\n"%(final_N_vectors))
stp = file_out_1_path_and_name.replace(local_path_to_field,"").replace(frame_number,"????")
file_out.write("%s %i\n"%(stp,len(stp)))
stp = file_out_2_path_and_name.replace(local_path_to_fluctu,"").replace(frame_number,"????")
file_out.write("%s %i\n"%(stp,len(stp)))
file_out.write("%s %i\n"%(file_out_1_path_and_name.replace(frame_number,"????"),len(file_out_1_path_and_name)))
file_out.write("%s %i\n"%(file_out_2_path_and_name.replace(frame_number,"????"),len(file_out_2_path_and_name)))
file_out.write("%s\n"%(out_image_11))
file_out.write("%s\n"%(out_image_12))
file_out.write("%s\n"%(out_image_13))
file_out.write("%s\n"%(out_image_14))
file_out.write("%s\n"%(out_image_15))
file_out.write("%s\n"%(out_image_16))
file_out.write("%s\n"%(out_image_field))
file_out.write("%s\n"%(out_image_fluctu))
file_out.close()
string_to_make_vff_out_info_file_read_only = "chmod 444 %s" % (file_out_path_and_name)
os.system(string_to_make_vff_out_info_file_read_only)
#***************************************************************************************************#
#** END 4 ******************************************************************************************#
#***************************************************************************************************#
#exit()
print


if non_interactive_mode == "OFF":
        #**** making/opening the "log.piv" file ****#
        input_string = raw_input("Make log file (yes or no): ")
        if input_string == "yes":
                file_out_path_and_name = "%slog-%s-%s-piv%s.vff" % (local_path_to_log,d0,hyst_or_norm,extension)
                #print file_out_path_and_name  # debug line
                file_out = open(file_out_path_and_name,"w")
                file_out.close()
                string_to_open_log_file = "emacs %s" % (file_out_path_and_name)
                os.system(string_to_open_log_file)
        #**** ****#
        #exit()
        print

print "********** END VFF.py *************"
exit()
