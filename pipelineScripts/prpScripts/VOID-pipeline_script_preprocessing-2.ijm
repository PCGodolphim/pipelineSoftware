run("Bio-Formats", "open=[<PATH_AND_FILE_NAME>] autoscale color_mode=Default rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT");
run("Make Substack...", "delete slices=<SLICE_i>-<SLICE_f>");
selectWindow("<FILE_NAME>");
close();
selectWindow("Substack (<SLICE_i>-<SLICE_f>)");
run("Image Sequence... ", "format=TIFF name=[] start=1 save=<PATH_TO_SAVE>0001.tif");
run("Quit");
