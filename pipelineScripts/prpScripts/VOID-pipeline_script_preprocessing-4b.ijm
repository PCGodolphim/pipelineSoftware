selectWindow("Hysteresis_Thresholding");
run("Image Sequence... ", "format=TIFF name=<LINAGE>-hyst use start=1 save=<PATH_TO_HYST><LINAGE>-hyst0001.tif");

selectWindow("Hysteresis_Thresholding");
run("Image Sequence... ", "format=JPEG name=<LINAGE>-hyst use start=1 save=<PATH_TO_MOVIES_AND_IMAGES>jpg/<LINAGE>-hyst0001.jpg");

selectWindow("Hysteresis_Thresholding");
run("AVI... ", "compression=JPEG frame=7 save=<PATH_TO_MOVIES_AND_IMAGES><LINAGE>-hyst.avi");
run("Quit");
