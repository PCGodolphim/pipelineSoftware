run("Image Sequence...", "open=<PATH_TO_NORM><LINAGE>-norm0001.tif number=<NUMBER_OF_STACKS> file=<LINAGE>-norm sort");

selectWindow("norm");
run("AVI... ", "compression=JPEG frame=7 save=<PATH_TO_MOVIES_AND_IMAGES><LINAGE>-norm.avi");

selectWindow("norm");
run("Image Sequence... ", "format=JPEG name=[] use start=1 save=<PATH_TO_MOVIES_AND_IMAGES>jpg/<LINAGE>-norm0001.jpg");

selectWindow("norm");
run("Duplicate...", "duplicate");

selectWindow("norm-1");
setColor(255, 255, 0);
for (i=1; i<=nSlices; i++) {
    setSlice(i);
    run("RGB Color");
    //run("Duplicate...", "title=Slice");
    // do some processing
	drawLine(694, 1, 694, 1040);
	drawLine(693, 1, 693, 1040);
	drawLine(1, 520, 1388, 520);
	drawLine(1, 519, 1388, 519);
}

selectWindow("norm-1");
saveAs("Tiff", "<PATH_TO_MANUAL_COUNTING><LINAGE>-norm_count.tif");
close();

run("Histogram", "slice");
//selectWindow("Histogram of norm");
run("Image Sequence...", "open=<PATH_TO_TMP_CUT>0001.tif sort");
run("Histogram", "slice");

