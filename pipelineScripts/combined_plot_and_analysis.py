import os
import sys
import numpy as np
import subprocess as subp
import matplotlib.pyplot as plt

print("\n >>> running CPA.py\n")

#********************************************#
#**** Section (-1): functions deinitions ****#
#********************************************#

#**** get color linage function****#
def get_color_linage(d0):
    color_linage = ""
    if "hacat" in d0:
        if d0 == "hacat-m03" or d0 == "hacat-m05":
            color_linage = "navy"
        elif d0 == "hacat-m04":
            color_linage = "cornflowerblue"
        else:
            color_linage = "blue"
    elif "cal27" in d0:
        color_linage = "darkorange"
    elif "scc9" in d0:
        color_linage = "red"
    elif "fibroblastos" in d0:
        color_linage = "black"
    else:
        print("unsuported linage-movie type: deu merda no fitted activity graph --> code stoped")
        #print(d0)  # debug line
        exit()
    return color_linage
#**** ****#
#exit()
            
#********************************************#
#**** END : Section (-1) ********************#
#********************************************#
#exit()
print

#*********************************************************************************************#
#**** Section 0: defining in variables and defining dictionary fot input data files names ****#
#*********************************************************************************************#

#**** initialazing necessary variables and paths ****#
hyst_or_norm = "hyst"
extension = "tif"
label = "<d0>-%s-piv%s-vff-" % (hyst_or_norm,extension)
local_path_to_adv = "<d0>/Calculations/adv/"
local_path_to_vaf = "<d0>/Calculations/vaf/"
#print(local_path_to_adv+"\n"+local_path_to_vaf+"\n"+label)  # debug line
#**** ****#
#exit()

#**** making CPA directory (combined plot analysis) ****#
#dcpa = "CPA" ; os.system("mkdir -p %s"%(dcpa))
#dcpa = "CPA_3.1" ; os.system("mkdir -p %s"%(dcpa))
dcpa = "CPA_3.2_EOSBF2020_disertaçãocorrigida" ; os.system("mkdir -p %s"%(dcpa))
local_path_to_cpa = "%s/" % (dcpa)
#local_path_to_cpa = ""   # debug line
#print(local_path_to_cpa)  # debug line
#**** ****#
#exit()

#**** defining files sufixes (that is actually the true names of the files) ****#
sfx1 = "activity.adv"
sfx2 = "drift_speed.adv"
sfx3 = "vrms.adv"
sfx4 = "fitted-activity.adv"
sfxA = "swirls_sizes.vaf"
sfxB = "velocity_correlation_length.vaf"
sfxC = "total_degree_of_correlation.vaf"
sfx_list = [sfx1,sfx2,sfx3,sfx4,sfxA,sfxB,sfxC]
#for sfx in sfx_list: print("sfx[%i]  =  %s" % (sfx_list.index(sfx),sfx))   # debug line
#**** ****#
#exit()

#**** making label varibales for d0s ****#
hac1 = "hacat-m01"
hac2 = "hacat-m02"
hac3 = "hacat-m03"
hac4 = "hacat-m04"
hac5 = "hacat-m05"
cal1 = "cal27-m01"
cal2 = "cal27-m02"
cal3 = "cal27-m03"
scc1 = "scc9-m01"
scc2 = "scc9-m02"
scc3 = "scc9-m03"
fib1 = "fibroblastos-m01"
#**** ****#
#exit()

#**** making files labels for dictionary ****#
f1 = "file1"
f2 = "file2"
f3 = "file3"
f4 = "file4"
f5 = "file5"
f6 = "file6"
f7 = "file7"
#**** ****#
#exit()

#**** making the files <void> names ****#
file1 = local_path_to_adv+label+sfx1
file2 = local_path_to_adv+label+sfx2
file3 = local_path_to_adv+label+sfx3
file4 = local_path_to_adv+label+sfx4
file5 = local_path_to_vaf+label+sfxA
file6 = local_path_to_vaf+label+sfxB
file7 = local_path_to_vaf+label+sfxC
#**** ****#
#exit()

#**** defining "d0's" list ****#
d0_list = []
d0_list.append("fibroblastos-m01")  # d0[0]
d0_list.append("hacat-m01")  # d0[1]
d0_list.append("cal27-m01")  # d0[2]
d0_list.append("scc9-m01")  # d0[3]
d0_list.append("hacat-m02")  # d0[4]
d0_list.append("cal27-m02")  # d0[5]
d0_list.append("scc9-m02")  # d0[6]
d0_list.append("cal27-m03")  # d0[7]
d0_list.append("scc9-m03")  # d0[8]
d0_list.append("hacat-m03")  # d0[9]
d0_list.append("hacat-m05")  # d0[10]
d0_list.append("hacat-m04")  # d0[11]
#for d0 in d0_list: print("d0[%i]  =  %s" % (d0_list.index(d0),d0))   # debug line
#**** ****#
#exit()

#**** making the linages-movies anf files (paths and names) dictionary ****#
files = {}
for d0 in d0_list:
    files[d0] = {
        f1: file1.replace("<d0>",d0),
        f2: file2.replace("<d0>",d0),
        f3: file3.replace("<d0>",d0),
        f4: file4.replace("<d0>",d0),
        f5: file5.replace("<d0>",d0),
        f6: file6.replace("<d0>",d0),
        f7: file7.replace("<d0>",d0),
    }
#print(files)  ; print() # debug line
#print(files[hac1]) ; print(); print(files[hac1][f1]) ; print() # debug line
#print(files[fib1]) ; print(); print(files[fib1][f4]) ; print() # debug line
#**** ****#
#exit()

#**** defining CPA images prefix label ****#
prefix_label = "cpa-"
#**** ****#
#exit()

#**** defining riqueired dictionaries labels ****#
time = "time"
speed = "speed"
act = "activity"
fluctu = "fluctuations_in_activity"
xfit = "x_points_of_the_fitted_function"
yfit = "y_points_of_the_fitted_function"
tau = "Tau_activity"
drop_point = "Time_where_activity_start_to_be_fitted"
fitA = "fit_value_a"
fitB = "fit_value_b"
fitf = "fit_function"
N = "Number_of_time_points"
fitx2 = "swirl_length_via_x2_fit"
locmin = "swirl_length_via_local_minima"
vcl = "velocity_correlation_length"
chi = "total_degree_of_correlation"
xi = "distace_of_zero_Cvv"
nchi = "normalized_total_degree_of_correlation"
meannchi = "mean normalized total degree of correlation"
stdvmeannchi = "stdv of mean normalized total degree of correlation"
#**** ****#
#exit()

#*********************************************************************************************#
#**** END : Section 0 ************************************************************************#
#*********************************************************************************************#
#exit()
#print

#*************************************************************#
#**** Section (0.1) : initializing ADV files dictionaries ****#
#*************************************************************#

#**** making the activity dictionary (for all movies) ****#
activity = {}
for d0 in d0_list:
    activity[d0] = {
        time: {},
        speed: {},
        fluctu: {},
    }
#print(activity, len(activity))  # debug line
#print(hac1, activity[hac1], activity[hac1][speed]) # debug line
#print(fib1, activity[fib1], activity[fib1][time]) # debug line
#**** ****#
#exit()

#**** making the vrms dictionary (for all movies) ****#
vrms = {}
for d0 in d0_list:
    vrms[d0] = {
        time: {},
        speed: {},
    }
#print(vrms, len(vrms))  # debug line
#print(hac1, vrms[hac1], vrms[hac1][speed]) # debug line
#print(fib1, vrms[fib1], vrms[fib1][time] # debug line
#**** ****#
#exit()

#**** making the drift dictionary (for all movies) ****#
drift = {}
for d0 in d0_list:
    drift[d0] = {
        time: {},
        speed: {},
}
#print(drift, len(drift)  # debug line
#print(hac1, drift[hac1], drift[hac1][speed] # debug line
#print(fib1, drift[fib1], drift[fib1][time] # debug line
#**** ****#
#exit()

#**** making the fitted_act dictionary (for all movies) ****#
fitted_act = {}
for d0 in d0_list:
    fitted_act[d0] = {
        time: {},
        act: {},
        xfit: {},
        yfit: {},
        tau: {},
        drop_point: {},
        fitA: {},
        fitB: {},
        fitf: "f(x) = A*exp(-B*x)",
        N: {},
    }
#print(fitted_act, len(fitted_act)  # debug line
#print(hac1, fitted_act[hac1], fitted_act[hac1][fitf] # debug line
#print(fib1, fitted_act[fib1], fitted_act[fib1][time] # debug line
#**** ****#
#exit()

#*************************************************************#
#**** END : Section (0.1) ************************************#
#*************************************************************#
#exit()
#print

#*************************************************************#
#**** Section (0.2) : initializing VAF files dictionaries ****#
#*************************************************************#

#**** making the swirl length dictionary (for all movies) ****#
swirl = {}
for d0 in d0_list:
    swirl[d0] = {
        time: {},
        fitx2: {},  # "swirl_length_via_x2_fit"
        locmin: {}, # "swirl_length_via_local_minima"
    }
#print(swirl, len(swirl)  # debug line
#print(hac1, swirl[hac1], swirl[hac1][fitx2] # debug line
#print(fib1, swirl[fib1], swirl[fib1][time] # debug line
#**** ****#
#exit()

#**** making the velocity correlation length dictionary (for all movies) ****#
correlength = {}
for d0 in d0_list:
    correlength[d0] = {
        time: {},
        vcl: {},  # "velocity_correlation_length"
    }
#print(correlength, len(correlength)  # debug line
#print(hac1, correlength[hac1], correlength[hac1][fitx2] # debug line
#print(fib1, correlength[fib1], correlength[fib1][time] # debug line
#**** ****#
#exit()

#**** making the velocity correlation length dictionary (for all movies) ****#
corredegree = {}
for d0 in d0_list:
    corredegree[d0] = {
        time: {},
        chi: {},  # "total_degree_of_correlation"
        xi: {},   # "distace_of_zero_Cvv"
        nchi: {},  # "normalized_total_degree_of_correlation"
        meannchi: {},  # "mean normalized total degree of correlation"
        stdvmeannchi: {},  # "stdv of mean normalized total degree of correlation"
    }
#print(corredegree, len(corredegree)  # debug line
#print(hac1, corredegree[hac1], corredegree[hac1][fitx2] # debug line
#print(fib1, corredegree[fib1], corredegree[fib1][time] # debug line
#**** ****#
#exit()

#*************************************************************#
#**** END : Section (0.2) ************************************#
#*************************************************************#
#exit()
#print


#******************************************************************#
#**** Section 1 : making the "end of pipeline" graph (diagram) ****#
#******************************************************************#
if 2 == 2:
    #**** geting the "nChi" and its stdv (for all movies) ****#
    nChi_list = []
    start_linage, end_linage = 1, 9   # default (2d only)
    #start_linage, end_linage = 0, 9  # with-fibroblasts (1d)
    #start_linage, end_linage = 1, 11 # with-hacat-3d
    #start_linage, end_linage = 0, 11 # with-all-except-hacm04
    #start_linage, end_linage = 0, 12 # with-all (hacat 1d to)
    for d0 in d0_list[start_linage:end_linage]:
        in_file_name = files[d0][f7] 
        #print(in_file_name) # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data) ; exit() # debug line
        data = data.split("\n")
        #print(d0, data[4])   # debug line
        line_data = data[4].split(" = ") ; line_data.remove(line_data[0]) ; line_data = line_data[0].split(" $\\pm$ ")
        #print(line_data)   # debug line
        nChi = line_data[0] ; stdv_nChi = line_data[1]
        #print(nChi, stdv_nChi)  # debug line
        nChi_list.append([d0,nChi,stdv_nChi])
    #for ele in nChi_list: print(ele)   # debug line
    #**** ****#
    #exit()

    #**** geting the "Tau_act" (for all movies) ****#
    Tau_act_list = []
    for d0 in d0_list[start_linage:end_linage]:
        in_file_name = files[d0][f4] 
        #print(in_file_name # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data ; exit() # debug line
        data = data.split("\n")
        #print(d0, data[8]   # debug line
        line_data = data[8].split(") = ") ; line_data.remove(line_data[0]) ; line_data = line_data[0].replace("h","")
        #print(line_data   # debug line
        Tau_act = line_data
        #print(Tau_act  # debug line
        Tau_act_list.append([d0,Tau_act])
    #for ele in Tau_act_list: print(ele   # debug line
    #**** ****#
    #exit()

    #**** defining the plot variables for the "end of pipeline" diagram (drop-drop diagram) ****#
    plt.rcParams.update({'font.size': 15})
    x = np.zeros(len(nChi_list))  ;  y = np.zeros(len(nChi_list))
    color_linage = []  ;  size = np.zeros(len(nChi_list))
    for i in range(len(nChi_list)):
        if nChi_list[i][0] != Tau_act_list[i][0]:
            print("deu merda no droop droop graph --> code stoped")
            #print(nChi_list[i][0], Tau_act_list[i][0]   # debug line
            exit()
        d0 = nChi_list[i][0]
        x[i] = float(Tau_act_list[i][1])
        y[i] = float(nChi_list[i][1])
        if "hacat" in d0:
            if d0 == "hacat-m03" or d0 == "hacat-m05":
                color_linage.append("navy")
            elif d0 == "hacat-m04":
                color_linage.append("cornflowerblue")
            else:
                color_linage.append("blue")
        elif "cal27" in d0:
            color_linage.append("orange")
        elif "scc9" in d0:
            color_linage.append("red")
        elif "fibroblastos" in d0:
            color_linage.append("black")
        else:
            print("unsuported linage-movie type: deu merda no droop droop graph --> code stoped")
            #print(d0  # debug line
            exit()        
        size[i] = float(nChi_list[i][2])
        #**** ****#
        #exit()

    #**** defining image names prefix label ****#
    drop_prefix_label = ""
    if "cornflowerblue" in color_linage:
        drop_prefix_label += "with-all-"
    elif "black" in color_linage and "navy" in color_linage:
        drop_prefix_label += "with-all-execpt-hacm04-"
    elif "black" in color_linage:
        drop_prefix_label += "with-fibroblastos-"
    elif "navy" in color_linage:
        drop_prefix_label += "with-hacm03-m05-"
    else:
        drop_prefix_label += ""
    #**** ****#
    #exit()

    #**** 0 ploting drop-drop diagram ****#
    plt.rcParams.update({'font.size': 22})
    fig, ax = plt.subplots(figsize=(1.4*5.8,1.4*5))

    #x = [0,2,4,6,8,10,12,14,16,18]
    #s_exp = [20*2**n for n in range(len(x))]
    #s_square = [20*n**2 for n in range(len(x))]
    #s_linear = [20*n for n in range(len(x))]
    #plt.scatter(x,[1]*len(x),s=s_exp, label='$s=2^n$', lw=1)
    #plt.scatter(x,[0]*len(x),s=s_square, label='$s=n^2$')
    #plt.scatter(x,[-1]*len(x),s=s_linear, label='$s=n$')
    #plt.ylim(-1.5,1.5)
    #plt.legend(loc='center left', bbox_to_anchor=(1.1, 0.5), labelspacing=3)
    #plt.show()
    #exit()

    #ax.scatter(x, y, color=color_linage, s=18000*size, alpha=0.5)
    #print(size
    #exit()

    #plt.rcParams['font.sans-serif'] = "Arial"
    #plt.rcParams['font.family'] = "sans-serif"
    
    #points for the three lines
    #x_1=[0.00,1.00,2.00,3.00,4.00,5.00,6.00,6.50]
    #y_1=[3.00,2.80,2.40,2.20,1.80,1.00,0.20,0.00]
    
    #x_2 = [0.00,1.00,2.00,2.80]
    #y_2 = [7.00,8.00,9.00,10.00]
    
    #x_3=[2.80,2.80,3.00,4.00]
    #y_3=[10.00,6.00,4.00,1.80]
    
    #create comparison arrays for fill_between
    #y_1_compare=[]
    #for item in x_1:
    #    y_1_compare.append(0)
        
    #y_2_compare=[]
    #for item in x_2:
    #    y_2_compare.append(10)
        
    #f=plt.figure(figsize=(5,5))
    #ax=plt.gca()
    #plt.plot(x_1,y_1,'o',linestyle='-', color='k',linewidth=0.7)
    #plt.plot(x_2,y_2,'s',linestyle='-',color='k',linewidth=0.7)
    #plt.plot(x_3,y_3,'^',linestyle='--',color='k',linewidth=0.7)
    #ax.set_xlim(0,8)
    #ax.set_ylim(0,10)
    #ax.fill_between(x_1, y_1, y_1_compare, where=y_1 >= y_1_compare, facecolor='tab:cyan', interpolate=True,alpha=0.6)
    #ax.fill_between(x_2, y_2, y_2_compare, where=y_2 <= y_2_compare, facecolor='green', interpolate=True,alpha=0.4)
    #ax.tick_params(labelsize=15)
    #plt.show()
    #exit()
    
    scala=20
    ax.set_xlabel(r"$\tau_{act}$ [ h ]", fontsize=25)
    ax.set_ylabel(r"$\eta$", fontsize=25)
    ax.set_title(r"$\tau$-$\eta$ diagram")
    ax.scatter((0.0),(0.0),color="blue",alpha=0.5,s=scala*40,label="$s")
    if "navy" in color_linage:
        ax.scatter((0.0),(0.0),color="navy",label="healthy 3d",alpha=0.5)
    if "cornflowerblue" in color_linage:
        ax.scatter((0.0),(0.0),color="cornflowerblue",alpha=0.5)
    ax.scatter((0.0),(0.0),color="orange",alpha=0.5,s=scala*80,label="$s")
    ax.scatter((0.0),(0.0),color="red",alpha=0.5)
    if "black" in color_linage:
        ax.set_ylim(0.225,0.57)
        ax.scatter((0.0),(0.0),color="black",label="healthy fibroblast 1d",alpha=0.5)
    else:
        ax.set_ylim(0.225,0.49)
        #ax.set_ylim(0.275,0.445)
    if "cornflowerblue" in color_linage:
        ax.set_xlim(-2,183)
        #ax.set_ylim(0.225,0.60)
    else:
        ax.set_xlim(5,60)
    if "cornflowerblue" in color_linage:
        ax.legend(loc='best', bbox_to_anchor=(0.38, 0.5, 0.5, 0.5))
    else:
        print("ola")
        #continue
        #ax.legend(loc="upper right")
    ax.grid(True)
    fig.tight_layout()
    #plt.show()
    name_out_image = "%s%s%sdrop-drop-diagram-diagnostic-model.png" % (local_path_to_cpa,prefix_label,drop_prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** 0.1 ploting drop-drop diagram for delta eta as Y ****#
    #fig, ax = plt.subplots(figsize=(5.8,5))
    fig, ax = plt.subplots()
    ax.scatter(x, size, color=color_linage, s=850, alpha=0.5)
    ax.set_xlabel(r"$\tau_{act}$ [ h ]", fontsize=17)
    ax.set_ylabel(r"$\delta \eta$", fontsize=17)
    ax.set_title("Drop-Drop Diagram")
    #ax.scatter((0.0),(0.0),color="blue",label="healthy 2d",alpha=0.5)
    ax.scatter((0.0),(0.0),color="blue",label="healthy",alpha=0.5)
    if "navy" in color_linage:
        ax.scatter((0.0),(0.0),color="navy",label="healthy 3d",alpha=0.5)
    if "cornflowerblue" in color_linage:
        ax.scatter((0.0),(0.0),color="cornflowerblue",label="healthy 1d",alpha=0.5)
    #ax.scatter((0.0),(0.0),color="orange",label="benign 2d",alpha=0.5)
    ax.scatter((0.0),(0.0),color="orange",label="benign",alpha=0.5)
    #ax.scatter((0.0),(0.0),color="red",label="malignant 2d",alpha=0.5)
    ax.scatter((0.0),(0.0),color="red",label="malignant",alpha=0.5)
    if "black" in color_linage:
        ax.set_ylim(0.225,0.57)
        ax.scatter((0.0),(0.0),color="black",label="healthy fibroblast 1d",alpha=0.5)
    else:
        #print("ola"
        #ax.set_ylim(0.225,0.49)
        ax.set_ylim(0.04,0.10)
        #ax.set_ylim(0.275,0.445)
    if "cornflowerblue" in color_linage:
        ax.set_xlim(-2,183)
        #ax.set_ylim(0.225,0.60)
    else:
        ax.set_xlim(5,60)
    if "cornflowerblue" in color_linage:
        ax.legend(loc='best', bbox_to_anchor=(0.38, 0.5, 0.5, 0.5))
    else:
        ax.legend(loc="upper right")
    ax.grid(True)
    fig.tight_layout()
    #plt.show()
    #name_out_image = "%s%s%sdrop-drop-diagram-delta-eta-Y.png" % (local_path_to_cpa,prefix_label,drop_prefix_label)
    #plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()
    
    #**** 1 ploting drop-drop diagram ****#
    plt.rcParams.update({'font.size': 18})
    fig, ax = plt.subplots(figsize=(5.8,5))
    ax.scatter(x, y, color=color_linage,s=200, alpha=0.5)
    ax.set_xlabel(r"$\tau_{act}$ [ h ]", fontsize=20)
    ax.set_ylabel(r"$\eta$", fontsize=20)
    ax.set_title(r"$\tau$-$\eta$ diagram")
    #ax.scatter((0.0),(0.0),color="blue",label="healthy",alpha=0.5)
    ax.scatter((0.0),(0.0),color="blue",label="Hacat",alpha=0.5)
    if "navy" in color_linage:
        ax.scatter((0.0),(0.0),color="navy",label="healthy 3d",alpha=0.5)
    if "cornflowerblue" in color_linage:
        ax.scatter((0.0),(0.0),color="cornflowerblue",label="healthy 1d",alpha=0.5)
    #ax.scatter((0.0),(0.0),color="orange",label="benign",alpha=0.5)
    ax.scatter((0.0),(0.0),color="orange",label="Cal27",alpha=0.5)
    #ax.scatter((0.0),(0.0),color="red",label="malignant",alpha=0.5)
    ax.scatter((0.0),(0.0),color="red",label="SCC9",alpha=0.5)
    if "black" in color_linage:
        ax.set_ylim(0.225,0.57)
        ax.scatter((0.0),(0.0),color="black",label="healthy fibroblast 1d",alpha=0.5)
    else:
        ax.set_ylim(0.225,0.49)
        #ax.set_ylim(0.275,0.445)
    if "cornflowerblue" in color_linage:
        ax.set_xlim(-2,183)
        #ax.set_ylim(0.225,0.60)
    else:
        ax.set_xlim(5,60)
    if "cornflowerblue" in color_linage:
        ax.legend(loc='best', bbox_to_anchor=(0.38, 0.5, 0.5, 0.5))
    else:
        ax.legend(loc="upper right")
    ax.set_xticks((10,20,30,40,50,60))
    ax.grid(True)
    fig.tight_layout()
    #plt.show()
    name_out_image = "%s%s%sdrop-drop-diagram-cpa.png" % (local_path_to_cpa,prefix_label,drop_prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** 2 ploting drop-drop diagram ****#
    fig, ax = plt.subplots(figsize=(5.8,5))
    xh = [13.001, 16.642] ; yh = [0.386, 0.410] ; eyh = [0.048, 0.065]
    xh35 = [14.333, 8.048] ; yh35 = [0.335, 0.331] ; eyh35 =[0.084, 0.064] 
    xh4 = [170.923] ; yh4 = [0.367] ; eyh4 =[0.061] 
    xc = [22.530, 11.677, 10.058] ; yc = [0.295, 0.322, 0.336] ; eyc = [0.060, 0.074, 0.087]
    xs = [52.622, 44.309, 34.781] ; ys = [0.329, 0.323, 0.361] ; eys = [0.059, 0.072, 0.075]
    xf = [10.849] ; yf = [0.477] ; eyf = [0.085]
    ax.errorbar(xh, yh, eyh, fmt=",", color="blue", ecolor="blue",capsize=2,ms=2, alpha=0.5)
    if "navy" in color_linage:
        ax.errorbar(xh35, yh35, eyh35, fmt=",", color="navy", ecolor="navy",capsize=2,ms=2, alpha=0.5)
    if "cornflowerblue" in color_linage:
        ax.errorbar(xh4, yh4, eyh4, fmt=",", color="cornflowerblue", ecolor="cornflowerblue",capsize=2,ms=2, alpha=0.5)
    ax.errorbar(xc, yc, eyc, fmt=",", color="orange", ecolor="orange",capsize=2,ms=2)
    ax.errorbar(xs, ys, eys, fmt=",", color="red", ecolor="red",capsize=2,ms=2)
    if "black" in color_linage:
        ax.errorbar(xf, yf, eyf, fmt=",", color="black", ecolor="black",capsize=2,ms=2)
    ax.scatter(x, y, color=color_linage, s=850, alpha=0.5)
    ax.set_xlabel(r"$\tau_{act}$ [ h ]", fontsize=17)
    ax.set_ylabel(r"$\eta$", fontsize=17)
    ax.set_title("Drop-Drop Diagram")
    ax.scatter((0.0),(0.0),color="blue",label="healthy",alpha=0.5)
    if "navy" in color_linage:
        ax.scatter((0.0),(0.0),color="navy",label="healthy 3d",alpha=0.5)
    if "cornflowerblue" in color_linage:
        ax.scatter((0.0),(0.0),color="cornflowerblue",label="healthy 1d",alpha=0.5)
    ax.scatter((0.0),(0.0),color="orange",label="benign",alpha=0.5)
    ax.scatter((0.0),(0.0),color="red",label="malignant",alpha=0.5)
    if "black" in color_linage:
        ax.set_ylim(0.225,0.57)
        ax.scatter((0.0),(0.0),color="black",label="healthy fibroblast 1d",alpha=0.5)
    else:
        ax.set_ylim(0.225,0.49)
    if "cornflowerblue" in color_linage:
        ax.set_xlim(-2,183)
        #ax.set_ylim(0.225,0.60)
    else:
        ax.set_xlim(5,60)
    if "cornflowerblue" in color_linage:
        ax.legend(loc='upper right', bbox_to_anchor=(0.38, 0.5, 0.5, 0.5))
    else:
        ax.legend(loc="upper right")
    ax.grid(True)
    fig.tight_layout()
    #plt.show()
    name_out_image = "%s%s%sdrop-drop-diagram-errorbars-cpa.png" % (local_path_to_cpa,prefix_label,drop_prefix_label)
    #plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** 3 ploting drop-drop diagram ****#
    plt.rcParams.update({'font.size': 18})
    fig, ax = plt.subplots(figsize=(7.1,5))
    ax.scatter(x, y, color=color_linage, s=18000*size, alpha=0.5)
    ax.set_xlabel(r"$\tau_{act}$ [ h ]", fontsize=20)
    ax.set_ylabel(r"$\eta$", fontsize=20)
    ax.set_title(r"$\tau$-$\eta$ diagram")
    ax.text(67,0.48,r"$\delta\eta$")
    ax.text(70,0.43,"0.09")
    ax.text(70,0.36,"0.07")
    ax.text(70,0.29,"0.05")
    #ax.scatter((55,55,55), (0.29,0.36,0.43), c=('grey','grey','grey'), s=(18000*0.05,18000*0.07,18000*0.09), alpha=0.8)
    ax.scatter((0.0),(0.0),color="blue",label="Hacat",alpha=0.5)
    if "navy" in color_linage:
        ax.scatter((0.0),(0.0),color="navy",label="healthy 3d",alpha=0.5)
    if "cornflowerblue" in color_linage:
        ax.scatter((0.0),(0.0),color="cornflowerblue",label="healthy 1d",alpha=0.5)
    ax.scatter((0.0),(0.0),color="orange",label="Cal27",alpha=0.5)
    ax.scatter((0.0),(0.0),color="red",label="SCC9",alpha=0.5)
    if "black" in color_linage:
        ax.set_ylim(0.225,0.57)
        ax.scatter((0.0),(0.0),color="black",label="healthy fibroblast 1d",alpha=0.5)
    else:
        ax.set_ylim(0.225,0.49)
        #ax.set_ylim(0.275,0.445)
    if "cornflowerblue" in color_linage:
        ax.set_xlim(-2,183)
        #ax.set_ylim(0.225,0.60)
    else:
        ax.set_xlim(5,60)
    if "cornflowerblue" in color_linage:
        ax.legend(loc='best', bbox_to_anchor=(0.38, 0.5, 0.5, 0.5))
    else:
        ax.legend(loc="upper right")
    ax.set_xticks((10,20,30,40,50,60))
    ax.grid(True)
    fig.tight_layout()
    #plt.show()
    name_out_image = "%s%s%sdrop-drop-diagram-sizes_as_errorbars-cpa.png" % (local_path_to_cpa,prefix_label,drop_prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

#******************************************************************#
#**** END : Section 1 *********************************************#
#******************************************************************#
exit()
#print

#**************************************************************#
#**** Section 2 : making the combined activity fited graph ****#
#**************************************************************#
if 2 == 2:

    #**** defining linages to be used ****#
    start_linage, end_linage = 0, 12 # with-all (hacat 1d to)
    #**** ****#
    #exit()
    
    #**** defining the activity dictionary: geting the activity data (for all movies) ****#
    for d0 in d0_list[start_linage:end_linage]:  # loop over linages-movies
        #*** loading the activity data ***#
        in_file_name = files[d0][f1] 
        #print(in_file_name # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data ; exit() # debug line
        data = data.split("\n")
        #for ele in data[:4]: print(d0, data.index(ele), ele # debug line
        #*** ***#
        #exit()
        
        #*** cleaning the data, check if it has the right len ***#
        for i in range(4):
            data.remove(data[0])
        data.remove("#END")
        #if len(data) != N_points:
        #    print("deu merda no numero de points da activity --->  code stoped!"
        #    print(len(data)  # debug line        
        #    exit()
        #for ele in data: print(ele   # debug line
        for i in range(len(data)):
            data[i] = data[i].split(" ")
        #for ele in data: print(ele  # debug line
        #*** ***#
        #exit()
        
        #*** saving file data to target arrays ***#
        npt = len(data)
        time_array = np.zeros(npt) ; act_array = np.zeros(npt) ; fluctu_array = np.zeros(npt)
        for i in range(npt):
            time_array[i] = float(data[i][0])
            act_array[i] = float(data[i][1])
            fluctu_array[i] = float(data[i][2])
        #for i in range(npt): print(i, time_array[i], act_array[i], fluctu_array[i]  # debug line
        #*** ***#
        #exit()

        #*** updating linage-movie dictionary plot arrays ***#
        activity[d0][time] = time_array
        activity[d0][speed] = act_array
        activity[d0][fluctu] = fluctu_array
        #print(activity[d0][time], type(activity[d0][time])  # debug line
        #*** ***#
        #exit()        
    #**** ****#
    #exit()
        
    #**** defining the fitted_act dictionary: geting the fitted activity data (for all movies) ****#
    for d0 in d0_list[start_linage:end_linage]:  # loop over linages-movies
        #*** loading the data ***#
        in_file_name = files[d0][f4] 
        #print(in_file_name) # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data) # debug line
        data = data.split("\n")
        #for ele in data[:11]: print(d0, data.index(ele), ele) # debug line
        #*** ***#
        #exit()
        
        #*** geting the header information ***#
        a = float(data[6].replace("# A = ","").replace("[um/h]",""))
        b = float(data[7].replace("# B = ",""))
        tau_act = float(data[8].replace("# tau_activity = (1/B) = ","").replace("h",""))
        N_points_in_fit_region = int(data[9].replace("# N = number_of_points_in_fit_region = ",""))
        print(d0, a, b, tau_act, N_points_in_fit_region) # debug line
        #*** ***#
        #exit()

        #*** cleaning the data, check if it has the right len ***#
        for i in range(11):
            data.remove(data[0])
        data.remove("#END")
        if len(data) != N_points_in_fit_region:
            print("deu merda no numero de points na fit region da activity --->  code stoped!")
            print(len(data))  # debug line        
            exit()
        #for ele in data: print(ele   # debug line
        for i in range(len(data)):
            data[i] = data[i].split(" ")
        #for ele in data: print(ele  # debug line
        #*** ***#
        #exit()

        #*** saving header info to linage-movie dictionary ***#
        fitted_act[d0][fitA] = a
        fitted_act[d0][fitB] = b
        fitted_act[d0][tau] = tau_act
        fitted_act[d0][N] = N_points_in_fit_region
        print(d0, fitted_act[d0][fitA], fitted_act[d0][fitB], fitted_act[d0][tau], fitted_act[d0][N])   # debug line
        #*** ***#
        #exit()
        
        #*** saving file data to target arrays ***#
        npt = fitted_act[d0][N]
        time_array = np.zeros(npt) ; act_array = np.zeros(npt)
        for i in range(npt):
            time_array[i] = float(data[i][0])
            act_array[i] = float(data[i][1])      
        #for i in range(npt): print(i, time_array[i], act_array[i]  # debug line
        xp_fit_exp = np.linspace(time_array[0]-5,time_array[npt-1])
        yp_fit_exp = a*np.exp(-b*xp_fit_exp)
        #for i in range(len(xp_fit_exp)): print( xp_fit_exp[i], yp_fit_exp[i] # debug line
        #*** ***#
        #exit()
        
        #*** updating linage-movie dictionary plot arrays ***#
        fitted_act[d0][time] = time_array
        fitted_act[d0][act] = act_array
        fitted_act[d0][xfit] = xp_fit_exp
        fitted_act[d0][yfit] = yp_fit_exp
        fitted_act[d0][drop_point] = time_array[0]
        #print(fitted_act[d0][time], type(fitted_act[d0][time]))  # debug line
        #print(fitted_act[d0][drop_point]) # debug line
        #*** ***#
        #exit()
    #**** ****#
    #exit()

    #**** ploting the activity graph ****#
    plt.rcParams.update({'font.size': 20})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x0 = activity[d0][time]
        y0 = activity[d0][speed]
        x = fitted_act[d0][time]
        y = fitted_act[d0][act]
        xf = fitted_act[d0][xfit]
        yf = fitted_act[d0][yfit]
        a = fitted_act[d0][fitA]
        b = fitted_act[d0][fitB]
        tau_activity = fitted_act[d0][tau]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),"g:",lw=2.5,label="full curve")
            axis.plot((-20,-10),(-10,-10),"g-",lw=2.5,label="fitted region")
            axis.plot((-20,-10),(-10,-10),color="orange",ls="--",lw=2.5,label="f(x)=A*exp(-B*x)")
            axis.plot((-20,-10),(-10,-10),"w,",label=r"$\tau_{act}=1/B$")
        else:
            #axis.plot(x0,y0,"g:",lw=2.5)#,label="full curve")
            #axis.plot(x,y,"g-",lw=2.5)#,label="fitted region")
            #axis.plot(xf,yf,color="orange",ls="--",lw=2.5)#,label="f(x)=%2.2f*exp(-%2.3fx)"%(a,b))
            axis.plot(x0,y0,"g:",lw=3.5)#,label="full curve")
            axis.plot(x,y,"g-",lw=3.5)#,label="fitted region")
            axis.plot(xf,yf,color="orange",ls="--",lw=3.5)#,label="f(x)=%2.2f*exp(-%2.3fx)"%(a,b))
            #axis.text(30.7,25.,d0.split("-")[1].replace("m","movie "),fontsize=14,color=color_linage,weight="bold")
            axis.text(26.7,25.,d0.split("-")[1].replace("m","movie "),fontsize=14,color=color_linage,weight="bold")
            if color_linage == "blue":
                axis.text(1.0,2.2,r"$\tau_{act}\approx$%2.0fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
            else:
                #axis.text(31.5,21.,r"$\tau_{act}\approx$%2.0fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
                axis.text(26.0,20.5,r"$\tau_{act}\approx$%2.0fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
        axis.set_ylim(0,28)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,7,14,21))
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"activity [ $\mu m/h$ ]",fontsize=23)
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]",fontsize=23)
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=14,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sfitted_activity.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()
    
    #if d0 == "fibroblastos-m01":
    #    plt.text(54.0,17.3,r"$\tau_{act}$ = %2.3fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
    #else:
    #    plt.text(34.0,17.3,r"$\tau_{act}$ = %2.3fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
    #plt.legend(fontsize=15)
    
#**************************************************************#
#**** END : Section 2 *****************************************#
#**************************************************************#
#exit()
#print

#****************************************************************************#
#**** Section 3 : making the combined speeds graph (and the drift graph) ****#
#****************************************************************************#
if 2 == 2:

    #**** defining linages to be used ****#
    start_linage, end_linage = 0, 12 # with-all (hacat 1d to)
    #**** ****#
    #exit()
    
    #**** defining the vrms dictionary: geting the vrms data (for all movies) ****#
    for d0 in d0_list[start_linage:end_linage]:  # loop over linages-movies
        #*** loading the activity data ***#
        in_file_name = files[d0][f3] 
        #print(in_file_name # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data ; exit() # debug line
        data = data.split("\n")
        #for ele in data[:4]: print(d0, data.index(ele), ele # debug line
        #*** ***#
        #exit()
        
        #*** cleaning the data, check if it has the right len ***#
        for i in range(4):
            data.remove(data[0])
        data.remove("#END")
        #if len(data) != N_points:
        #    print("deu merda no numero de points da activity --->  code stoped!"
        #    print(len(data)  # debug line        
        #    exit()
        #for ele in data: print(ele   # debug line
        for i in range(len(data)):
            data[i] = data[i].split(" ")
        #for ele in data: print(ele  # debug line
        #*** ***#
        #exit()
        
        #*** saving file data to target arrays ***#
        npt = len(data)
        time_array = np.zeros(npt) ; vrms_array = np.zeros(npt) 
        for i in range(npt):
            time_array[i] = float(data[i][0])
            vrms_array[i] = float(data[i][1])
        #for i in range(npt): print(i, time_array[i], vrms_array[i]  # debug line
        #*** ***#
        #exit()

        #*** updating linage-movie dictionary plot arrays ***#
        vrms[d0][time] = time_array
        vrms[d0][speed] = vrms_array
        #print(vrms[d0][time], type(vrms[d0][time])  # debug line
        #*** ***#
        #exit()        
    #**** ****#
    #exit()
    
    #**** defining the drift dictionary: geting the drift data (for all movies) ****#
    for d0 in d0_list[start_linage:end_linage]:  # loop over linages-movies
        #*** loading the activity data ***#
        in_file_name = files[d0][f2] 
        #print(in_file_name # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data ; exit() # debug line
        data = data.split("\n")
        #for ele in data[:4]: print(d0, data.index(ele), ele # debug line
        #*** ***#
        #exit()
        
        #*** cleaning the data, check if it has the right len ***#
        for i in range(4):
            data.remove(data[0])
        data.remove("#END")
        #if len(data) != N_points:
        #    print("deu merda no numero de points da activity --->  code stoped!"
        #    print(len(data)  # debug line        
        #    exit()
        #for ele in data: print(ele   # debug line
        for i in range(len(data)):
            data[i] = data[i].split(" ")
        #for ele in data: print(ele  # debug line
        #*** ***#
        #exit()
        
        #*** saving file data to target arrays ***#
        npt = len(data)
        time_array = np.zeros(npt) ; drift_array = np.zeros(npt) 
        for i in range(npt):
            time_array[i] = float(data[i][0])
            drift_array[i] = float(data[i][1])
        #for i in range(npt): print(i, time_array[i], drift_array[i]  # debug line
        #*** ***#
        #exit()

        #*** updating linage-movie dictionary plot arrays ***#
        drift[d0][time] = time_array
        drift[d0][speed] = drift_array
        #print(drift[d0][time], type(drift[d0][time])  # debug line
        #*** ***#
        #exit()        
    #**** ****#
    #exit()
        
    #**** ploting the activity fluctuation/acitivity graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x1 = activity[d0][time]
        #y1 = (-activity[d0][fluctu] + activity[d0][speed])/activity[d0][speed]
        y1 = activity[d0][fluctu]/activity[d0][speed]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="violet",lw=2.5,label=r"$\Gamma$")
            axis.plot((-20,-10),(-10,-10),color="black",ls="--",lw=2.5,label=r"$\overline{\Gamma}$")
            axis.text(2,0.15,r"$\overline{\Gamma}=<\Gamma>_{0.25h\leq t \leq45h}$",fontsize=14,weight="bold")
        else:
            axis.plot(x1,y1,color="violet",lw=1.5)
            axis.text(2.,0.9,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
            if d0 == "cal27-m01":
                y1[129] = (y1[128] + y1[130])*0.5
            CoefVariat = sum(y1[:177])/len(y1[:177])
            axis.plot((0.25,45.0),(CoefVariat,CoefVariat),color="black",lw=2,ls="dashed")
            axis.text(30.0,0.1,r"$\overline{\Gamma}\approx$%2.2f"%(CoefVariat),fontsize=12, bbox=dict(facecolor='grey', alpha=0.3,boxstyle="round"))
            #a"$<\overline{\Gamma}>\approx$%2.2f"%(CoefVariat),fontsize=12, bbox=dict(facecolor='grey', alpha=0.3,boxstyle="round"))
            #print(x1[177],y1[177])
            print("########## ",x1[177])
            print("########## ",len(x1[:177]))
            print("########## ",len(y1[:177]))
            #print("#### ",d0,sum(y1[:177])/len(y1[:177]))
        axis.set_ylim(0,1.0)
        axis.set_xlim(-1.7,52.5)
        #axis.set_xlim(-1.7,52.5)
        #axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,0.2,0.4,0.6,0.8))
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"[ - ]")
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=12,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sactivity-CoefVariat.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()        
    #**** ploting the activity fluctuation/acitivity graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x1 = activity[d0][time]
        y1 = drift[d0][speed]/activity[d0][speed]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="violet",lw=2.5,label=r"$\Gamma$")
            axis.plot((-20,-10),(-10,-10),color="black",ls="--",lw=2.5,label=r"$\overline{\Gamma}$")
            axis.text(2,0.15,r"$\overline{\Gamma}=<\Gamma>_{0.25h\leq t \leq45h}$",fontsize=14,weight="bold")
        else:
            axis.plot(x1,y1,color="violet",lw=1.5)
            axis.text(2.,0.9,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
            if d0 == "cal27-m01":
                y1[129] = (y1[128] + y1[130])*0.5
            CoefVariat = sum(y1[:177])/len(y1[:177])
            axis.plot((0.25,45.0),(CoefVariat,CoefVariat),color="black",lw=2,ls="dashed")
            axis.text(30.0,0.1,r"$\overline{\Gamma}\approx$%2.2f"%(CoefVariat),fontsize=12, bbox=dict(facecolor='grey', alpha=0.3,boxstyle="round"))
            #a"$<\overline{\Gamma}>\approx$%2.2f"%(CoefVariat),fontsize=12, bbox=dict(facecolor='grey', alpha=0.3,boxstyle="round"))
            #print(x1[177],y1[177])
            print("########## ",x1[177])
            print("########## ",len(x1[:177]))
            print("########## ",len(y1[:177]))
            #print("#### ",d0,sum(y1[:177])/len(y1[:177]))
        axis.set_ylim(0,1.0)
        axis.set_xlim(-1.7,52.5)
        #axis.set_xlim(-1.7,52.5)
        #axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,0.2,0.4,0.6,0.8))
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"[ - ]")
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=12,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sdrif-over-activity.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()
    
    #**** ploting the drift speed graph ****#
    plt.rcParams.update({'font.size': 20})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x3 = drift[d0][time]
        y3 = drift[d0][speed]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.text(4,12.,r"$V_{drift} = \left \langle \ \vec{v} \ \right \rangle_{\vec{r}}$",fontsize=22,weight="bold")
        else:
            axis.plot(x3,y3,color="crimson",ls="-",lw=3.5)
            axis.text(26.7,25.,d0.split("-")[1].replace("m","movie "),fontsize=14,color=color_linage,weight="bold")
        axis.set_ylim(0,28)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,7,14,21))
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"$V_{drift}$ [ $\mu m/h$ ]")
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=12,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sdrift.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #if d0 == "fibroblastos-m01":
    #    plt.text(54.0,17.3,r"$\tau_{act}$ = %2.3fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
    #else:
    #    plt.text(34.0,17.3,r"$\tau_{act}$ = %2.3fh"%(tau_activity),fontsize=15, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
    #plt.legend(fontsize=15)
    
#**************************************************************#
#**** END : Section 3 *****************************************#
#**************************************************************#
#exit()
#print

#***************************************************#
#**** Section 4 : making the vaf related graphs ****#
#***************************************************#
if 2 == 2:

    #**** defining linages to be used ****#
    start_linage, end_linage = 0, 12 # with-all (hacat 1d to)
    #**** ****#
    #exit()
    
    #**** defining the total correlation degree dictionary: geting the data (for all movies) ****#
    for d0 in d0_list[start_linage:end_linage]:  # loop over linages-movies
        #*** loading the corredegree data ***#
        in_file_name = files[d0][f7] 
        #print(in_file_name # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data ; exit() # debug line
        data = data.split("\n")
        #for ele in data[:6]: print(d0, data.index(ele), ele # debug line
        #*** ***#
        #exit()

        #**** geting the mean nchi ans its stdv (for all movies) ****#
        line_data = data[4].split(" = ") ; line_data.remove(line_data[0]) ; line_data = line_data[0].split(" $\\pm$ ")
        #print(line_data   # debug line
        nChi = float(line_data[0]) ; stdv_nChi = float(line_data[1])
        #print(nChi, stdv_nChi  # debug line
        corredegree[d0][meannchi] = nChi
        corredegree[d0][stdvmeannchi] = stdv_nChi
        #**** ****#
        #exit()
        
        #*** cleaning the data, check if it has the right len ***#
        for i in range(6):
            data.remove(data[0])
        data.remove("#END")
        #if len(data) != N_points:
        #    print("deu merda no numero de points da totaldegree --->  code stoped!"
        #    print(len(data)  # debug line        
        #    exit()
        #for ele in data: print(ele   # debug line
        for i in range(len(data)):
            data[i] = data[i].split(" ")
        #for ele in data: print(ele  # debug line
        #*** ***#
        #exit()
        
        #*** saving file data to target arrays ***#
        npt = len(data)
        time_array = np.zeros(npt) ; chi_array = np.zeros(npt) ; xi_array = np.zeros(npt) ; nchi_array = np.zeros(npt)
        for i in range(npt):
            time_array[i] = float(data[i][0])
            chi_array[i] = float(data[i][1])
            xi_array[i] = float(data[i][2])
            nchi_array[i] = float(data[i][3])
        #for i in range(npt): print(i, time_array[i], chi_array[i], xi_array[i], nchi_array[i]  # debug line
        #*** ***#
        #exit()

        #*** updating linage-movie dictionary plot arrays ***#
        corredegree[d0][time] = time_array
        corredegree[d0][chi] = chi_array
        corredegree[d0][xi] = xi_array
        corredegree[d0][nchi] = nchi_array
        #print(corredegree[d0][time], type(corredegree[d0][time])  # debug line
        #*** ***#
        #exit()        
    #**** ****#
    #exit()
    
    #**** defining the correlation length dictionary: geting the data (for all movies) ****#
    for d0 in d0_list[start_linage:end_linage]:  # loop over linages-movies
        #*** loading the correlength data ***#
        in_file_name = files[d0][f6] 
        #print(in_file_name # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data ; exit() # debug line
        data = data.split("\n")
        #for ele in data[:3]: print(d0, data.index(ele), ele # debug line
        #*** ***#
        #exit()

        #*** cleaning the data, check if it has the right len ***#
        for i in range(3):
            data.remove(data[0])
        data.remove("#END")
        #if len(data) != N_points:
        #    print("deu merda no numero de points da totaldegree --->  code stoped!"
        #    print(len(data)  # debug line        
        #    exit()
        #for ele in data: print(ele   # debug line
        for i in range(len(data)):
            data[i] = data[i].split(" ")
        #for ele in data: print(ele  # debug line
        #*** ***#
        #exit()
        
        #*** saving file data to target arrays ***#
        npt = len(data)
        time_array = np.zeros(npt) ; vcl_array = np.zeros(npt)
        for i in range(npt):
            time_array[i] = float(data[i][0])
            vcl_array[i] = float(data[i][1])
        #for i in range(npt): print(i, time_array[i], vcl_array[i]  # debug line
        #*** ***#
        #exit()

        #*** updating linage-movie dictionary plot arrays ***#
        correlength[d0][time] = time_array
        correlength[d0][vcl] = vcl_array
        #print(correlength[d0][time], type(correlength[d0][time])  # debug line
        #*** ***#
        #exit()        
    #**** ****#
    #exit()
    
    #**** defining the swirl length dictionary: geting the data (for all movies) ****#
    for d0 in d0_list[start_linage:end_linage]:  # loop over linages-movies
        #*** loading the swirl data ***#
        in_file_name = files[d0][f5] 
        #print(in_file_name # debug line
        in_file = open(in_file_name,"r")
        data = in_file.read()
        #print(data ; exit() # debug line
        data = data.split("\n")
        #for ele in data[:5]: print(d0, data.index(ele), ele # debug line
        #*** ***#
        #exit()

        #*** cleaning the data, check if it has the right len ***#
        for i in range(5):
            data.remove(data[0])
        data.remove("#END")
        #if len(data) != N_points:
        #    print("deu merda no numero de points da totaldegree --->  code stoped!"
        #    print(len(data)  # debug line        
        #    exit()
        #for ele in data: print(ele   # debug line
        for i in range(len(data)):
            data[i] = data[i].split(" ")
        #for ele in data: print(ele  # debug line
        #*** ***#
        #exit()
        
        #*** saving file data to target arrays ***#
        npt = len(data)
        time_array = np.zeros(npt) ; fitx2_array = np.zeros(npt) ; locmin_array = np.zeros(npt)
        for i in range(npt):
            time_array[i] = float(data[i][0])
            fitx2_array[i] = float(data[i][1])
            locmin_array[i] = float(data[i][2])
        #for i in range(npt): print(i, time_array[i], fitx2_array[i], locmin_array[i]  # debug line
        #*** ***#
        #exit()

        #*** updating linage-movie dictionary plot arrays ***#
        swirl[d0][time] = time_array
        swirl[d0][fitx2] = fitx2_array
        swirl[d0][locmin] = locmin_array
        #print(swirl[d0][time], type(swirl[d0][time])  # debug line
        #*** ***#
        #exit()        
    #**** ****#
    #exit()

    print("@************@")
    print("Polar plot routine comented in script here")
    #x = np.linspace(0, 2*np.pi, 400)
    #y = np.sin(x**2)
    #fig, axs = plt.subplots(2, 2, subplot_kw=dict(polar=True))
    #axs[0, 0].plot(x, y)
    #axs[1, 1].scatter(x, y)
    #plt.show()
    print("@************@")
    #exit()
    

    #**** ploting the activity vs total degree (chi) graph ****#
    #from matplotlib.colors import LogNorm  #not in use
    #plt.style.use('ggplot')  #not in use
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex=True, sharey=True,
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    #ax9 = plt.subplot(336, sharey=ax7)
    #ax9.set_yticklabels([])
    
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        print(d0)   #debug line
        x = activity[d0][speed]
        y = corredegree[d0][chi]
        z = activity[d0][time]
        #print(x); print(y); print(z) #debug line
        drop_index = np.where(z == fitted_act[d0][drop_point])
        if len(drop_index) != 1:
            print("erro, dorp_index 'degenerado'")
            exit()
        else:
            drop_index = int(drop_index[0])
        #x = x[drop_index:]
        #y = y[drop_index:]
        #z = z[drop_index:]        
        #print(drop_index,z[drop_index],fitted_act[d0][drop_point])  # debug line
        #vx = np.zeros(len(x)) # quiver stuff not working
        #vy = np.zeros(len(x)) # quiver stuff not working
        #for i in range(len(x)-1): # quiver stuff not working
        #    vx[i] = x[i+1] - x[i] # quiver stuff not working
        #    vy[i] = y[i+1] - y[i] # quiver stuff not working
        #exit()  #debug line/trash
        #nnz = int(len(z)**.5)  #trash
        #z_matrix = z.reshape(nnz, nnz) #trash
        #x2 = correlength[d0][time]
        #y0 = activity[d0][speed]
        #y2 = correlength[d0][vcl]
        color_linage = ""
        color_linage = get_color_linage(d0)
        #if "hacat" in d0:
        #    y1 = y1*np.sqrt(6./7.)
        #    y2 = y2*np.sqrt(6./7.)
        #if "cal27" in d0:
        #    y1 = y1*np.sqrt(6./10.)
        #    y2 = y2*np.sqrt(6./10.)
        if axis == ax7:
            #axis.scatter(x,y,c=z,cmap="jet") #to plot a linage instead
            yt=4
            #axis.plot([3],[125+yt],color="white",marker="$S$",ms=12)
            axis.plot([20],[90],color="white",marker="$D$",ms=12)
            axis.text(18,55,"Drop\npoint",fontsize=10,color="black",weight="bold")
            axis.scatter([3,3,3,3],[100+yt,75+yt,50+yt,25+yt],c=[1,12,24,48],s=55,cmap="jet",vmin=0,vmax=50)
            #axis.text(5,125,"Start: t = %i h"%(z[0]),fontsize=10,color="black",weight="bold")
            axis.text(5,100,"t =  1 h",fontsize=10,color="black",weight="bold")
            axis.text(5,75,"t = 12 h",fontsize=10,color="black",weight="bold")
            axis.text(5,50,"t = 24 h",fontsize=10,color="black",weight="bold")
            axis.text(5,25,"t = 48 h",fontsize=10,color="black",weight="bold")
            axis.patch.set_facecolor('xkcd:salmon')
            axis.patch.set_alpha(0.6)
            #print("ola")  #debug line
            #axis.plot(12,30,"w*",ms=12)  #debug line
            continue
        else:
            #axis.quiver(x,y,vx,vy,z,units='xy' ,scale=1.0,cmap="jet")#,headwidth=3.0,headlength=5.0,headaxislength=4.5) #some other option
            axis.scatter(x[drop_index:],y[drop_index:],c=z[drop_index:],cmap="jet",vmin=0,vmax=50)
            axis.plot(x[drop_index:],y[drop_index:],color="black",lw=1.0)
            axis.text(10,40,"Drop point = %i h"%(z[drop_index]),fontsize=10,color="black",weight="bold")
            #axis.scatter(x,y,c=z,cmap="jet",vmin=0,vmax=50)
            #axis.plot(x,y,color="black",lw=1.0)
            #axis.plot(x[0],y[0],color="white",marker="$S$",ms=12)
            axis.plot(x[drop_index],y[drop_index],color="white",marker="$D$",ms=12)
            if d0 == "hacat-m02":
                axis.text(16.0,23.0,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
            else:
                axis.text(16.0,126.0,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
            axis.patch.set_facecolor('xkcd:salmon')
            axis.patch.set_alpha(0.4)
            #print(d0)   #debug line
            #axis.plot(12,30,"w*",ms=12)   #debug line
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(15,145)
            #print("ola")   #debug line
            #continue
        axis.set_xlim(0,27.0)
        axis.set_xticks((0,6,12,18,24))
        axis.set_yticks((30,60,90,120))
        #print(d0, axis.get_ylim())     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"correlation length [ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            #axis.set_xlabel("velocity [ $\mu m/h$ ]")
            axis.set_xlabel("activity [ $\mu m/h$ ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=17,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.clim(0,50) ; plt.colorbar(label= "time [h]")  #trash?
    #plt.show()
    prefix_label += ""
    #name_out_image = "%s%sact-chi-2.png" % (local_path_to_cpa,prefix_label)
    name_out_image = "%s%sact-chi-20h.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    print("onde termina? Procure por: @aqui@")
    exit()    
    
    #**** segundo plot desses ploting the activity vs total degree (chi) graph ****#
    #from matplotlib.colors import LogNorm  #not in use
    #plt.style.use('ggplot')  #not in use
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            #um = 0 ; index = 11
            um = 0 ; index = 0
        d0 = d0_list[index]
        print(d0)   #debug line
        x = activity[d0][speed]
        y = corredegree[d0][chi]
        z = activity[d0][time]
        #x = x[40:]
        #y = y[40:]
        #z = z[40:]        
        vx = np.zeros(len(x))
        vy = np.zeros(len(x))
        for i in range(len(x)-1):
            vx[i] = x[i+1] - x[i]
            vy[i] = y[i+1] - y[i]
        #exit()  #debug line/trash
        #nnz = int(len(z)**.5)  #trash
        #z_matrix = z.reshape(nnz, nnz) #trash
        #x2 = correlength[d0][time]
        #y0 = activity[d0][speed]
        #y2 = correlength[d0][vcl]
        color_linage = ""
        color_linage = get_color_linage(d0)
        #if "hacat" in d0:
        #    y1 = y1*np.sqrt(6./7.)
        #    y2 = y2*np.sqrt(6./7.)
        #if "cal27" in d0:
        #    y1 = y1*np.sqrt(6./10.)
        #    y2 = y2*np.sqrt(6./10.)
        if axis == ax7:
            axis.scatter(x,y,c=z,cmap="jet")
            axis.plot(x,y,color="black",lw=1.0)
            axis.plot(x[0],y[0],"w*",ms=12)
            yt=4
            #axis.plot([6],[125+yt],"w*",ms=12)
            #axis.scatter([6,6,6,6],[100+yt,75+yt,50+yt,25+yt],c=[1,12,24,48],cmap="jet",s=55)
            #axis.text(8,125,"Inicio: t = 0 h",fontsize=10,color="black",weight="bold")
            #axis.text(8,100,"t =  1 h",fontsize=10,color="black",weight="bold")
            #axis.text(8,75,"t = 12 h",fontsize=10,color="black",weight="bold")
            #axis.text(8,50,"t = 24 h",fontsize=10,color="black",weight="bold")
            #axis.text(8,25,"t = 48 h",fontsize=10,color="black",weight="bold")
            axis.patch.set_facecolor('xkcd:salmon')
            axis.patch.set_alpha(0.4)
            #axis.plot((-20,-10),(-10,-10),color="red",lw=2.5,label=r"$\lambda$")
            #axis.plot((-20,-10),(-10,-10),color="blue",lw=2.5,label=r"$\chi_{int}$")
            #axis.plot((-20,-10),(-10,-10),"w,",label=r"$\tau_{act}=1/B$")
            #print("ola")  #debug line
            continue
        else:
            #axis.plot(x2,y2,color="red",lw=2.5)
            #plt.imshow(z, extent=(np.amin(x_list), np.amax(x_list), np.amin(y_list), np.amax(y_list)), norm=LogNorm(), aspect = 'auto') #trash
            #axis.imshow(z_matrix, extent=(x,y), norm=LogNorm(), aspect = 'auto') #trash
            #Q = plt.quiver(X,Y,2*Vx,2*Vy,Vr,units='xy' ,scale=1.0,cmap=cmap_type,headwidth=3.0,headlength=5.0,headaxislength=4.5) #trash
            #axis.quiver(x,y,vx,vy,z,units='xy' ,scale=1.0,cmap="jet")#,headwidth=3.0,headlength=5.0,headaxislength=4.5) #some other option
            axis.scatter(x,y,c=z,cmap="jet")
            axis.plot(x,y,color="black",lw=1.0)
            axis.plot(x[0],y[0],"w*",ms=12)
            if d0 == "hacat-m02":
                axis.text(16.0,23.0,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
            else:
                axis.text(16.0,126.0,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
            print(d0)   #debug line
        if d0 == "fibroblastos-m012":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(15,145)
            #print("ola")   #debug line
            #continue
        #axis.set_xlim(-1.7,52.5)
        axis.set_xlim(0,27.0)
        axis.set_xticks((0,6,12,18,24))
        axis.set_yticks((0,30,60,90,120))
        #print(d0, axis.get_ylim())     # debug line
        axis.patch.set_facecolor('xkcd:salmon')
        axis.patch.set_alpha(0.4)
        if d0 == "fibroblastos-m012":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            #axis.set_ylabel(r"[ $\mu m$ ]")
            axis.set_ylabel(r"correlation length [ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("velocity [ $\mu m/h$ ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=17,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.clim(0,50) ; plt.colorbar(label= "time [h]")
    #plt.show()
    prefix_label += ""
    #name_out_image = "%s%sact-chi-hacat-m04.png" % (local_path_to_cpa,prefix_label)
    name_out_image = "%s%sact-chi-fibroblastos.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    print("onde termina? Procure por: @aqui2@")
    exit()    
    
    #**** ploting the normalized total degree of correlation graph ****#
    plt.rcParams.update({'font.size': 20})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x = corredegree[d0][time]
        y = corredegree[d0][nchi]
        mean_nChi = corredegree[d0][meannchi]
        stdv_nChi = corredegree[d0][stdvmeannchi]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="cornflowerblue",lw=2.5,label=r"$\overline{C}_{vv}^{+}$")
            #axis.plot((-20,-10),(-10,-10),color="orange",ls="--",lw=2.5,label=r"$<\chi_{I}/\xi>_{5h\leq t \leq45h}$")
            axis.plot((-20,-10),(-10,-10),color="orange",ls="--",lw=2.5,label=r"$\eta$")
            #axis.plot((-20,-10),(-10,-10),"w,",label=r"$\eta=<\overline{C}_{vv}^{+}>_{5h\leq t \leq45h}$")
            #axis.text(3,0.2,r"$\eta=<\overline{C}_{vv}^{+}>_{5h\leq t \leq45h}$",fontsize=14,weight="bold")
            axis.text(-1,0.15,r"$\eta=<\overline{C}_{vv}^{+}>_{5h\leq t \leq45h}$",fontsize=16,weight="bold")
            #axis.text(3,0.1,r"$\delta\eta=$ stdv of $\eta$",fontsize=14)
        else:
            #axis.plot(x,y,color="cornflowerblue",lw=2)#,label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
            #axis.plot((5.0,45.0),(mean_nChi,mean_nChi),color="orange",lw=2,ls="dashed")
            #axis.text(30.7,0.53,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
            axis.plot(x,y,color="cornflowerblue",lw=2.5)#,label="%s-%s-piv%s"%(d0,hyst_or_norm,extension))
            axis.plot((5.0,45.0),(mean_nChi,mean_nChi),color="orange",lw=3,ls="dashed")
            axis.text(26.7,0.53,d0.split("-")[1].replace("m","movie "),fontsize=15,color=color_linage,weight="bold")
            #if color_linage == "blue":
            #    axis.text(1.0,2.2,r"$\tau_{act}\approx$%2.0fh"%(tau_activity),fontsize=11, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
            #else:
            #    axis.text(31.5,21.,r"$\tau_{act}\approx$%2.0fh"%(tau_activity),fontsize=11, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
            if d0 == "fibroblastos-m01":
                #axis.text(1.0,0.05,r"$<\chi_{I}/\xi>_{t}\approx$%2.2f"%(mean_nChi),fontsize=11, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
                axis.text(1.0,0.05,r"$\eta\approx$%2.2f"%(mean_nChi),fontsize=12, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
                axis.text(22.0,0.05,r"$\delta\eta\approx$%2.3f"%(stdv_nChi),fontsize=12, bbox=dict(facecolor='red', alpha=0.3,boxstyle="round"))
                #axis.text(1.0,0.05,r"$<\chi_{I}/\xi>_{t}\approx$%2.2f$\pm$%2.2f"%(mean_nChi,stdv_nChi),fontsize=11,bbox=dict(facecolor='orange',alpha=0.3,boxstyle="round"))
            else:
                #axis.text(1.0,0.05,r"$<\chi_{I}/\xi>_{t}\approx$%2.2f"%(mean_nChi),fontsize=11, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
                #axis.text(1.0,0.05,r"$<\chi_{I}/\xi>_{t}\approx$%2.2f$\pm$%2.2f"%(mean_nChi,stdv_nChi),fontsize=11,bbox=dict(facecolor='orange',alpha=0.3,boxstyle="round"))
                #axis.text(1.0,0.05,r"$\eta\approx$%2.2f"%(mean_nChi),fontsize=12, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
                #axis.text(22.0,0.05,r"$\delta\eta\approx$%2.3f"%(stdv_nChi),fontsize=12, bbox=dict(facecolor='red', alpha=0.3,boxstyle="round"))
                axis.text(1.0,0.05,r"$\eta\approx$%2.2f"%(mean_nChi),fontsize=14, bbox=dict(facecolor='orange', alpha=0.3,boxstyle="round"))
                axis.text(25.0,0.05,r"$\delta\eta\approx$%2.3f"%(stdv_nChi),fontsize=14, bbox=dict(facecolor='red', alpha=0.3,boxstyle="round"))
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,0.6)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        #axis.set_yticks((0,0.15,0.30,0.45))
        axis.set_yticks((0.10,0.30,0.50))
        #axis.set_yticks((0,0.12,0.24,0.36,0.48))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            #axis.set_ylabel(r"$\chi_{I}$ / $\xi$")
            #axis.set_ylabel(r"$\overline{C}_{vv}^{+}$ [ - ]")
            axis.set_ylabel("[ - ]",fontsize=22)
            #axis.set_ylabel(r"$C_{vv}^{+}$")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]",fontsize=22)
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            #axis.legend(loc="center",fontsize=13,frameon=False)
            axis.legend(loc="upper left",fontsize=20,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%snormalized_chi.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** ploting the total degree + vcl graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x1 = corredegree[d0][time]
        y1 = corredegree[d0][chi]
        x2 = correlength[d0][time]
        y2 = correlength[d0][vcl]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if "hacat" in d0:
            y1 = y1*np.sqrt(6./7.)
            y2 = y2*np.sqrt(6./7.)
        if "cal27" in d0:
            y1 = y1*np.sqrt(6./10.)
            y2 = y2*np.sqrt(6./10.)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="red",lw=2.5,label=r"$\lambda$")
            axis.plot((-20,-10),(-10,-10),color="blue",lw=2.5,label=r"$\chi_{int}$")
            #axis.plot((-20,-10),(-10,-10),"w,",label=r"$\tau_{act}=1/B$")
        else:
            axis.plot(x2,y2,color="red",lw=2.5)
            axis.plot(x1,y1,color="blue",lw=2.5)
            axis.text(30.7,8.0,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,160)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,45,90,135))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            #axis.set_ylabel(r"[ $\mu m$ ]")
            axis.set_ylabel(r"SCC diamater")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=17,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%schi_vcl.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()
    
    #**** ploting the distance of zero Cvv graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x = corredegree[d0][time]
        y = corredegree[d0][xi]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.text(5.7,130.53,r"$\xi$ "+"is the effective\n distance of zero\n correlation:\n\n"+r"  $C_{vv}(\Delta r=\xi)=0$",fontsize=12,color="black",weight="bold")
            continue
        else:
            axis.plot(x,y,color="violet",lw=2.5)
            axis.text(30.7,15.53,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,400)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,100,200,300))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"$\xi$ [ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=13,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sdistance_of_zero_Cvv.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** ploting the fitx2 swirls sizes graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x = swirl[d0][time]
        y1 = swirl[d0][fitx2]
        y2 = swirl[d0][locmin]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="crimson",marker="o",lw=2.5,label=r"via fit x2")
            #axis.plot((-20,-10),(-10,-10),color="black",lw=2.5,label=r"via local minima")
            #axis.plot((-20,-10),(-10,-10),"w,",label=r"$\tau_{act}=1/B$")
        else:
            axis.plot(x,y1,color="crimson",marker="o",ms=4,lw=1.0)
            #axis.plot(x,y2,color="black",lw=2.5)
            axis.text(30.7,20.53,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,715)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,150,300,450,600))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"swirls characteristic length [ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=13,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sswirls_length_fitx2.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** ploting the fitx2 + locmin swirls sizes graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x = swirl[d0][time]
        y1 = swirl[d0][fitx2]
        y2 = swirl[d0][locmin]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="crimson",marker="o",lw=2.5,label=r"via fit x2")
            axis.plot((-20,-10),(-10,-10),color="black",lw=2.5,label=r"via local minima")
            #axis.plot((-20,-10),(-10,-10),"w,",label=r"$\tau_{act}=1/B$")
        else:
            axis.plot(x,y1,color="crimson",marker="o",ms=4,lw=1.0)
            axis.plot(x,y2,color="black",lw=1.5)
            axis.text(30.7,20.53,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,715)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,150,300,450,600))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"swirls characteristic length [ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=13,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sswirls_length_fitx2_locmin.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** ploting the combined vaf graf total degree + vcl graph + locmin ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x1 = corredegree[d0][time]
        y1 = corredegree[d0][chi]
        x2 = correlength[d0][time]
        y2 = correlength[d0][vcl]
        x3 = swirl[d0][time]
        y3 = swirl[d0][locmin]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="black",lw=2.5,label="Swirl length")
            axis.plot((-20,-10),(-10,-10),"w,",label="(via local minima)")
            axis.plot((-20,-10),(-10,-10),color="red",lw=2.5,label=r"correlation length")
            axis.plot((-20,-10),(-10,-10),color="blue",lw=2.5,label=r"$\chi_{I}$")
        else:
            axis.plot(x2,y2,color="red",lw=2.5)
            axis.plot(x1,y1,color="blue",lw=2.5)
            axis.plot(x3,y3,color="black",lw=1.5)
            axis.text(31.7,650.0,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,715)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,150,300,450,600))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"[ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=13,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%scombined_vaf.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** ploting the combined vaf graf total degree + vcl graph + locmin + \xi ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x1 = corredegree[d0][time]
        y1 = corredegree[d0][chi]
        x2 = correlength[d0][time]
        y2 = correlength[d0][vcl]
        x3 = swirl[d0][time]
        y3 = swirl[d0][locmin]
        x4 = corredegree[d0][time]
        y4 = corredegree[d0][xi]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="black",lw=2.5,label="Swirl length")
            axis.plot((-20,-10),(-10,-10),"w,",label="(via local minima)")
            axis.plot((-20,-10),(-10,-10),color="red",lw=2.5,label=r"correlation length")
            axis.plot((-20,-10),(-10,-10),color="blue",lw=2.5,label=r"$\chi_{I}$")
            axis.plot((-20,-10),(-10,-10),color="violet",lw=2.5,label=r"$\xi$")
        else:
            axis.plot(x2,y2,color="red",lw=2.5)
            axis.plot(x1,y1,color="blue",lw=2.5)
            axis.plot(x3,y3,color="black",lw=1.5)
            axis.plot(x4,y4,color="violet",lw=1.5)
            axis.text(31.7,650.0,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,715)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,150,300,450,600))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"[ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=13,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%scombined_vaf_2.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** ploting the \xi + locmin graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x1 = swirl[d0][time]
        y1 = swirl[d0][locmin]
        x2 = corredegree[d0][time]
        y2 = corredegree[d0][xi]
        color_linage = ""
        color_linage = get_color_linage(d0)
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="black",lw=2.5,label="Swirl length")
            axis.plot((-20,-10),(-10,-10),"w,",label="(via local minima)")
            axis.plot((-20,-10),(-10,-10),color="violet",lw=2.5,label=r"$\xi$")
            #axis.plot((-20,-10),(-10,-10),"w,",label=r"$\tau_{act}=1/B$")
        else:
            axis.plot(x1,y1,color="black",lw=1.5)
            axis.plot(x2,y2,color="violet",lw=1.5)
            axis.text(30.7,20.53,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,715)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,150,300,450,600))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"[ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=13,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sxi_and_locmin.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

    #**** ploting the \xi + locmin graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        x1 = swirl[d0][time]
        y1 = swirl[d0][locmin]
        x2 = corredegree[d0][time]
        y2 = corredegree[d0][xi]
        color_linage = ""
        color_linage = get_color_linage(d0)
        factor=1.6
        if axis == ax7:
            axis.plot((-20,-10),(-10,-10),color="black",lw=2.5,label="Swirl length")
            axis.plot((-20,-10),(-10,-10),"w,",label="(via local minima)")
            axis.plot((-20,-10),(-10,-10),color="violet",lw=2.5,label=r"%1.1fx$\xi$"%(factor))
            #axis.plot((-20,-10),(-10,-10),"w,",label=r"$\tau_{act}=1/B$")
        else:
            axis.plot(x1,y1,color="black",lw=1.5)
            axis.plot(x2,factor*y2,color="violet",lw=1.5)
            axis.text(30.7,20.53,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,715)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,150,300,450,600))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"[ $\mu m$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ h ]")
        if axis in top_axis_list:
            axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
        if axis == ax7:
            axis.legend(loc="center",fontsize=13,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sxi_times_two__and_locmin.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

#**************************************************************#
#**** END : Section 4 *****************************************#
#**************************************************************#
#exit()
#print

#******************************************************************#
#**** Section 5 : making resutls format self explanatory graph ****#
#******************************************************************#

if 2 == 2:
    #**** ploting the self explanatory graph ****#
    plt.rcParams.update({'font.size': 14})
    #for i in range(len(x)): print(x[i], y[i]  # debug line
    ffs = 1.5  # figure size factor scale
    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row',
                            gridspec_kw={'hspace': 0, 'wspace': 0},figsize=(ffs*6,ffs*5))    
    (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) = axs
    axis_list = [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9]
    top_axis_list = [ax1,ax2,ax3] ; ylabel_axis_list = [ax4] ; xlabel_axis_list = [ax8]
    um = 1
    for axis in axis_list:
        index = axis_list.index(axis)+um
        if axis == ax7:
            um = 0 ; index = 11
        d0 = d0_list[index]
        #x1 = swirl[d0][time]
        #y1 = swirl[d0][locmin]
        #x2 = corredegree[d0][time]
        #y2 = corredegree[d0][xi]
        color_linage = ""
        color_linage = get_color_linage(d0)
        factor=1.6
        theta = np.linspace(0, 2*np.pi, 100)
        r = np.sqrt(20.0) ; xc = r*np.cos(theta) ; yc = r*15*np.sin(theta)
        if axis == ax7:
            #axis.plot((-20,-10),(-10,-10),color="black",lw=2.5,label="Swirl length")
            axis.text(5.7,480.53,"Graphs data labels\n            and\n extra info region:",fontsize=12,color="black",weight="bold")
            axis.plot((-20,-10),(-10,-10),color="cornflowerblue",label="a light blue circle",lw=2.5)
            #axis.plot((-20,-10),(-10,-10),color="deeppink",label="a pink circle",lw=2.5)
            #axis.plot((-20,-10),(-10,-10),"w,",label="\"movie 03\" is the short\n for \"replicate number 3\n graph\"")
            #axis.text(1.6,110.53,"\"movie 03\" is the short\n for \"replicate number 3\n graph\"", fontsize=12)
            a=-10
            axis.text(1.6,240.53+a,"\"",fontsize=12)
            axis.text(3.5,240.53+a,"movie 03",fontsize=12,color="darkorange",weight="bold")
            axis.text(23.5,240.53+a,"\" is the short",fontsize=12)
            axis.text(2.5,168.53+a,"for \"",fontsize=12)
            #axis.text(11.0,46.53,"replicate\nnumber 3\ngraph",fontsize=12,color="darkorange",weight="bold")
            #axis.text(11.0,46.53,"replicate number\n3 graph",fontsize=12,color="darkorange",weight="bold")
            axis.text(11.0,168.53+a,"replicate number",fontsize=12,color="darkorange",weight="bold")
            axis.text(2.5,96.53+a,"3 graph",fontsize=12,color="darkorange",weight="bold")
            axis.text(19.6,96.53+a,"\"",fontsize=12)
            #axis.plot((-20,-10),(-10,-10),color="violet",lw=2.5,label=r"%1.1fx$\xi$"%(factor))
            #axis.plot((-20,-10),(-10,-10),"w,",label=r"$\tau_{act}=1/B$")
        else:
            #axis.plot(xc+10,yc+220,color="hotpink",lw=2.5)
            #axis.plot(xc+10,yc+220,color="deeppink",lw=2.5)
            axis.plot(xc+10,yc+200,color="cornflowerblue",lw=2.5)
            #axis.plot(xc+13,yc+250,color="turquoise",lw=2.5)
            #axis.plot(x1,y1,color="black",lw=1.5)
            #axis.plot(x2,factor*y2,color="violet",lw=1.5)
            a=10
            if axis == ax8:                
                axis.text(24.7,550.53+a,d0.split("-")[1].replace("m","movie "),fontsize=12,color=color_linage,weight="bold")
            else:
                axis.text(24.7,430.53+a,d0.split("-")[1].replace("m","replicate\n").replace("0","number ")+"\ngraph",fontsize=12,color=color_linage,weight="bold")
        if d0 == "fibroblastos-m01":
            axis.set_ylim(0,0.65)
        else:
            axis.set_ylim(0,715)
        axis.set_xlim(-1.7,52.5)
        axis.set_xticks((0,15,30,45))
        axis.set_yticks((0,200,400,600))
        #print(d0, axis.get_ylim()     # debug line
        if d0 == "fibroblastos-m01":
            axis.set_xlim(15.0,75)
        if axis in ylabel_axis_list:
            axis.set_ylabel(r"some physical quantity [ $Y$ $units$ $for$ $all$ $graphs$ ]")
            continue
        if axis in xlabel_axis_list:
            axis.set_xlabel("time [ $time$ $units$ $for$ $all$ $graphs$ ]")
        if axis in top_axis_list:
            #axis.set_title(d0.split("-")[0],color=color_linage,weight='bold')
            if color_linage == "blue":
                axis.set_title("Healthy\n Linage Name",color=color_linage,weight='bold')
            elif color_linage == "darkorange":
                axis.set_title("Benign\n Linage Name",color=color_linage,weight='bold')
            elif color_linage == "red":
                axis.set_title("Malignant\n Linage Name",color=color_linage,weight='bold')
            else:
                print(" unsuported linage type for titles in self explanatory graph  --> code stoped!")
                exit()
        if axis == ax7:
            axis.legend(loc="center left",fontsize=13,frameon=False)
        else:
            continue
            #axis.legend(fontsize=12)
    #plt.show()
    prefix_label += ""
    name_out_image = "%s%sself_explanatory_graph.png" % (local_path_to_cpa,prefix_label)
    plt.savefig(name_out_image, bbox_inches='tight')
    plt.close()
    #**** ****#
    #exit()

#******************************************************************#
#**** END : Section 5 *********************************************#
#******************************************************************#
#exit()
#print

print("\n END CPA.py <<<\n")
exit()




