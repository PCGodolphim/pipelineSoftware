import os
import sys
import numpy as np


#**********************************************************************#
#**** Section 0: Making directories tree and defining in variables ****#
#**********************************************************************#

#**** in variables ****#
d0 = sys.argv[1] ; # d0 = <d0> = linage-movie directory
local_path_to_field = sys.argv[2]    # data files directory path
local_path_to_fluctu = sys.argv[3]   # data files directory path
hyst_or_norm = sys.argv[4]   # "hyst" or "norm" images
extension = sys.argv[5]   # tif, bmp, png ...
number_of_frames = int(sys.argv[6])
number_of_vectors = int(sys.argv[7])    # after vff
#shape_field_file_name = sys.argv[8]   # shape = string = "<name> len(<name>)"

field_file_name = sys.argv[8]
len_field_file_name = int(sys.argv[9])
fluctu_file_name = sys.argv[10]
len_fluctu_file_name = int(sys.argv[11])

field_file_path_and_name = sys.argv[12]
len_field_file_path_and_name = int(sys.argv[13])
fluctu_file_path_and_name = sys.argv[14]
len_fluctu_file_path_and_name = int(sys.argv[15])

path_to_VOID_pipeline_scripts = sys.argv[16];  # path to VOID pipeline scripts
path_to_pipeline = sys.argv[17];  # path to pipeline
local_path_to_dsoi = sys.argv[18]  
non_interactive_mode = sys.argv[19]
absolut_time_correction_factor_in_hours = sys.argv[20]

#print d0+"\n"+local_path_to_field+"\n"+local_path_to_fluctu+"\n"+hyst_or_norm+"\n"+extension+"\n"+str(number_of_frames)+"\n"+str(number_of_vectors)+"\n"+field_file_name+" "+str(len_field_file_name)+"\n"+fluctu_file_name+" "+str(len_fluctu_file_name)+"\n"+field_file_path_and_name+" "+str(len_field_file_path_and_name)+"\n"+fluctu_file_path_and_name+" "+str(len_fluctu_file_path_and_name)+"\n"+path_to_VOID_pipeline_scripts+"\n"+path_to_pipeline+"\n"+local_path_to_dsoi+"\n"+non_interactive_mode+"\n"+absolut_time_correction_factor_in_hours   # debug line
#hyst_or_norm = "norm"   # debug line
#** Security routine and defining last varibales:
if number_of_vectors != 4312:     #security routine
        print "ERROR:   Unsuported number of vectors."
        print "         Calculations are only working for number of vectors = 4312"
        print "code stoped!"
        exit()
else:
        N_X = 77 ; N_Y = 56 ; DL = 16
#!integer, parameter :: N_vectors = 5440, N_x = 85, N_y = 64, dl = 16
#!integer, parameter :: N_vectors = 4860, N_x = 81, N_y = 60, dl = 16
#!integer, parameter :: N_vectors = 4312, N_x = 77, N_y = 56, dl = 16
#**** ****#
#exit()

#**** making directories ****#
d8 = "Calculations"  ; os.system("mkdir -p %s/%s" % (d0,d8))
#d8log = "LOG" ; os.system("mkdir -p %s/%s/%s" % (d0,d8,d8log))
d8vaf = "vaf" ; os.system("mkdir -p %s/%s/%s" % (d0,d8,d8vaf))
d8adv = "adv" ; os.system("mkdir -p %s/%s/%s" % (d0,d8,d8adv))
d8dna = "dna" ; os.system("mkdir -p %s/%s/%s" % (d0,d8,d8dna))
#d8quali = "Quality" ; os.system("mkdir -p %s/%s/%s" % (d0,d8,d8quali))
#**** ****#
#exit()

#**** defining necessary paths ****#
#path_to_clc = "%s%s/%s/%s/" % (path_to_pipeline,d0,d8)
local_path_to_clc = "%s/%s/" % (d0,d8)

#path_to_log = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d8,d8log)
#local_path_to_log = "%s/%s/%s/" % (d0,d8,d8log)

#path_to_vaf = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d8,d8vaf)
local_path_to_vaf = "%s/%s/%s/" % (d0,d8,d8vaf)

#path_to_adv = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d8,d8adv)
local_path_to_adv = "%s/%s/%s/" % (d0,d8,d8adv)

#path_to_dna = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d8,d8dna)
local_path_to_dna = "%s/%s/%s/" % (d0,d8,d8dna)

#path_to_clc_scripts = "%sz_MAIN_pipeline_scripts/calculations_scripts/" % (path_to_pipeline)
local_path_to_clc_scripts = "z_MAIN_pipeline_scripts/calculations_scripts/"
#**** ****#
#exit()

#**** defining used scripts list ****#
#adv_used_scripts_list = []   # not in use
vaf_used_scripts_list = []
#dna_used_scripts_list = []   # not in use
#**** ****#
#exit()

#**** defining scripts, executables and auxiliar data files names ****#
#** adv:
adv_py = "activity_and_drift_velocities-1.3.py"

#** vaf:
vaf_f90_script = "vaf-2.0.f90"
vaf_f90_executable = vaf_f90_script.replace("-","_f90-").replace(".f90",".ex")
#print vaf_f90_executable+"\n"+"./vaf_f90.ex"    # debug line

vaf_bash_gnu_N_by_N_plot_script = "script_to_plot_at_each_N_frames-dr_bin16_via_gnu_script-1.1.sh"
vaf_bash_gnu_N_by_N_plot_script_gt0cut = "script_to_plot_at_each_N_frames-gt0cut_via_gnu_script-1.2.sh"

vaf_vdot_mask_file = "Vdots_mask_value_77x56.vaff90"
vaf_dr2_distribution = "dr2_distribution-77x56.ddm"

vaf_py = "velocity_autocorrelation_function-4.0.py"

#** dna:
# dna_py = "dna.py"  # not in use
#**** ****#
#exit()

#**********************************************************************#
#**** END : Section 0 *************************************************#
#**********************************************************************#
#exit()
print

#************************************************************************#
#**** Section 1  - initializing ADV necessary scripts and data files ****#
#************************************************************************#

#nothing to initialize yet

#************************************************************************#
#**** END : Section 1  **************************************************#
#************************************************************************#
#exit()
#print

#**************************************************************#
#**** Section 2 - activity and drift velocity calculations ****#
#**************************************************************#

#**** calling "vaf_py" script ****#
so1 = d0
so2 = local_path_to_field
so3 = local_path_to_fluctu
so4 = hyst_or_norm
so5 = extension
so6 = str(number_of_frames)
so7 = str(number_of_vectors)
so8 = local_path_to_adv
so9 = "no_adv_auxiliar_script_defined" #vaf_f90_executable
so10 = path_to_pipeline
so11 = absolut_time_correction_factor_in_hours
string_to_call_adv_py = "python %s %s %s %s %s %s %s %s %s %s %s %s" % (adv_py, so1, so2, so3, so4, so5, so6, so7, so8, so9, so10, so11)
os.system(string_to_call_adv_py)
#**** ****#
#exit()

#**** updating adv used script list ****#
#not in use
#adv_used_scripts_list.append()
#**** ****#
#exit()

#**************************************************************#
#**** END : Section 2 *****************************************#
#**************************************************************#
#exit()
print

#************************************************************************#
#**** Section 3  - initializing VAF necessary scripts and data files ****#
#************************************************************************#

#**** modifing vaf_f90 script and moving to folder ****#
file_in_to_open = "%sVOID-%s" % (path_to_VOID_pipeline_scripts,vaf_f90_script)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
file_out_to_open = "%s%s" % (local_path_to_vaf,vaf_f90_script)
file_out = open(file_out_to_open,"w")
data=data.replace("<NUMBER_OF_FRAMES>",str(number_of_frames))  
data=data.replace("<NUMBER_OF_VECTORS>",str(number_of_vectors))
data=data.replace("<LATTICE_X_LENGTH>",str(N_X))
data=data.replace("<LATTICE_Y_LENGTH>",str(N_Y))
data=data.replace("<LATTICE_CHARACTERISTIC_LENGTH>",str(DL))
data=data.replace("<LEN_NAME_FLUCTUATION_FILES>",str(len_fluctu_file_path_and_name+9))
#data=data.replace("<LEN_NAME_CVV_F90_TMP_FILES>",)
file_out.write(data)
file_in.close()
file_out.close() ; print
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** making vaf_f90 executable ****#
string_to_make_vaf_f90_executable = "gfortran -O3 <path>%s -o <path>%s"%(vaf_f90_script,vaf_f90_executable)
string_to_make_vaf_f90_executable = string_to_make_vaf_f90_executable.replace("<path>",local_path_to_vaf) 
#print string_to_make_vaf_f90_executable   # debug line
os.system(string_to_make_vaf_f90_executable)
#**** ****#
#exit()

#**** coping vaf plot scripts and necessary data files to vaf folder ****#
string_to_copy_vaf_plot_and_data_scripts = "cp <path>%s <path>%s <path>%s <path>%s %s."%(vaf_bash_gnu_N_by_N_plot_script,vaf_bash_gnu_N_by_N_plot_script_gt0cut,vaf_vdot_mask_file,vaf_dr2_distribution,local_path_to_vaf)
string_to_copy_vaf_plot_and_data_scripts = string_to_copy_vaf_plot_and_data_scripts.replace("<path>",local_path_to_clc_scripts)
#print string_to_copy_vaf_plot_and_data_scripts   # debug line
os.system(string_to_copy_vaf_plot_and_data_scripts)
#**** ****#
#exit()

#***********************************************************************#
#**** END : Section 3 **************************************************#
#***********************************************************************#
#exit()
print

#************************************************************#
#**** Section 4 - velocity auto correlation calculations ****#
#************************************************************#

#**** defining the dr bin size (in pixels)****#
dr_bin_size = 16
#**** ****#
#exit()

#**** making vaf necessary directories ****#
dCvv = "Cvv" ; os.system("mkdir -p %s%s" % (local_path_to_vaf,dCvv))
dCvv_drbin = "Cvv_drBin_%s" % (dr_bin_size) ; os.system("mkdir -p %s%s" % (local_path_to_vaf,dCvv_drbin))
dused = "used_scripts" ; os.system("mkdir -p %s%s" % (local_path_to_vaf,dused))
dCvvgt0 = "Cvv_gt_0_cut" ; os.system("mkdir -p %s%s" % (local_path_to_vaf,dCvvgt0))
dCvvmin = "Cvv_minimun_values" ; os.system("mkdir -p %s%s" % (local_path_to_vaf,dCvvmin))
#**** ****#
#exit()

#**** calling "vaf_py" script ****#
so1 = d0
so2 = str(dr_bin_size)
so3 = local_path_to_fluctu
so4 = hyst_or_norm
so5 = extension
so6 = str(number_of_frames)
so7 = str(number_of_vectors)
so8 = local_path_to_vaf
so9 = vaf_f90_executable
so10 = path_to_pipeline
so11 = absolut_time_correction_factor_in_hours
string_to_call_vaf_py = "python %s %s %s %s %s %s %s %s %s %s %s %s" % (vaf_py, so1, so2, so3, so4, so5, so6, so7, so8, so9, so10, so11)
os.system(string_to_call_vaf_py)
#**** ****#
#exit()

#**** updating vaf used script list ****#
vaf_used_scripts_list.append(vaf_f90_script)
vaf_used_scripts_list.append(vaf_f90_executable)
vaf_used_scripts_list.append(vaf_vdot_mask_file)
vaf_used_scripts_list.append(vaf_dr2_distribution)
#**** ****#
#exit()

#***** Making N_by_N Cvv graphs via bash/gnu script *******#
#**********************************************************#

#**** executing bash script dr_bin to targte folder ****#
string_to_execute_bash_script = "cd %s ; bash %s 1 %i ; cd %s" % (local_path_to_vaf,vaf_bash_gnu_N_by_N_plot_script,number_of_frames,path_to_pipeline) 
#string_to_execute_bash_script = "cd %s ; bash %s 1 3 ; cd %s" % (local_path_to_vaf,vaf_bash_gnu_N_by_N_plot_script,path_to_pipeline)   # debug line
os.system(string_to_execute_bash_script)
#**** ****#
#exit()

#**** reading string with ab ex fit from tmp file generated bi vaf.py ****#
file_with_ab_fit_string_path_and_name = "%sstring-fit-ab.tmp"%(local_path_to_vaf)
file_with_ab_fit_string = open(file_with_ab_fit_string_path_and_name,"r")
string_fit_ab = file_with_ab_fit_string.read()
#print "ola", string_fit_ab  # debug line
#**** ****#
#exit()

#**** modifing bash gnu script gt0cut with the fit data from vaf.py  ****#
file_in_to_open = "%s%s" % (local_path_to_vaf,vaf_bash_gnu_N_by_N_plot_script_gt0cut)
#print file_in_to_open ; os.system("more %s" % (file_in_to_open))    # debug line
file_in = open(file_in_to_open,"r")
data = file_in.read()
file_out_to_open = "%s%s" % (local_path_to_vaf,vaf_bash_gnu_N_by_N_plot_script_gt0cut)
file_out = open(file_out_to_open,"w")
data=data.replace("<a0=a0;b0=b0;>",string_fit_ab)  
file_out.write(data)
file_in.close()
file_out.close() ; print
#print file_out_to_open ; os.system("more %s" % (file_out_to_open))    # debug line
#**** ****#
#exit()

#**** removing tmp file fit exp ab  ****#
os.system("rm %s"%(file_with_ab_fit_string_path_and_name))
#**** ****#
#exit()

#**** executing bash scrpt gt0cut to target folder ****#
string_to_execute_bash_script = "cd %s ; bash %s 1 %i ; cd %s" % (local_path_to_vaf,vaf_bash_gnu_N_by_N_plot_script_gt0cut,number_of_frames,path_to_pipeline) 
#string_to_execute_bash_script = "cd %s ; bash %s 1 3 ; cd %s" % (local_path_to_vaf,vaf_bash_gnu_N_by_N_plot_script_ft0cut,path_to_pipeline)   # debug line
os.system(string_to_execute_bash_script)
#**** ****#
#exit()

#**** updating vaf used script list ****#
vaf_used_scripts_list.append(vaf_bash_gnu_N_by_N_plot_script)
vaf_used_scripts_list.append("script_to_plot_Cvv_at_each_N_frames-dr_bin16.gnu")
vaf_used_scripts_list.append(vaf_bash_gnu_N_by_N_plot_script_gt0cut)
vaf_used_scripts_list.append("script_to_plot_Cvv_at_each_N_frames-gt0cut.gnu")
#**** ****#
#exit()

#***** END: Making N_by_N Cvv graphs **********************#
#**********************************************************#
#exit()

#**** moving data files and scripts used to right folders ****#
string_to_move_vaf_data_files = "mv %s*Cvv.vaf %s%s/." % (local_path_to_vaf,local_path_to_vaf,dCvv)
#print string_to_move_vaf_data_files    # debug line
os.system(string_to_move_vaf_data_files)
string_to_move_vaf_data_files = "mv %s*Cvv-drbin%i.vaf %s%s/." % (local_path_to_vaf,dr_bin_size,local_path_to_vaf,dCvv_drbin)
#print string_to_move_vaf_data_files    # debug line
os.system(string_to_move_vaf_data_files)
string_to_move_vaf_data_files = "mv %s*Cvv-gt0cut.vaf %s%s/." % (local_path_to_vaf,local_path_to_vaf,dCvvgt0)
#print string_to_move_vaf_data_files    # debug line
os.system(string_to_move_vaf_data_files)
string_to_move_vaf_image_files = "mv %s*Cvv-gt0cut-vaf.png %s%s/." % (local_path_to_vaf,local_path_to_vaf,dCvvgt0)
#print string_to_move_vaf_image_files    # debug line
os.system(string_to_move_vaf_image_files)
string_to_move_vaf_data_files = "mv %s*Cvv-minvalues.vaf %s%s/." % (local_path_to_vaf,local_path_to_vaf,dCvvmin)
#print string_to_move_vaf_data_files    # debug line
os.system(string_to_move_vaf_data_files)
string_to_move_vaf_image_files = "mv %s*Cvv-minvalues-vaf.png %s%s/." % (local_path_to_vaf,local_path_to_vaf,dCvvmin)
#print string_to_move_vaf_image_files    # debug line
os.system(string_to_move_vaf_image_files)
for ele in vaf_used_scripts_list:
        string_to_move_used_script = "mv %s%s %s%s/." % (local_path_to_vaf,ele,local_path_to_vaf,dused)
        #print string_to_move_used_script   # debug line
        os.system(string_to_move_used_script)
#**** ****#
#exit()

#************************************************************#
#**** END : Section 4 ***************************************#
#************************************************************#
exit()
print

#****************************************************************#
#**** Section 5 : Number Density and Area (dna) calculations ****#
#****************************************************************#

# UNDER CONSTRUCTION   (nothing here yet)

#****************************************************************#
#**** END : Section 5 *******************************************#
#****************************************************************#
exit()
print
