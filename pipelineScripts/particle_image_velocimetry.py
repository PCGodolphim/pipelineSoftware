#!/usr/bin/env python
# the version 1.0 of this code is equal to "piv.py" from "../resultados_parcias.../"
import os
import sys
import math
import openpiv.tools
import openpiv.process
import openpiv.scaling
import openpiv.validation
import openpiv.filters
import numpy as np
import subprocess as subp
import matplotlib.pyplot as plt

#********************************************************#
#**** Section (-1): defining PIV function ***************#
#********************************************************#

def openPIV(frame_A_name,frame_B_name,file_out_name,raw_file_out_name,path_to_images,extension,out_images_path):

        #**** opening piv data out file and writing header ****#
	file_out = open(file_out_name,'w')
        file_out.write("#  Mask values are boolean: 1 = \"replaced vector by piv replace_outlier function\" and 0 = \"non replaced vector (original)\"\n")
        file_out.write("#  X[px]  Y[px]  Vx[px/h]  Vy[px/h]  Mask[boolean]\n")
        #**** ****#
        #exit()
        
        #**** opening praw iv data out file and writing header ****#
	raw_file_out = open(raw_file_out_name,'w')
        raw_file_out.write("#  X[px]  Y[px]  Vx[px/h]  Vy[px/h]  Garbage(ingnore this column)\n")
        #**** ****#
        #exit()

        #**** defining title and name (on VOID mode) of the piv steps images ****#
        title = "frame pair (A-B): frame A = %s\n" % (frame_A_name.replace(path_to_images,""))
        title += "                            frame B = %s\n" % (frame_B_name.replace(path_to_images,""))
        title += "<PIV_STEP_TITLE>\n"
        name_out_image = (frame_A_name.replace(path_to_images,"%spiv%s-"%(out_images_path,extension))).replace(".%s"%(extension),"-<PIV_STEP_LABEL>")
        #**** ****#
        #exit()

        #**** opening frame pair (A-B) images reading/making them as arrays of pixels intenisty ****#
	frame_A = open(frame_A_name,'r')
	frame_A = openpiv.tools.imread(frame_A)

	frame_B = open(frame_B_name,'r')
	frame_B = openpiv.tools.imread(frame_B)
        
	frame_A = np.int32(frame_A)
	frame_B = np.int32(frame_B)
        #**** ****#
        #exit()

        #**** calling openpiv-funtions that calculated the vector field of frame pair A-B ****#
	u, v, sig2noise = openpiv.process.extended_search_area_piv( frame_A, frame_B, window_size=32, overlap=16, dt=0.25, search_area_size=86, sig2noise_method='peak2peak' )
                                                                                                                 #dt=15min 
	x, y = openpiv.process.get_coordinates( image_size=frame_A.shape, window_size=32, overlap=16 )
        #**** ****#
        #exit()

        #**** saving/writing raw piv out data in file ****#
	openpiv.tools.save(x, y, u, v, (x-0.99*x), raw_file_out)
        #**** ****#
        #exit()

        #**** fig out step A ****#
        fig, ax = plt.subplots()
        ax.set_xlabel("X [px]") ; ax.set_ylabel("Y [px]") ; ax.set_title(title.replace("<PIV_STEP_TITLE>","RAW PIV OUTPUT"),fontsize=12)
        fig.set_size_inches(8.5*1.3, 6.5*1.3)
        Q = plt.quiver( x, y, u, v )
        plt.savefig(name_out_image.replace("<PIV_STEP_LABEL>","A"), bbox_inches='tight')
        #plt.show()   # debug line
        plt.close()
        #**** ****#
        #exit()

        #**** calling openpiv-python function that defines wich vectors are outliers ****#
	u, v, mask = openpiv.validation.sig2noise_val( u, v, sig2noise, threshold = 1.3 )
        #**** ****#
        #exit()

        #**** fig out step B ****#
        fig, ax = plt.subplots()
        ax.set_xlabel("X [px]") ; ax.set_ylabel("Y [px]") ; ax.set_title(title.replace("<PIV_STEP_TITLE>","AFTER SIG2NOISE ELIMINATION"),fontsize=12)
        fig.set_size_inches(8.5*1.3, 6.5*1.3)
        Q = plt.quiver( x, y, u, v )
        plt.savefig(name_out_image.replace("<PIV_STEP_LABEL>","B"), bbox_inches='tight')
        #plt.show()   # debug line
        plt.close()
        #**** ****#
        #exit()

        #**** calling openpiv-python outliers replacement function ****#
	u, v = openpiv.filters.replace_outliers( u, v, method='localmean', max_iter=10, kernel_size=2)
        #**** ****#
        #exit()

        #**** fig out step C ****#
        fig, ax = plt.subplots()
        ax.set_xlabel("X [px]") ; ax.set_ylabel("Y [px]") ; ax.set_title(title.replace("<PIV_STEP_TITLE>","OUTLIERS REPLACED BY \"localmean\": FINAL DATA"),fontsize=12)
        fig.set_size_inches(8.5*1.3, 6.5*1.3)
        Q = plt.quiver( x, y, u, v )
        plt.savefig(name_out_image.replace("<PIV_STEP_LABEL>","C"), bbox_inches='tight')
        #plt.show()   # debug line
        plt.close()
        #**** ****#
        #exit()

        #**** saving/writing piv out data in file ****#
	openpiv.tools.save(x, y, u, v, mask, file_out)
        #**** ****#
        #exit()

        #**** defining outlier percentage (step B) by reading the mask values ("True" == outilers)****#
        #print len(mask)    # debug line
        number_of_vectors = 0
        number_of_outliers = 0
        for ele in mask:
                for vector_mask in ele:
                        number_of_vectors += 1
                        #print type(vector_mask)   # debug line
                        if vector_mask == True: number_of_outliers += 1
        #exit()
        #**** ****#
        #exit()

        #**** openpiv-python functions that could be used in a near future ****#
	#x, y, u, v = openpiv.scaling.uniform(x, y, u, v, scaling_factor = 1) #1 px/um
        #openpiv.tools.display_vector_field(file_out, scale=100, width=0.0025)
        #**** ****#
        #exit()
        
        return number_of_vectors, number_of_outliers

#****************************#
#**** END : Section (-1) ****#
#****************************#
print
#exit()

#**********************************************************************#
#**** Section 0: Making directories tree and defining in variables ****#
#**********************************************************************#

#**** in variables ****#
d0 = sys.argv[1] ; # d0 = <d0> = linage-movie directory
local_path_to_norm = sys.argv[2]
local_path_to_hyst = sys.argv[3]
hyst_or_norm = sys.argv[4]   # "hyst" or "norm" images
number_of_stacks = int(sys.argv[5])
extension = sys.argv[6]   # tif, bmp, png ...
path_to_pipeline = sys.argv[7];  # path to pipeline
local_path_to_dsoi = sys.argv[8]
non_interactive_mode = sys.argv[9]
#print d0+"\n"+local_path_to_norm+"\n"+local_path_to_hyst+"\n"+hyst_or_norm+"\n"+str(number_of_stacks)+"\n"+extension+"\n"+path_to_pipeline+"\n"+local_path_to_dsoi+"\n"+non_interactive_mode   # debug line
#hyst_or_norm = "norm"   # debug line
#**** ****#
#exit()

#**** defining the number of (out) frames ****#
number_of_frames = number_of_stacks - 1
#**** ****#
#exit()

#**** defining "frame_number and outlier_percentage" list ****#
frame_number_and_outlier_percentage_list = []
#**** ****#
#exit()

#**** making directories ****#
d6 = "PIV"  ; os.system("mkdir -p %s/%s" % (d0,d6))
d6log = "LOG" ; os.system("mkdir -p %s/%s/%s" % (d0,d6,d6log))
d6data = "piv_data" ; os.system("mkdir -p %s/%s/%s" % (d0,d6,d6data))
d6raw = "raw_piv_data" ; os.system("mkdir -p %s/%s/%s" % (d0,d6,d6raw))
d6steps = "piv_steps_images" ; os.system("mkdir -p %s/%s/%s" % (d0,d6,d6steps))
d6quali = "Quality" ; os.system("mkdir -p %s/%s/%s" % (d0,d6,d6quali))
#**** ****#
#exit()

#**** defining necessary paths ****#
#path_to_piv = "%s%s/%s/%s/" % (path_to_pipeline,d0,d6)
local_path_to_piv = "%s/%s/" % (d0,d6)

#path_to_log = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d6,d6log)
local_path_to_log = "%s/%s/%s/" % (d0,d6,d6log)

#path_to_data = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d6,d6data)
local_path_to_data = "%s/%s/%s/" % (d0,d6,d6data)

#path_to_raw = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d6,d6raw)
local_path_to_raw = "%s/%s/%s/" % (d0,d6,d6raw)

#path_to_steps = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d6,d6steps)
local_path_to_steps = "%s/%s/%s/" % (d0,d6,d6steps)

#path_to_quali = "%s%s/%s/%s/%s/" % (path_to_pipeline,d0,d6,d6quali)
local_path_to_quali = "%s/%s/%s/" % (d0,d6,d6quali)
#**** ****#
#exit()

#**** loading the list of in (".pivtif") files to be used ****#
if hyst_or_norm == "hyst":
        string_to_list_images_files  = "ls %s%s-%s????.%s" % (local_path_to_hyst,d0,hyst_or_norm,extension)
        path_to_images = local_path_to_hyst
        #os.system(string_to_list_images_files) # debug line
elif hyst_or_norm == "norm":
        string_to_list_images_files  = "ls %s%s-%s????.%s" % (local_path_to_norm,d0,hyst_or_norm,extension)
        path_to_images = local_path_to_norm
        #os.system(string_to_list_images_files) # debug line
else:
        print "deu merda no hyst or norm definition"
        print "pipeline forced to close"
        exit()
I_string = subp.check_output(string_to_list_images_files,shell=True) # string of all ".extension" images files to be used 
#print I_string    # debug line
list_of_images_files = I_string.split("\n")  ;  list_of_images_files.remove("")
#print list_of_images_files, len(list_of_images_files)   # debug line

#** security routine:
if len(list_of_images_files) != number_of_stacks:
    print "ERROR:   number of in \"images-%s-%s.%s\" files is diferent from number of stacks broadcast from prp" % (d0,hyst_or_norm,extension)
    print "         Expected number of stacks = ", number_of_stacks
    print "         number of in files =", len(list_of_images_files)
    print "code stoped!"
    exit()
#**** ****#
#exit()

#**** defining the name of the images to be open and the names of the files out to be writen ****#
A = [] ; B = [] ; C = [] ; D = []
index = 0
for image in list_of_images_files[:number_of_stacks-1]:
        A.append(image)
        #print "A = %s" % (A[index])    # debug line
        B.append(list_of_images_files[list_of_images_files.index(image)+1])
        #print "B = %s" % (B[index])    # debug line
        C.append((image.replace(path_to_images,local_path_to_data)).replace(extension,"piv"+extension))
        #print "C = %s" % (C[index])    # debug line
        D.append((image.replace(path_to_images,"%sraw-"%(local_path_to_raw))).replace(extension,"piv"+extension))
        #print "D = %s" % (D[index])    # debug line
        #print   # debug line
        index += 1
#**** ****#
#exit()

#**** debug lines ****#
#for index in range(number_of_stacks-1):
#        print A[index]  ;  print B[index] ; print C[index]
#**** ****#
#exit()

#**********************************************************************#
#**** END : Section 0 *************************************************#
#**********************************************************************#
#exit()
print


#****************************************#
#**** Section 1 : Executing PIV loop ****#
#****************************************#

print " PIV loop:"
#number_of_frames = 2 ; number_of_vectors = 5440   #  debug line
for index in range(number_of_frames):
        
        #**** calling piv function (saves out file, images setps and return number of vectors and outliers number) ****#
        number_of_vectors, number_of_outliers = openPIV(A[index],B[index],C[index],D[index],path_to_images,extension,local_path_to_steps)
        print "   Frame pair %i/%i done: \"%s\" out file was written" % ((index+1),(number_of_frames),C[index])
        #print number_of_vectors, number_of_outliers   # debug line
        #**** ****#
        #exit()

        #**** defining frame number ****#
        frame_number =  int(((C[index].replace(local_path_to_data,"")).replace("%s-%s"%(d0,hyst_or_norm),"")).replace(".piv%s"%(extension),""))
        #print frame_number   # debug line
        #**** ****#
        #exit()

        #**** appending data to outliers percatage list ****#
        percentage_of_outliers = (float(number_of_outliers)/number_of_vectors)*100
        frame_number_and_outlier_percentage_list.append([frame_number,percentage_of_outliers])
        #print frame_number_and_outlier_percentage_list[index]   # debug line
        #**** ****#
        #exit()

#print frame_number_and_outlier_percentage_list  # debug line
#** security routine:
if len(frame_number_and_outlier_percentage_list) != number_of_frames:
    print "ERROR:   number of in calculated pivs is diferent from number of frames" % (d0,hyst_or_norm,extension)
    print "         Expected number of frames = ", number_of_frames
    print "         number of calculated pivs =", len(frame_number_and_outlier_percentage_list)
    print "code stoped!"
    exit()
#****************************************#
#**** END : Section 1 *******************#
#****************************************#
#exit()
print

#***********************************************************************#
#**** Section 2 : ploting piv quality graph and saving quality file ****#
#***********************************************************************#

#**** saving quality file ****#
quality_out_file_name = "%squality-%s-%s-outliers.piv%s" % (local_path_to_quali,d0,hyst_or_norm,extension)
#print quality_out_file_name   # debug line
file_quali_out = open(quality_out_file_name,"w")
file_quali_out.write("# frame number[#]  percentage of outilers[%]\n")
for ele in frame_number_and_outlier_percentage_list:
        file_quali_out.write("%i %f\n"%(ele[0],ele[1]))
#**** ****#
#exit()

#**** Defining X,Y for plot ****#
#print len(Quality_block_data)   # debug line
x = np.zeros(number_of_frames) ; y = np.zeros(number_of_frames) 
counter = 0
for ele in frame_number_and_outlier_percentage_list:
    i = counter
    x[i] = ele[0] ; y[i] = ele[1]
    counter += 1
#**** ****#
#exit()
    
#**** plot 1: outliers percentage plot ****#
fig, ax = plt.subplots()
ax.plot(x,y,"bo",label="over all %i vectors"%(number_of_vectors))

plt.ylabel("Outliers vectors percentage [%]")
plt.xlabel("Frame number [#]")
plt.title("data from \"%s-%s.piv%s\" files"%(d0,hyst_or_norm,extension))
plt.legend()
plt.xlim(-10,number_of_frames+10) ; plt.ylim(0,100)
#plt.show()  # debug line
plt.savefig("%spiv%s-%s-%s-outliers"%(local_path_to_quali,extension,d0,hyst_or_norm,))
plt.close()
#**** ****#
#exit()

#**********************************************************************#
#**** END : Section 2 *************************************************#
#**********************************************************************#
#exit()
print

#************************************************************************************************************#
#**** 3 - saving the section out information necessary for the rest of pipeline in a "file.pivextension" ****#
#************************************************************************************************************#
# pipeline will read this file and load the information for teh rest of the sections
file_out_path_and_name = "%sout-info-%s-%s.piv%s" % (local_path_to_dsoi,d0,hyst_or_norm,extension)
string_to_remove_piv_out_info_file = "rm %s" % (file_out_path_and_name) ; os.system(string_to_remove_piv_out_info_file)
#print file_out_path_and_name    # debug line
file_out = open(file_out_path_and_name,"w")
file_out.write("%s\n"%(local_path_to_data))
file_out.write("%s\n"%(number_of_frames))
file_out.write("%i\n"%(number_of_vectors))
file_out.close()
string_to_make_piv_out_info_file_read_only = "chmod 444 %s" % (file_out_path_and_name)
os.system(string_to_make_piv_out_info_file_read_only)
#***************************************************************************************************#
#** END 3 ******************************************************************************************#
#***************************************************************************************************#
#exit()
print

if non_interactive_mode == "OFF":
        #**** making/opening the "log.piv" file ****#
        input_string = raw_input("Make log file (yes or no): ")
        if input_string == "yes":
                file_out_path_and_name = "%slog-%s-%s.piv%s" % (local_path_to_log,d0,hyst_or_norm,extension)
                #print file_out_path_and_name  # debug line
                file_out = open(file_out_path_and_name,"w")
                file_out.close()
                string_to_open_log_file = "emacs %s" % (file_out_path_and_name)
                os.system(string_to_open_log_file)
        #**** ****#
        #exit()
        print

print "******* End of PIV *******\n"
