import sys
import os
import numpy as np
import subprocess as subp
import matplotlib.pyplot as plt

#*********************************************************************#
                #***************************************#
                #**** Pre PreProcessing - Section 0 ****#
                #***************************************#
#*********************************************************************#

#**** defining pipeline version string ****#
pipeline_version = sys.argv[0].replace("pipeline-","")
pipeline_version = pipeline_version.replace(".py","")
#**** ****#
#exit()

#**** defining bye bye statement ****#
bye_bye = "\n  Aloha bye bye, pipeline ended all right!\n\n"
#**** ****#
#exit()

#**** making welcome statement and showing to user ****#
print "\n\n       Welcome to pipeline!      (version %s)" % (pipeline_version)
print "                                              by Paulo Godolphim and Leonardo Brunnet."
print "                                                 MTC, LabCel, Lamoc, UFRGS - IF and ICBS"
print "                                                 python, FIJI, octave, openpiv-python, gimp, f90\n\n"
#**** ****#
#exit()

#**** geting if is interactive mode or not (this is more for debug and develooment steps (or maybe not)) ****#
non_interactive_mode = "OFF"
list_of_sections = ["1","2","3","4","5"]
run_mode = "" ; local = "local" ; external = "external" ; aux = "aux"
list_of_run_modes = [local,external,aux]
already_done_section_list = ["1","prp","2","piv","3","vff","5","clc"]
if len(sys.argv[:]) < 2 or len(sys.argv[:]) > 2:
    if len(sys.argv[:]) != 5:
        print "few or to many input arguments  ---> code stoped\n"
        print "How to run:"
        print "    interactive mode --> python %s 0" % (sys.argv[0]) 
        print "non interactive mode --> python %s <SECTION> <LINAGE> <MOVIE> <RUN_MODE>" % (sys.argv[0])
        print "                                                                           <RUN_MODE> = [local,external,aux]\n"
        exit()
    if sys.argv[1] in list_of_sections and sys.argv[4] in list_of_run_modes:
        non_interactive_mode = "ON"
        run_mode = sys.argv[4]
    else:
        print "wrong section or run_mode input  ---> code stoped\n"
        print "How to run:"
        print "    interactive mode --> python %s 0" % (sys.argv[0]) 
        print "non interactive mode --> python %s <SECTION> <LINAGE> <MOVIE> <RUN_MODE>" % (sys.argv[0])
        print "                                                                           <RUN_MODE> = [local,external,aux]\n"
        exit()
    if sys.argv[1] in already_done_section_list and sys.argv[4] != aux:
        print "Section already done --> can only be run in non interactive \"auxiliar\" mode"
        print
        exit()
if len(sys.argv[:]) == 2 and sys.argv[1] != "0":
    print "wrong section or run_mode input  ---> code stoped\n"
    print "How to run:"
    print "    interactive mode --> python %s 0" % (sys.argv[0]) 
    print "non interactive mode --> python %s <SECTION> <LINAGE> <MOVIE> <RUN_MODE>" % (sys.argv[0])
    print "                                                                           <RUN_MODE> = [local,external,aux]\n"
    exit()
#**** ****#
#exit()

#**** defining some global important paths ****#
if run_mode == "" or run_mode == local or run_mode == aux:
    path_to_dropbox = "/home/paulo/Dropbox/"
elif run_mode == external:
    path_to_dropbox = "/home/paulo/Dropbox/pesquisas/pesq_mestrado/pipeline/z_fake_Dropbox_to_run_in_pcout/"  # when running on a outside pc

if run_mode == "" or run_mode == local or run_mode == aux:
    path_to_lamoc_movies = path_to_dropbox + "pesquisas/pesq_mestrado/movies_Lamoc/"
elif run_mode == external:
    path_to_lamoc_movies = path_to_dropbox + "movies_Lamoc/"   # when running on a outside pc

if run_mode == "" or run_mode == local or run_mode == aux:
    path_to_pipeline = path_to_dropbox + "pesquisas/pesq_mestrado/pipeline/"
elif run_mode == external:
    path_to_pipeline = path_to_dropbox + "pipeline/"              # when running on a outside pc

#path_to_pipeline = path_to_dropbox + "pesquisas/pesq_mestrado/resultados-parcias-scc9-cal27-hacat/new_pipe_line_test/pipeline/" # beta version path
path_to_pipeline_scripts = path_to_pipeline + "VOID_pipeline_scripts/"

path_to_imagej_fiji_scripts  = path_to_dropbox + "software/fiji-linux64/"
path_to_imagej_fiji_executable  = path_to_dropbox + "software/fiji-linux64/Fiji.app/"
#**** ****#
#exit()


#******************************************************************#
#****** A - showing the linage options and letting user chose *****#
#******************************************************************#

#**** Geting and printing what are the linages options for the user to chose ****#
string_to_list_lamoc_movies = "ls -d %s*/" % (path_to_lamoc_movies)
#os.system(string_to_list_lamoc_movies) #debug line
d_string = subp.check_output(string_to_list_lamoc_movies,shell=True) # string of all directories in "movies_Lamoc/"
d_list = d_string.split('\n') ; d_list.remove('') ; # make a list of the directories
dcl = []
for j in range(len(d_list)):
    d_list[j]=d_list[j].split(path_to_lamoc_movies) ; d_list[j].remove("")
    ele = d_list[j] ; dcl.append(ele[0])    
print "** directories in movies_Lamoc: ",dcl

dcl2 =[]
for j in range(len(dcl)):    
    dcl[j]=dcl[j].split("/") ; dcl[j].remove("")
    ele = dcl[j] ; dcl2.append(ele[0])
print '\n\n'+'     '+'     '.join([str(item) for item in dcl2])+'\n\n'
#**** ****#
#exit()

#**** Asking and geting the linage chosen by the user ****#
if non_interactive_mode == "ON":
    input_string = sys.argv[2]         # headless mode 
    print "Enter one of the above linages: %s" % (input_string)  # headless mode 
else:
    input_string = raw_input("Enter one of the above linages: ")
for linage in dcl2:
    if input_string == linage:
        j=dcl2.index(linage)
linage = dcl[j]
#**** ****#
#exit()

#**** geting specific information about 'linage' directory and printing it ****# 
string_to_list_linages_movies = "du -h "+path_to_lamoc_movies+linage[0]+"/"
mdu_string = subp.check_output(string_to_list_linages_movies,shell=True) 
mdu_list = mdu_string.split('\t') 
size_linage_directory = mdu_list[0]
mwc_string = subp.check_output(string_to_list_linages_movies+"*.zvi | wc",shell=True) 
mwc_list = mwc_string.split(' ') 
number_of_zvi_files = mwc_list[6]
mwc_string = subp.check_output(string_to_list_linages_movies+"* | wc",shell=True) 
mwc_list = mwc_string.split(' ') 
number_of_files = mwc_list[6]
sp1 = path_to_lamoc_movies+linage[0]+"/" # string to print 1 ...
sp2 = size_linage_directory
sp3 = number_of_zvi_files
sp4 = str(int(number_of_files)-int(number_of_zvi_files))
print "** user chose directory: '%s' "% (sp1)
print "**            with size: '%s' and number of \".zvi\" files = '%s'  (number of non \".zvi\" files = %s)"% (sp2,sp3,sp4)
print
#**** ****#
#exit()

#******************************************************************#
#** END A *********************************************************#
#******************************************************************#
#exit()
print

#******************************************************************#
#****** B - showing the movies options and letting user chose *****#
#******************************************************************#

#**** Geting and printing what are the movies options for the user to chose ****#
string_to_list_linages_movies = "du -h "+path_to_lamoc_movies+linage[0]+"/"
#os.system(string_to_list_linages_movies)
m_string = subp.check_output(string_to_list_linages_movies+"*.zvi",shell=True) # string of all movies in linage directory
m_list = m_string.split('\n') ; m_list.remove('') ; # make a list of the movies
print " size     code    movie name"
for j in range(len(m_list)):
    index = j ; movie_number = j+1 ; m_code = "m0"+str(movie_number)
    if movie_number > 10: m_code = "m"+str(movie_number)
    m_list[index]=m_list[index].split('\t'+sp1) ; m_list[index].append(m_code)
for ele in m_list:
    print "  %s     %s     %s" % (ele[0], ele[2], ele[1])
print
#**** ****#
#exit()

#**** Asking and geting the movie chosen by the user ****#
if non_interactive_mode == "ON":
    input_string = sys.argv[3]   # headless mode 
    print "Enter the code of one of the above movies: %s" % (input_string)  # headless mode 
else:
    input_string = raw_input("Enter the code of one of the above movies: ")
for ele in m_list:
    m_code = ele[2]
    if input_string == m_code:
        file_name = ele[1]
file_name = file_name.replace(' ','\ ') # used to in headless mode
#print file_name    # debug line
#**** ****#
#exit()

#******************************************************************#
#** END B *********************************************************#
#******************************************************************#
#exit()
print

#*************************************************#
#**** C - defining the main section to be run ****#
#*************************************************#

#**** making the main section labels list ****#
main_section_labels_list = []
prp = "prp"  ;  main_section_labels_list.append(prp)
piv = "piv"  ;  main_section_labels_list.append(piv)
vff = "vff"  ;  main_section_labels_list.append(vff)
mnc = "mnc"  ;  main_section_labels_list.append(mnc)
clc = "clc"  ;  main_section_labels_list.append(clc)
#**** ****#
#exit()

#**** making the main section names list ****#
main_section_names_list = []
prp_name = "preProcessing"  ;  main_section_names_list.append(prp_name)
piv_name = "Particle Image Velocimetry (PIV)"  ;  main_section_names_list.append(piv_name)
vff_name = "Velocity Field Frame maker"  ;  main_section_names_list.append(vff_name)
mnc_name = "Manual Counting Procedures"  ;  main_section_names_list.append(mnc_name)
clc_name = "Calculations"  ;  main_section_names_list.append(clc_name)
#**** ****#
#exit()

#**** showing to user the main section options ****#
stp = "\n"
stp += " Section   Input Argument   Section Name\n"
stp += "\n"
stp += "   (1)          prp    -    preProcessing\n"
stp += "   (2)          piv    -    Particle Image Velocimetry (PIV)\n"
stp += "   (3)          vff    -    Velocity Field Frame maker\n"
stp += "   (4)          mnc    -    Manual Counting Procedures\n"
stp += "   (5)          clc    -    Calculations\n"
print stp
#**** ****#
#exit()

#**** Asking and geting (from user) the main section to be run ****#
if non_interactive_mode == "ON":
    main_section = main_section_labels_list[int(sys.argv[1])-1]    # headless mode 
    print "Enter the input argument of the section to be run: %s" % (main_section)   # headless mode 
else:
    main_section = raw_input("Enter the input argument of the section to be run: ")
#**** ****#
#exit()

#**** showing the main section chosed to user ****#
print "** user chose section: "+ main_section_names_list[main_section_labels_list.index(main_section)]
#** security/check routine:
if non_interactive_mode == "OFF":
    if main_section in already_done_section_list: 
        print "Section already done --> can only be run in non interactive \"auxiliar\" mode"
        print
        exit()
#**** ****#
#exit()

#**********************************************#
#** END C *************************************#
#**********************************************#
#exit()
print

              #*******************************************#
              #**** END pre PreProcessing - Section 0 ****#
#*********************************************************************#
#print ; exit()


#*******************************************************************#
                #***********************************#
                #**** PreProcessing - Section 1 ****#
                #***********************************#
#*******************************************************************#
path_and_file_name = sp1+file_name #; print path_and_file_name # movie to be opened

d0 = linage[0]+"-"+input_string  ;  os.system("mkdir -p %s" % (d0))
dsoi = "sections_out_info_files"  ;  os.system("mkdir -p %s/%s" % (d0,dsoi))
#path_to_dsoi = "s%s/%s/" % (path_to_pipeline,d0,dsoi)
local_path_to_dsoi = "%s/%s/" % (d0,dsoi)

#**** calling preProcessing MAIN script ****#
so1 = d0       # String Out 1 
so2 = path_and_file_name
so3 = path_to_pipeline
so4 = path_to_imagej_fiji_executable
so5 = path_to_pipeline_scripts
so6 = path_to_imagej_fiji_scripts
so7 = file_name
so8 = local_path_to_dsoi
#so9 = non_interactive_mode
string_to_call_main_script_preprocessing = "python preProcessing-1.1.py %s %s %s %s %s %s %s %s" % (so1, so2, so3, so4, so5, so6, so7, so8)
if main_section == prp:
    "parado de propostito para nao sobrescrever nada, fiz isso no lugar de adiconar 'ifs' melhores"
    exit()
    os.system(string_to_call_main_script_preprocessing)
    print bye_bye
    exit()
#**** ****#
#exit()

              #***********************#
              #**** END Section 1 ****#
#*****************************************************#
#print ; exit()


#*******************************************************************#
                #*************************#
                #**** PIV - Section 2 ****#
                #*************************#
#*******************************************************************#

#**** loading section 1 out data ****#
file_in_path_and_name = "%sout-info-%s.prp" % (local_path_to_dsoi,d0)
#print file_in_path_and_name    #  debug line
file_in = open(file_in_path_and_name)
data = file_in.read()
data = data.split("\n") ; data.remove("")
#print data       #  debug line
if len(data) != 3:
    print "      Error on in file \"out prp info file\": wrong number of infos"
    print "      pipeline forced to end."
    exit()
local_path_to_norm = data[0]
local_path_to_hyst = data[1]
number_of_stacks = data[2]   
#**** ****#
#exit()

#**** defining "hyst_or_norm" and images "extension" ****#
hyst_or_norm = "hyst"
#hyst_or_norm = "norm"
extension = "tif"
#extension = "bmp"
#**** ****#
#exit()

#**** calling Particle Image Velocimetry MAIN script ****#
so1 = d0       # String Out 1 
so2 = local_path_to_norm
so3 = local_path_to_hyst
so4 = hyst_or_norm
so5 = number_of_stacks
so6 = extension
so7 = path_to_pipeline
so8 = local_path_to_dsoi
so9 = non_interactive_mode
string_to_call_main_script_piv = "python particle_image_velocimetry-1.6.py %s %s %s %s %s %s %s %s %s" % (so1, so2, so3, so4, so5, so6, so7, so8, so9)
if main_section == piv:
    os.system(string_to_call_main_script_piv)
    print bye_bye
    exit()
#**** ****#
#exit()

              #***********************#
              #**** END Section 2 ****#
#**************************************************************#
#print ; exit()


#*************************************************************#
                #*************************#
                #**** VFF - Section 3 ****#
                #*************************#
#*************************************************************#

#**** loading section 2 out data ****#
if run_mode == aux:
    file_in_path_and_name = "%sout-info-%s-%s.piv%s" % (local_path_to_dsoi.replace(d0,d0+"_aux"),d0+"_aux",hyst_or_norm,extension)    # debug line
else:
    file_in_path_and_name = "%sout-info-%s-%s.piv%s" % (local_path_to_dsoi,d0,hyst_or_norm,extension)
#print file_in_path_and_name    #  debug line
file_in = open(file_in_path_and_name)
data = file_in.read()
data = data.split("\n") ; data.remove("")
#print data     #  debug line
if len(data) != 3:
    print "      Error on in file \"out piv info file\": wrong number of infos"
    print "      pipeline forced to end."
    exit()
local_path_to_piv_data = data[0]
number_of_frames = data[1]
number_of_vectors = data[2]   
#local_path_to_piv_data = data[0].replace("piv_data/","piv_data_aux/")   # debug line
#number_of_frames = 85   # debug line
#number_of_vectors = 5440   # debug line   
#**** ****#
#exit()

#**** defining "hyst_or_norm" and images "ON" or "OFF" ****#
hyst_or_norm = "hyst"
#hyst_or_norm = "norm"
#images_ON_or_OFF = "ON"
images_ON_or_OFF = "OFF"
#**** ****#
#exit()

#**** calling Velocity Field Frame maker MAIN script ****#
so1 = d0     # String Out 1 
so2 = local_path_to_piv_data
so3 = hyst_or_norm
so4 = extension
so5 = number_of_frames
so6 = number_of_vectors
so7 = images_ON_or_OFF
so8 = path_to_pipeline
so9 = local_path_to_dsoi
so10 = non_interactive_mode
if run_mode == aux:
    so1 = d0+"_aux"               # debug line
    so9 = local_path_to_dsoi.replace(d0,so1)  # debug line
string_to_call_main_script_vff = "python Velocity_Field_Frame_maker-4.3.py %s %s %s %s %s %s %s %s %s %s" % (so1, so2, so3, so4, so5, so6, so7, so8, so9, so10)
if main_section == vff:
    os.system(string_to_call_main_script_vff)
    print bye_bye
    exit()
#**** ****#
#exit()

              #***********************#
              #**** END Section 3 ****#
#****************************************************#
#print ; exit()


#*************************************************************#
                #*************************#
                #**** MNC - Section 4 ****#
                #*************************#
#*************************************************************#

#**** loading section 1 out data ****#
#**** ****#
#exit()

#**** calling Manual Counting Procedures MAIN script ****#
#so1 = d0     # String Out 1 
#so2 = local_path_to_piv_data
#so3 = hyst_or_norm
#so4 = extension
#so5 = number_of_frames
#so6 = number_of_vectors
#so7 = images_ON_or_OFF
#so8 = path_to_pipeline
#so9 = local_path_to_dsoi
#so10 = non_interactive_mode
#if run_mode == aux:
#    so1 = d0+"_aux"               # debug line
#    so9 = local_path_to_dsoi.replace(d0,so1)  # debug line
#string_to_call_main_script_mnc = "python mnc.py %s %s %s %s %s %s %s %s %s %s" % (so1, so2, so3, so4, so5, so6, so7, so8, so9, so10)
if main_section == mnc:
    print
    print "\n   SECTION UNDERCONSTRUCTION ---> sorry!"
    print
    #os.system(string_to_call_main_script_mnc)
    print bye_bye
    exit()
#**** ****#
#exit()

              #***********************#
              #**** END Section 4 ****#
#****************************************************#
#print ; exit()


#*************************************************************#
                #*************************#
                #**** CLC - Section 5 ****#
                #*************************#
#*************************************************************#

#**** loading section 3 out data ****#
if run_mode == aux:
    file_in_path_and_name = "%sout-info-%s-%s-piv%s.vff" % (local_path_to_dsoi.replace(d0,d0+"_aux"),d0+"_aux",hyst_or_norm,extension)    # debug line
else:
    file_in_path_and_name = "%sout-info-%s-%s-piv%s.vff" % (local_path_to_dsoi,d0,hyst_or_norm,extension)
#print file_in_path_and_name    #  debug line
file_in = open(file_in_path_and_name)
data = file_in.read()
data = data.split("\n") ; data.remove("")
#print data     #  debug line
if len(data) != 18:
    print "      Error on file \"out vff info file\": wrong number of infos"
    print "      pipeline forced to end."
    exit()
local_path_to_field = data[0]             
local_path_to_fluctu = data[1]            
number_of_frames = data[3]                
number_of_vectors = data[5]               
shape_field_file_name = data[6]           
shape_fluctu_file_name = data[7]          
shape_field_file_path_and_name = data[8]  
shape_fluctu_file_path_and_name = data[9]
#print local_path_to_field+"\n"+local_path_to_fluctu+"\n"+number_of_frames+"\n"+number_of_vectors+"\n"+shape_field_file_name+"\n"+shape_fluctu_file_name+"\n"+shape_field_file_path_and_name+"\n"+shape_fluctu_file_path_and_name   # debug line
#local_path_to_ftm = data[2]              ; print local_path_to_ftm   # debug line
#old_number_of_vectors = data[4]          ; print old_number_of_vectors   # debug line
#ftm_path_and_name_list = data[10:]       ; print ftm_path_and_name_list   # debug line
#**** ****#
#exit()

#**** loading absolut time corretion factor from preProcessing files ****#
file_to_open_name = "%s/preProcessing/absolut-time-correction-for-cuted-frames-in-hours.prp" % (d0)
#print file_to_open_name   # debug line
file_to_open = open(file_to_open_name,"r")
data = file_to_open.read() ; data.replace("\n","")
abslout_time_correction_factor_in_hours = float(data)
#print abslout_time_correction_factor_in_hours    # debug line
#**** ****#
#exit()

#**** calling Calculations MAIN script ****#
so1 = d0     # String Out 1 
so2 = local_path_to_field
so3 = local_path_to_fluctu
so4 = hyst_or_norm
so5 = extension
so6 = number_of_frames
so7 = number_of_vectors
so8 = shape_field_file_name.replace("????","####")
so9 = shape_fluctu_file_name.replace("????","####")
so10 = shape_field_file_path_and_name.replace("????","####")
so11 = shape_fluctu_file_path_and_name.replace("????","####")
so12 = path_to_pipeline_scripts
so13 = path_to_pipeline
so14 = local_path_to_dsoi
so15 = non_interactive_mode
so16 = abslout_time_correction_factor_in_hours
if run_mode == aux:
    so1 = d0+"_aux"                           # debug line
    s14 = local_path_to_dsoi.replace(d0,so1)  # debug line
string_to_call_main_script_clc="python calculations-1.4.py %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"%(so1,so2,so3,so4,so5,so6,so7,so8,so9,so10,so11,so12,so13,so14,so15,so16)
if main_section == clc:
    os.system(string_to_call_main_script_clc)
    print bye_bye
    exit()
#**** ****#
#exit()

              #***********************#
              #**** END Section 5 ****#
#****************************************************#
print ; exit()





#*********************************#
#***  Section Velocity Fields  ***#
#*********************************#
